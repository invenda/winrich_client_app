<?php

return [
   'adminEmail' => 'admin@example.com',

   'WINRICH_REPORTING_SERVER' => [
       'DOMAIN' => 'http://winrichdev.com',
       'BASE_URL' => '',
   ],
   
   'salt' => 'qtjdk99t4',
   'passwordresetsalt' => 'lksfgrgka',
   'mailgunKey' => 'key-9e6f4a8581d71a50a96066d1ede7579e',
   'mailgunDomainName' => 'winrichdev.com',
   'mailsendfrom' => 'customer_support@winrichdev.com',
   'reportsdb' => 'mysql:host=127.0.0.1;dbname=winrich',
   'reportsdbuser' => 'root',
   'reportsdbpass' => '',
   'defaultbucket' => 'imgwinrichapp.owshi.com',
   'imgs3rootdir' => 'http://imgwinrichapp.owshi.com',
];
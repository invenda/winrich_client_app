<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        's3' => [
                'class' => 'frostealth\yii2\aws\s3\Service',                
                'credentials' => [ // for QA imgkhadem.owshi.com
                        'key' => 'AKIAICZYGSUCA4JYSQJQ',
                        'secret' => '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',
                ],
                'region' => 'us-east-1',
                'defaultBucket' => $params['defaultbucket'],
                'defaultAcl' => 'public-read',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xSMA6ysY39bICPa0gTj_VGPS0pckQbso',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
				 '<controller:assets>/<instrumentId:\d+>' => '<controller>/index',
				 '<controller:assets>/<action:create>/<instrumentId:\d+>' => '<controller>/create',
				 '<controller:site>/<action:resetpassword>/<token:\w+>' => '<controller>/<action>',
				 '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				 '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
				 
				 
			],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

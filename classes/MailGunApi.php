<?php

namespace app\classes;

use Yii;
use Mailgun\Mailgun;

class MailGunApi {
  
    public function sendmail($subject, $textContent, $fromAddress, $toAddress){
        $mailGunKey =  Yii::$app->params['mailgunKey'] ;
        $mailGunDomainName = Yii::$app->params['mailgunDomainName'] ;
      
        $mgClient = new Mailgun($mailGunKey);
        $domain = $mailGunDomainName;
        try {
            $result = $mgClient->sendMessage($domain, array(
                'from' => "<$fromAddress>",
                'to' => $toAddress,
                'subject' => $subject,
                'html' => $textContent
            ));
        }catch (\Exception $e){
            $message = $e->getMessage();
            $status = 'unsuccessful';
            return json_encode(array($status, $message));
        }
        
        if($result) {
            if(property_exists($result, 'http_response_body'))
                $mailGunMsgId = $result->http_response_body->id;

            if(!empty($mailGunMsgId)){
                $status = 'success';
                return json_encode(array($status, $mailGunMsgId));
            }else{
                return json_encode(array('unsuccessful', 'could not fetch email Id'));
            }
        }else{
            return json_encode(array('unsuccessful', 'could not send email'));
        }
      
    }
}

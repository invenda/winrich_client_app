<?php

namespace app\classes;
use Yii;

class RestApi {

    var $baseUrl;
    var $domain;
    
    public function __construct($domain2=NULL) {
        $apiServer = Yii::$app->params['WINRICH_REPORTING_SERVER'];
        $this->baseUrl = $apiServer["BASE_URL"];
		
		if(empty($domain2)){
			
			$this->domain =  $apiServer["DOMAIN"];
		}else{
			
			$this->domain = $domain2;
		}
    }
    
    private function getUrl($uri) {
        
        return $this->domain . $this->baseUrl . $uri;
    }
    
    public function post($uri, $data) {
        $url = $this->getUrl($uri);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_HTTPHEADER, [
            'Accept:application/json',
            'Content-Type:application/json'
        ]);
        curl_setopt($ch, CURLOPT_POST, 1);
        Yii::trace("Going to post " . $url . " : ");// . json_encode($data));
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }
    
    
    public function patch($uri, $data) {
        $url = $this->getUrl($uri);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($ch,CURLOPT_HTTPHEADER, [
            'Accept:application/json',
            'Content-Type:application/json'
        ]);
        //curl_setopt($ch, CURLOPT_POST, 1);
        Yii::trace("Going to patch " . $url . " : " . json_encode($data));
        $ret = curl_exec($ch);
        curl_close($ch);
        return $ret;
    }
    
    
    public function get($uri, $data=[]) {

        $urldata = [];
        foreach($data as $k=>$v) {
            $urldata[] = $k . '=' . urlencode($v);
        }        
        $url = $this->getUrl($uri) . '?' .implode('&', $urldata);
		$ch = curl_init($url);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch,CURLOPT_HTTPHEADER, [
            'Accept:application/json',
            'Content-Type:application/json'
        ]);
		//curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        //curl_setopt($ch, CURLOPT_POST, 1);
        Yii::trace("Going to get " . $url . " : " . json_encode($data));
        $ret = curl_exec($ch);
        curl_close($ch);
		//return $ret;
		$ret_ob = json_decode($ret,false);
		//return $ret_ob;
		if( $ret_ob === NULL ) {
        //return "test";
			//Remove CR's from ouput - make it one line
			$json = str_replace("\n", "", $ret);

		    //Remove //, [ and ] to build qualified string  
			$data = substr($json, 4, strlen($json) -5);

		    return json_decode($data,false); 
			
		}else{
			return json_decode($ret,false);
		}
    }
}

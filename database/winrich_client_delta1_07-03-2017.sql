ALTER TABLE `winrich_client`.`fixeddeposit` 
ADD COLUMN `bankname` VARCHAR(255) NULL AFTER `accountname`,
ADD COLUMN `bankaddress` VARCHAR(255) NULL AFTER `bankname`;

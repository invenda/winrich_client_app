-- MySQL dump 10.13  Distrib 5.7.14, for Win64 (x86_64)
--
-- Host: localhost    Database: winrich_client
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arn`
--

DROP TABLE IF EXISTS `arn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `instrumentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arn`
--

LOCK TABLES `arn` WRITE;
/*!40000 ALTER TABLE `arn` DISABLE KEYS */;
INSERT INTO `arn` VALUES (1,'Winrich',1),(2,'HDFC',3),(3,'Shriram',3),(4,'Bajaj Allianz',4),(5,'Chola Ms',4),(6,'ICICI Prudential',4),(7,'Star Health',4),(8,'Tata AIG',4),(9,'The New India Assurance Co',4),(10,'United Insurance',4),(11,'Birla Sun Life Insurance',5),(12,'HDFC Standard Life',5),(13,'ICICI Prudential',5),(14,'LIC',5);
/*!40000 ALTER TABLE `arn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instrumentid` int(11) DEFAULT NULL,
  `arnid` int(11) DEFAULT NULL,
  `folio` varchar(500) DEFAULT NULL,
  `transactiontypeid` int(11) DEFAULT NULL,
  `schemeid` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `multipledays` varchar(45) DEFAULT NULL,
  `frequencyid` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `modifieddate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets`
--

LOCK TABLES `assets` WRITE;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` VALUES (1,1,1,'123',1,1,'wqert',1,124,'12',12,'2017-01-13 00:00:00','2017-01-19 00:00:00','2017-01-10 00:00:00','2017-01-12 18:57:55'),(2,4,4,'name',3,4,'des',1,123,'123',4,'2017-01-11 00:00:00','2017-01-11 00:00:00','2017-01-11 18:40:47',NULL),(3,1,1,'richie rich',1,1,'breif',1,9900,'2',345,'2017-01-13 00:00:00','2017-01-13 00:00:00','2017-01-12 17:35:33','2017-01-12 17:35:33'),(4,1,1,'qwerty',1,1,'wqert',1,124,'12',12,'2017-01-12 00:00:00','2017-01-19 00:00:00','2017-01-12 18:57:10','2017-01-12 18:57:10');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commodity`
--

DROP TABLE IF EXISTS `commodity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `clientid` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `purchasedate` datetime DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commodity`
--

LOCK TABLES `commodity` WRITE;
/*!40000 ALTER TABLE `commodity` DISABLE KEYS */;
INSERT INTO `commodity` VALUES (1,1,'Just for fun',1,5,'2017-01-01 00:00:00','2017-01-16 00:00:00',NULL),(2,1,'Testing my gold purchase',1,100,'2016-12-01 00:00:00',NULL,NULL),(3,1,'Test',1,1212,'2015-01-01 00:00:00',NULL,NULL);
/*!40000 ALTER TABLE `commodity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commoditytype`
--

DROP TABLE IF EXISTS `commoditytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commoditytype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commoditytype`
--

LOCK TABLES `commoditytype` WRITE;
/*!40000 ALTER TABLE `commoditytype` DISABLE KEYS */;
INSERT INTO `commoditytype` VALUES (1,'Gold'),(2,'Silver');
/*!40000 ALTER TABLE `commoditytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instrument`
--

DROP TABLE IF EXISTS `instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instrument` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instrument`
--

LOCK TABLES `instrument` WRITE;
/*!40000 ALTER TABLE `instrument` DISABLE KEYS */;
INSERT INTO `instrument` VALUES (1,'Cash Flow'),(2,'Equity'),(3,'Fixed Deposits'),(4,'General Insurance'),(5,'Life Insurance'),(6,'Medical Insurance'),(7,'Real Estate');
/*!40000 ALTER TABLE `instrument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scheme`
--

DROP TABLE IF EXISTS `scheme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scheme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `instrumentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `scheme`
--

LOCK TABLES `scheme` WRITE;
/*!40000 ALTER TABLE `scheme` DISABLE KEYS */;
INSERT INTO `scheme` VALUES (1,'CashFlow','0000000Cas',1),(2,'HDFC FD','0000000Hdf',3),(3,'Shriram FD','0000000Shr',3),(4,'Accident Cover','0000000Acc',4),(5,'Bajaj','0000000Baj',4),(6,'Bajaj Allianz - Health Guard','000000Baj1',4),(7,'Bajaj Allianz - Personal Guard','000000Baj2',4),(8,'Bajaj Critical Illness','000000Baj4',4),(9,'Bajaj-Family Floater','000000Baj5',4),(10,'Bajajallianz Private Car Package Policy','000000Baj3',4),(11,'Extra Care','0000000Ext',4),(12,'Family Floater','0000000Fam',4),(13,'Four Wheeler','0000000Fou',4),(14,'Health Guard','0000000Hea',4),(15,'Icici Pru','000000Ici1',4),(16,'Icici Pru Health Saver','000000Ici2',4),(17,'Icici Pru Health Saver','000000Ici4',4),(18,'Lic','000000Lic3',4),(19,'Birla Sun Life Insurance','0000000Bir',5),(20,'Hospita Care','0000000Hos',5),(21,'Hospital Care','000000Hos1',5),(22,'Icici Pru Health Saver','00000Ici10',5),(23,'Icici Pru Icare','000000Ici3',5),(24,'Icici Pru Iprotect','000000Ici5',5),(25,'Icici Pru Iprotect Option I','000000Ici6',5),(26,'Icici Pru Iprotect Option I','000000Ici8',5),(27,'Icici Pru Life Stage Wealth II','00000Ici13',5),(28,'Icici Pru Lifetime Pension Max','00000Ici11',5),(29,'Icici Pru Smart Kid Premier','00000Ici12',5),(30,'Icici Pru Smartkid Maxima','000000Ici9',5),(31,'Icici Pru Wealth Builder','000000Ici7',5),(32,'Icici Prudential','0000000Ici',5),(33,'Ipru Icare Ii Rp','0000000Ipr',5);
/*!40000 ALTER TABLE `scheme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactiontype`
--

DROP TABLE IF EXISTS `transactiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactiontype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) DEFAULT NULL,
  `instrumentid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactiontype`
--

LOCK TABLES `transactiontype` WRITE;
/*!40000 ALTER TABLE `transactiontype` DISABLE KEYS */;
INSERT INTO `transactiontype` VALUES (1,'Cash Flow Statement',1),(2,'FD',3),(3,'Premium',4),(4,'Premium',5);
/*!40000 ALTER TABLE `transactiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usertype` tinyint(4) DEFAULT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `birthdate` datetime DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `stateid` int(11) DEFAULT NULL,
  `countryid` int(11) DEFAULT NULL,
  `postalcode` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `facebookid` varchar(255) DEFAULT NULL,
  `twitterhandle` varchar(255) DEFAULT NULL,
  `logoimageid` int(11) DEFAULT NULL,
  `profileimageid` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `modifieddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,NULL,'admin','admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-17 18:33:38

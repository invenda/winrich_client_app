ALTER TABLE `winrich_client`.`userdetails` 
ADD COLUMN `passport` VARCHAR(45) NULL AFTER `profileimageid`,
ADD COLUMN `aadhar` VARCHAR(45) NULL AFTER `passport`,
ADD COLUMN `voterid` VARCHAR(45) NULL AFTER `aadhar`,
ADD COLUMN `pf` VARCHAR(45) NULL AFTER `voterid`,
ADD COLUMN `ppf` VARCHAR(45) NULL AFTER `pf`,
ADD COLUMN `superannuation` VARCHAR(45) NULL AFTER `ppf`,
ADD COLUMN `pan` VARCHAR(45) NULL AFTER `superannuation`,
ADD COLUMN `drivinglicense` VARCHAR(45) NULL AFTER `pan`,
ADD COLUMN `pran` VARCHAR(45) NULL AFTER `drivinglicense`;


CREATE TABLE `winrich_client`.`bankaccount` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `clientid` INT(11) NULL,
  `primaryholder` VARCHAR(100) NULL,
  `jointholder` VARCHAR(100) NULL,
  `bankname` VARCHAR(100) NULL,
  `bankaddress` VARCHAR(255) NULL,
  `accountnum` VARCHAR(100) NULL,
  `ifsc` VARCHAR(45) NULL,
  `micr` VARCHAR(45) NULL,
  `isactive` VARCHAR(10) NULL,
  `createdate` DATETIME NULL,
  `modifieddate` DATETIME NULL,
  PRIMARY KEY (`id`));

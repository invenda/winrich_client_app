-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 14, 2017 at 10:53 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `winrich_client`
--

-- --------------------------------------------------------

--
-- Table structure for table `userimagedocument`
--

CREATE TABLE `userimagedocument` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL COMMENT 'refer to user table',
  `doctype` tinyint(4) NOT NULL COMMENT '1-profile, 2-pan, 3-adhar',
  `filelabel` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `createdby` int(11) DEFAULT NULL,
  `createddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `userimagedocument`
--
ALTER TABLE `userimagedocument`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userimgdoc_userid_fk1_idx` (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `userimagedocument`
--
ALTER TABLE `userimagedocument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `userimagedocument`
--
ALTER TABLE `userimagedocument`
  ADD CONSTRAINT `userimgdoc_userid_fk1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE userdetails ADD `userid` int(11) NOT NULL;
ALTER TABLE userdetails ADD CONSTRAINT `userdetail_userid_fk1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `winrich_client`.`userdetails` 
ADD INDEX `userdetail_stateid_fk2_idx` (`stateid` ASC),
ADD INDEX `userdetail_countryid_fk3_idx` (`countryid` ASC);
ALTER TABLE `winrich_client`.`userdetails` 
ADD CONSTRAINT `userdetail_stateid_fk2`
  FOREIGN KEY (`stateid`)
  REFERENCES `winrich_client`.`states` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `userdetail_countryid_fk3`
  FOREIGN KEY (`countryid`)
  REFERENCES `winrich_client`.`country` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

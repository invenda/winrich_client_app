<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class HeaderWidget extends Widget
{
    public $username;
	public $userId;

    public function init()
    {
        parent::init();
        if ($this->username === null) {
            $this->username = 'Client';
        }
    }

    public function run()
    {
        //return $this->render('head');
		return $this->render('head', [
				'username' => $this->username,
				'userId' => $this->userId,
			]);
    }
}
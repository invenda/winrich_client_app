<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Unittype;
use app\models\Commodity;
use app\models\Commoditytype;
use app\models\Transactiontype;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommodityController implements the CRUD actions for Commodity model.
 */
class CommodityController extends Controller
{
	var $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','create', 'update'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					],
				],
			],
			/*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],*/
        ];
    }

    /**
     * Lists all Commodity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $userId = Yii::$app->user->id;
		
		$dataProvider = Commodity::find()->where(['clientid'=>$userId])->asArray()->all();
		$commodityType = (new \yii\db\Query())->select('*')->from(['commoditytype'])->all();
		$unitType = (new \yii\db\Query())->select('*')->from(['unittype'])->all();
		$transType = (new \yii\db\Query())->select('*')->from(['transactiontype'])->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'commoditytype' => $commodityType,
			'unittype' => $unitType,
			'transtype' => $transType
        ]);
    }

    /**
     * Displays a single Commodity model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $clientid = (new \yii\db\Query())->select('commodity.clientid')->from('commodity')->where(['commodity.id'=>$id])->one();
		
		//print_r($this->findModelArray($id));die;
		if($clientid['clientid'] == Yii::$app->user->id){
			return $this->render('view', [
				'model' => $this->findModelArray($id),
			]);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Commodity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$createDate = $date->format('Y-m-d');
		
		$userId = Yii::$app->user->id;
		$model = new Commodity();
		
		if ($model->load(Yii::$app->request->post()) ) {
			
			if($_POST['Commodity']['clientid'] == $userId){
				//print_r($model);die;
				$model->createdate = $createDate;
				$model->save();
				return $this->redirect(['view', 'id' => $model->id]);
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        
		} else {
		
		//print_r($model->errors); die;
			$commodityType = Commoditytype::find()->asArray()->all();
			$unitType = Unittype::find()->asArray()->all();
			$transType = Transactiontype::find()->asArray()->all();
			return $this->render('create', [
                'model' => $model,
				'commoditytype' => $commodityType,
				'unittype' => $unitType,
				'transtype' => $transType,
            ]);
        }
    }

    /**
     * Updates an existing Commodity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$modifiedDate = $date->format('Y-m-d');
		
		$model = $this->findModel($id);
		$userId = Yii::$app->user->id;
		
        if ($model->load(Yii::$app->request->post())) {
            if($_POST['Commodity']['clientid'] == $userId){
				$model->modifieddate = $modifiedDate;
				$model->save();
				return $this->redirect(['view', 'id' => $model->id]);
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        } else {
			
            $record = $this->findModelArray($id);
			if($record['clientid'] == $userId){
				return $this->render('update', [
					'model' => $record,
				]);
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        }
    }

    /**
     * Deletes an existing Commodity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $clientid = (new \yii\db\Query())->select('commodity.clientid')->from('commodity')->where(['commodity.id'=>$id])->one();
		if($clientid['clientid'] == Yii::$app->user->id){
			$this->findModel($id)->delete();
			return $this->redirect(['index']);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Commodity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Commodity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Commodity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelArray($id)
    {
		$commodityType = new \yii\db\Expression('commoditytype.id');
		$unitType = new \yii\db\Expression('unittype.id');
		$transactionType = new \yii\db\Expression('transactiontype.id');
		
		$model = (new \yii\db\Query())->select(['commodity.id','commodity.clientid','commodity.commoditytypeid','commodity.unittypeid','commodity.quantity','commodity.amount','commodity.transactiontypeid','commodity.transactiondate','commodity.marketvalue','commodity.marketvaluedate','commodity.documentimageid','commodity.createdate','commodity.modifieddate','commoditytype.description as commoditydes','unittype.description as unitdes','transactiontype.description as transdes'])
		->from(['commodity','commoditytype','unittype','transactiontype'])
		->where(['commodity.id' => $id, 'commodity.commoditytypeid' => $commodityType, 'commodity.unittypeid' => $unitType, 'commodity.transactiontypeid' => $transactionType])->one();
		
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

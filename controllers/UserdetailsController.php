<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Userdetails;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\classes\MailGunApi;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * UserdetailsController implements the CRUD actions for Userdetails model.
 */
class UserdetailsController extends Controller
{
	var $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','create', 'update','getstates'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					],
				],
			],*/
			'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Userdetails models.
     * @return mixed
     */
    /*public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Userdetails::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Displays a single Userdetails model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		if($id == Yii::$app->user->id){
			$userDetails = $this->findModelArray($id);
			
			//if($userDetails['clientid'] == 'new'){
				
				//$userDetails2 = $this->findModelArray($id);
                //print_r($userDetails);die;
				//$state = (new \yii\db\Query())->select('*')->from(['states'])->where(['states.id' => $userDetails['stateid']])->one();
				//$country = (new \yii\db\Query())->select('*')->from(['country'])->where(['country.id' => $userDetails['countryid']])->one();
				
				//if ($state == false){$state = array('description' => '');}
				//if ($country == false){$country = array('description' => '');}
				
				//print_r($userDetails);die;
                if(isset($userDetails['userdetails']) && count($userDetails['userdetails']) > 0){
                  $model = $userDetails['userdetails'][0];
                }else{
                  $model = $userDetails['userdetails'];
                }
                //print_r($model['firstname']);die;
                //print_r($userDetails['id']);die;
				return $this->render('view', [
					'model' => $model,
					//'state' => $state,
					//'country' => $country,
                    'userid' => $id,
                    'userImage' => $userDetails['userimagedocuments'],
                    's3route' => Yii::$app->params['imgs3rootdir']
				]);
			//}else{
				
				/*$state = (new \yii\db\Query())->select('*')->from(['states'])->where(['states.id' => $userDetails['stateid']])->one();
				$country = (new \yii\db\Query())->select('*')->from(['country'])->where(['country.id' => $userDetails['countryid']])->one();
				
				if ($state == false){$state = array('description' => '');}
				if ($country == false){$country = array('description' => '');}
				
				//print_r($userDetails);die;
				return $this->render('view', [
					'model' => $userDetails,
					'state' => $state,
					'country' => $country
				]);*/
			//}
		}else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Userdetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new Userdetails();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing Userdetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $userId = (new \yii\db\Query())->select('*')->from('user')->where(['user.id'=>$id])->one();
		if($userId['id'] == Yii::$app->user->id){
            if (Yii::$app->request->post()) {
                $postData = Yii::$app->request->post();
                $postData['Userdetails']['userid'] = $id;
                $postData['Userdetails']['clientid'] = $userId['clientid'];
                $this->updateUserdetails($postData);
                if(isset($_FILES["images"])){                  
                  $uploadedFile = UploadedFile::getInstancesByName('images');
                  foreach($uploadedFile as $key => $val){
                    if($val->error == 0){
                      $contentType = $val->type;
                      $lastpos = strripos($val->type, '/');
                      $filetype = substr($val->type, $lastpos+1);
                      $filename = 'user/'.$id.'/'.date("y_m_d_H_i_s").'_'.rand(10,100).'.'.$filetype;
                      $this->uploadLogoToS3($val->tempName, $filename, $contentType);
                      $bodyJson['userid'] = $id;
                      $bodyJson['url'] = $filename;
                      if(isset($postData['Userdetails']['filelabel'])){
                        $bodyJson['filelabel'] = $postData['Userdetails']['filelabel'];
                      }
                      $this->updateImageDocument($bodyJson);
                    }
                  }
                  
                }
				return $this->redirect(['view', 'id' => $id]);
			} else {
				//echo "update"; die;
				$countrySelect = "";
				$stateSelect = "";
				$countryField = "";
				$stateField = "";
				$genderField = "";
				$z = "\"";
				
				$userDetails = $this->findModelArray($id);
                //print_r($userDetails);die;                
				/*$coun = (new \yii\db\Query())->select('*')->from(['country'])->->asArray()->all();
				$st = (new \yii\db\Query())->select('*')->from(['states'])->asArray()->all();*/
				$countries = (new \yii\db\Query())->select('*')->from(['country'])->all();
                $gender = array('Male', 'Female');
                
				//print_r($coun);die;
				/*if($coun == false){
					
					$countryField = "<option value=\"\">Select a Country..</option>";
					//print_r($conutryField); die;
					foreach($countries as $c){
						$countryField .= "<option value=\"".$c['id']."\">".$c['description']."</option>";
					}
					
				}else{
					
					$countryField = "<option value=\"\">Select a Country..</option>";
					
					foreach($countries as $c){
						if($c['id'] == $coun['id']){
							$countrySelect = "selected";
						}else{
							$countrySelect = "";
						}
						$countryField .= "<option value=\"".$c['id']."\" ".$countrySelect.">".$c['description']."</option>";
						$countrySelect = "";
					}
				}

				if($states == false){
					
					$stateField = "<option value=\"\">Select a State..</option>";
				
				}else{
					
					if($st == false){
						
						$stateField = "<option value=\"\">Select a State..</option>";
						foreach($states as $s){
							$stateField .= "<option value=\"".$s['id']."\">".$s['description']."</option>";
						}
						
					}else{
						
						foreach($states as $s){
							if($s['id'] == $st['id']){
								$stateSelect = "selected";
							}else{
								$stateSelect = "";
							}
							$stateField .= "<option value=\"".$s['id']."\" ".$stateSelect.">".$s['description']."</option>";
							$stateSelect = "";
						}
					}
					
				}*/
				/*
				if(empty($userDetails['gender'])){
					$genderField = "<option value=\"\">Select Gender..</option>";
					$genderField .= "<option value=\"Male\">Male</option>";
					$genderField .= "<option value=\"Female\">Female</option>";
					$genderField .= "<option value=\"Other\">Other</option>";
				}else{
					$genderField = "<option value=\"\">Select Gender..</option>";
					if($userDetails['gender'] == "Male"){
						$genderField .= "<option value=\"Male\" selected>Male</option>";
					}else{
						$genderField .= "<option value=\"Male\">Male</option>";
					}
					
					if($userDetails['gender'] == "Female"){
						$genderField .= "<option value=\"Female\" selected>Female</option>";
					}else{
						$genderField .= "<option value=\"Female\">Female</option>";
					}
					
					if($userDetails['gender'] == "Other"){
						$genderField .= "<option value=\"Other\" selected>Other</option>";
					}else{
						$genderField .= "<option value=\"Other\">Other</option>";
					}
					
				}*/
                //print_r($userDetails);die;
                $states = array();
                if(isset($userDetails['userdetails']) && count($userDetails['userdetails']) > 0){
                  $model = $userDetails['userdetails'][0];
                  
                if( isset($model['country']) && isset($model['country']['id'])){
                  $states = (new \yii\db\Query())->select('*')->from(['states'])->where(['countryid' => $model['country']['id']])->all();  
                }
				
				//print_r($states);die;
                }else{
                  $model = $userDetails['userdetails'];
                }
				return $this->render('update', [
					'model' => $model,
                    'userid' => $id,
                    'userImage' => $userDetails['userimagedocuments'],
					'country' => $countries,
					'state' => $states,
					'gender' => $gender,
                    's3route' => Yii::$app->params['imgs3rootdir']
				]);
			}
		}else {
			//print_r("test");die;
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Userdetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
		if($id == Yii::$app->user->id){
			$this->findModel($id)->delete();

			return $this->redirect(['index']);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Userdetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Userdetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
			if (($model = Userdetails::findOne($id)) !== null) {
				return $model;
			} else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
    }
	
	protected function findModelArray($id)
    {
      //(new \yii\db\Query())
      
		//print_r('client id: '.$id);
		$model = \app\models\User::find()/*->select(['userdetails.id','userdetails.clientid','userdetails.firstname','userdetails.lastname','userdetails.gender','userdetails.birthdate','userdetails.address1','userdetails.address2','userdetails.city','userdetails.stateid','userdetails.countryid','userdetails.postalcode','userdetails.phone','userdetails.mobile','userdetails.email','userdetails.website','userdetails.facebookid','userdetails.twitterhandle','userdetails.logoimageid','userdetails.profileimageid','userdetails.passport','userdetails.aadhar','userdetails.voterid','userdetails.pf','userdetails.ppf','userdetails.superannuation','userdetails.pan','userdetails.drivinglicense','userdetails.pran', 'userimagedocument.id', 'userimagedocument.url', 'userimagedocument.doctype'])*/
		//->from(['userdetails'])
		->where(['user.id' => $id])
        ->with(['userdetails'=> function($userdet){
            $userdet->with(['state'])->with(['country']);
        }])
        ->with(['userimagedocuments' => function($userimgdocquery){
                
         }])
         ->asArray()->all();
         // ->one();
		//print_r($model[0]['user']['userimagedocuments']);die;
        if ($model == null || count($model) == 0) {
            $model = new Userdetails();
			$model->clientid = $id;
			$model->save();
			return array('clientid' => 'new', 'id' => $model->id);
        } else {
			return $model[0];
        }
    }
	
	public function actionGetstates()
    {
		$id = $_POST['countryid'];
		//$id = 77;
		if (Yii::$app->request->isPost) {
			$states = (new \yii\db\Query())->select('*')->from(['states'])->where(['countryid' => $id])->all();
			
			if ($states == false) {
				$response= json_encode(
					array(
						'status' => 'fail',
						'message' => 'no states found'
					)
				);
				return $response;
			}else {
				$response= json_encode(
					array(
						'status' => 'success',
						'data' => $states
					)
				);				
				
				return $response;
			}
			
		}else {
				$response= json_encode(
					array(
						'status' => 'fail',
						'message' => 'bad request'
					)
				);
				return $response;
		}
	}
	
	public function actionChangepassword()
    {
		date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$year = $date->format('Y');
		
			if (Yii::$app->request->post()) {
				
				$userId = $_POST['clientid'];
				
				if($userId == Yii::$app->user->id){
				
					$salt = Yii::$app->params['salt'];
					$oldPassword = $_POST['oldpassword'];
					$newPassword = $_POST['newpassword'];
					$newPasswordRepeat = $_POST['newpasswordrepeat'];
					
					$newPasswordSalt = $newPassword;
					$newPasswordSalt .= $salt;
					$newPassEncrypt = openssl_digest($newPasswordSalt, 'sha512');
					
					if($newPassword != $newPasswordRepeat){
						throw new NotFoundHttpException('Your passwords do not match.');
					}
					
					$oldPasswordSalt = $oldPassword.$salt;
					$oldPasswordHash = openssl_digest($oldPasswordSalt, 'sha512');
					
					$usernameCheck = (new \yii\db\Query())->select('*')->from('user')->where(['user.id'=>$userId, 'user.isactive' =>'1'])->one();
					//print_r($oldPasswordSalt);die;
					if(!empty($usernameCheck)){
						
						if($usernameCheck['password'] == $oldPasswordHash && $newPassEncrypt != $oldPasswordHash){
							
							try {
								$setPassword = Yii::$app->db->createCommand()->update('user', ['password' => $newPassEncrypt], 'user.id ='.$userId)->execute();
							}catch (\Exception $e){
								$message = "<div style=\"color:red !important;\"><p><center>Your password could not be reset.<center></p></div>";
								return $this->render('changepassword',[
									'userId' => Yii::$app->user->id,
									'message' => $message,
								]);
							}
							
							$mailGun = new MailGunApi();
							$subject = "Winrich Client Portal - Password reset";
							$textContent = $this->renderPartial('/emailtemplates/changepassword', [
								'username' => $usernameCheck['username'],
								'year' => $year,
							]);
							$fromAddress = Yii::$app->params['mailsendfrom'] ;
							$toAddress  = $usernameCheck['username'];
							$send = $mailGun->sendmail($subject, $textContent, $fromAddress, $toAddress);
							$send = json_decode($send);
						
							$message = "<div style=\"color:green !important;\"><p><center>Your password has been successfully reset. Check email for Confirmation.</a><center></p></div>";
							return $this->render('changepassword',[
								'userId' => Yii::$app->user->id,
								'message' => $message,
							]);
							
							
						}else{
							
							$message = "<div style=\"color:red !important;\"><p><center>Your old password doesn't match.<center></p></div>";
							return $this->render('changepassword',[
								'userId' => Yii::$app->user->id,
								'message' => $message,
							]);
						}
					}else{
						
						$message = "<div style=\"color:red !important;\"><p><center>Unable to contact the server, Try again later.<center></p></div>";
								return $this->render('changepassword',[
									'userId' => Yii::$app->user->id,
									'message' => $message,
								]);
					}
				
				}else{
					$message = "<div style=\"color:red !important;\"><p><center>Something is wrong with the server request.<center></p></div>";
								return $this->render('changepassword',[
									'userId' => Yii::$app->user->id,
									'message' => $message,
								]);
				}	
			}else{
				return $this->render('changepassword',[
					'userId' => Yii::$app->user->id,
				]);
			}
	}
  
    public function uploadLogoToS3($img, $filename, $contentType){
      $s3 = Yii::$app->get('s3');
      $result = $s3->commands()->upload($filename, $img)->withContentType($contentType)->execute();
    }
  
    private function updateImageDocument($bodyJson){
      $values = [];
      $values['doctype'] = 1;//$bodyJson['doctype']; ToDo: take from client
      $userimgdoc = new \app\models\Userimagedocument();
      /*if(isset($bodyJson['id'])){ // ToDo: update later
        $id = $bodyJson['id'];
        $userimgdoc = $userimgdoc->findOne($id);  
        $values['userid'] = $bodyJson['userid'];
        $values['doctype'] = $bodyJson['doctype'];
      }*/
      if(isset($bodyJson['filelabel'])){
        $values['filelabel'] = $bodyJson['filelabel'];  
      }
      $values['userid'] = $bodyJson['userid'];
      $values['url'] = $bodyJson['url'];
      $userimgdoc->attributes = $values;
      $status = $userimgdoc->save();
    }
  
    private function updateUserdetails($bodyJson){
      $userdetails = new \app\models\Userdetails();
      if($usrdt = $userdetails->findOne(['userid' => $bodyJson['Userdetails']['userid']])){
        $userdetails = $usrdt;
      }
      $userdetails->attributes = $bodyJson['Userdetails'];
      $status = $userdetails->save();
    }
  
}

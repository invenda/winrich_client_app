<?php

namespace app\controllers;


use Yii;
use app\classes\RestApi;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\classes\MailGunApi;
use app\classes\Tools;

class SiteController extends Controller
{
	var $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','create', 'update'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					],
				],
			],
			
			'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get','post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        date_default_timezone_set("Asia/Kolkata");
		$toDate = new \DateTime('now');
		$fromDate = clone $toDate;
		$toCommodity = clone $toDate;
		
		$fromDate->modify('-2 months');
		$toCommodity->modify('-5 days');
		
		$to = $toDate->format('Y-m-d');
		$from = $fromDate->format('Y-m-d');
		$toComm = $fromDate->format('Y-m-d');
		$quandl_apiKey = "G1U7hRxzeZbyoc1wFEi7";
				
		
		if (Yii::$app->user->isGuest)
			
			$this->redirect('site/login');
        
		else{
		
			$restApi = new RestApi();

			$userId = Yii::$app->user->getIdentity()->clientid;
			
			if(empty($userId)){
				$userId = 0;
			}
			
			$aum = $restApi->get('/users/getLastFourWeekAumByClientIds', ['clientIds' => $userId,'fromDate' => $from, 'toDate' => $to]);
			//Parsing
			//$aum = $aum->aum; foreach($aum as $a)
			//$aumCreateDate = $a->createdate; $aumTotal = $a->clients[0]->total;
			//print_r($aum->aum[0]->clients[0]->total); die;
			
			$sip = $restApi->get('/users/getLastFourWeekSipColumnChartByClientIds', ['clientIds' => $userId,'fromDate' => $from, 'toDate' => $to]);
			//Parsing
			//$sip = $sip->sip; foreach($sip as $s)
			//$sipCreateDate = $s->createdate; sipTotal = $s->total;
			
			$investment = $restApi->get('/users/getLastFourWeekInvestmentColumnChartByClientIds', ['clientIds' => $userId,'fromDate' => $from, 'toDate' => $to]);
			//Parsing
			//$aum = $investment->investment; foreach($investment as $in)
			//$investmentCreateDate = $in->createdate; $investmentTotal = $in->clients[0]->total;
			
			//print_r($investment); die;
			
			//$restApi2 = new RestApi("http://www.google.com");
			//$bse = $restApi2->get('/finance/info',['client'=>'g','q'=>'INDEXBOM:SENSEX']);
			//$usd = $restApi2->get('/finance/info',['client'=>'g','q'=>'CURRENCY:USDINR']);
			
			//$restApi3 = new RestApi("https://www.quandl.com");
			//$gold = $restApi3->get('/api/v3/datasets/CHRIS/MCX_GC1.json',['api_key'=>$quandl_apiKey,'start_date'=>$toComm]);
			
			
			
			//echo "<pre>".$gold->dataset->data[0][3]."</pre>"; die;
			//echo "<pre>".$bse->l_fix."</pre>"; die;
			//echo "<pre>".$usd->l."</pre>"; die;
			
			return $this->render('index',[
				//'aum' => $aum->aum,
				//'sip' => $sip->sip,
				//'investment' => $investment->investment,
				//'bse' => $bse->l_fix,
				//'usd' => $usd->l,
				//'gold' => $gold->dataset->data[0][3],
			]);
        
		}
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {	
	
		$this->layout = false;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
		$salt = Yii::$app->params['salt'];
	
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $password = $_POST['LoginForm']['password'].$salt;
			$model->password = openssl_digest($password, 'sha512');
			if ($model->login()){
				return $this->goBack();
			}else{
				$message = "<div style=\"color:red !important;\"><p><center>Either your username or password doesn't match<center></p></div>";
				return $this->render('login', [
					'model' => $model,
					'username' => $_POST['LoginForm']['username'],
					'message' => $message
				]);
			}
        }
		
		return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	
    public function actionSignup()
    {
		date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$year = $date->format('Y');
		
		$this->layout = false;
        if (Yii::$app->user->isGuest) {        
		
			if (Yii::$app->request->post()) {
				
				$email = $_POST['LoginForm']['username'];
				
				if(empty($email)) {
					$message = "<div style=\"color:red !important;\"><p><center>Cannot proceed without an email address<center></p></div>";
							return $this->render('signup',[
								'message' => $message,
							]);
				}
				
				if(empty($_POST['LoginForm']['terms'])) {
					$message = "<div style=\"color:red !important;\"><p><center>Cannot proceed without accepting terms and conditions<center></p></div>";
							return $this->render('signup',[
								'message' => $message,
							]);
				}
			
				
				$usernameCheck = (new \yii\db\Query())->select('*')->from('user')->where(['user.username'=>$email, 'user.isactive' =>'0'])->one();


				if($usernameCheck !== false){
					
					if($usernameCheck['username'] == $email){
						
						$userId = $usernameCheck['id'];
						$length = 10;
						$tools = new Tools;
						$newPassword = $tools-> generateRandomString($length);
						$salt = Yii::$app->params['salt'];
						$newPasswordSalt = $newPassword;
						$newPasswordSalt .= $salt;
						$newPassEncrypt = openssl_digest($newPasswordSalt, 'sha512');
						
						$setPassword = Yii::$app->db->createCommand()->update('user', ['password' => $newPassEncrypt, 'isactive' => '1', 'termsaccept' => '1'], 'user.id ='.$userId)->execute();
						
						$mailGun = new MailGunApi();
						$subject = "Welcome to Winrich Client Portal. Here's your temporary Password";
						$textContent = $this->renderPartial('/emailtemplates/signup', [
							'username' => $usernameCheck['username'],
							'password' => $newPassword,
							'year' => $year,
						]);
						$fromAddress = Yii::$app->params['mailsendfrom'] ;
						$toAddress  = $usernameCheck['username'];
						$send = $mailGun->sendmail($subject, $textContent, $fromAddress, $toAddress);
						$send = json_decode($send);
						
						if($send[0] == "success"){
							$message = "<div style=\"color:red !important;\"><p><center>If you are enrolled, you will receive an email shortly for futher action. Else contact <a href=\"mailto:knsridharan@winrich.in\">knsridharan@winrich.in</a><center></p></div>";
							return $this->render('signup',[
								'message' => $message,
							]);
						}else{
							$message = "<div style=\"color:red !important;\"><p><center>Error contacting the server, Try again later. Else contact <a href=\"mailto:knsridharan@winrich.in\">knsridharan@winrich.in</a><center></p></div>";
							return $this->render('signup',[
								'message' => $message,
							]);
						}
						
					}else{
						
						$message = "<div style=\"color:red !important;\"><p><center>Error contacting the server, Try again later. Else contact <a href=\"mailto:knsridharan@winrich.in\">knsridharan@winrich.in</a><center></p></div>";
						return $this->render('signup',[
							'message' => $message,
						]);
					}
				}else{
					
					$message = "<div style=\"color:red !important;\"><p><center>Unable to contact the server, Try again later.<center></p></div>";
							return $this->render('signup',[
								'message' => $message,
							]);
				}
				
				
			}else{
				return $this->render('signup');
			}
		}else{
			return $this->goHome();
		}
	}
	
	public function actionForgotpassword()
    {
		date_default_timezone_set("Asia/Kolkata"); 
		$date = new \DateTime('+15 minutes');
		$year = $date->format('Y');
		$time = $date->format('Y-m-d H:i:s');
		$this->layout = false;
        if (Yii::$app->user->isGuest) {        
		
			if (Yii::$app->request->post()) {
				
				$email = $_POST['LoginForm']['username'];
				
				$usernameCheck = (new \yii\db\Query())->select('*')->from('user')->where(['user.username'=>$email, 'user.isactive' =>'1'])->one();
				
				if(!empty($usernameCheck)){
					
					if($usernameCheck['username'] == $email){
						
						$userId = $usernameCheck['id'];
						$length = 15;
						$tools = new Tools;
						$token = $tools-> generateRandomString($length);
						$salt = Yii::$app->params['passwordresetsalt'];
						$tokenSalt = $token.$salt;
						$tokenEncrypt = openssl_digest($tokenSalt, 'sha512');
						
						$deactivateTokens = Yii::$app->db->createCommand()->update('passwordreset', ['status' => 0], 'passwordreset.clientid ='.$userId)->execute();
						
						$setToken = Yii::$app->db->createCommand()->insert('passwordreset', ['clientid' => $userId,'token' => $tokenEncrypt, 'status' => '1', 'expirydate' => $time])->execute();
						
						$mailGun = new MailGunApi();
						$subject = "Winrich Client Portal. Here's your Reset Password link";
						$textContent = $this->renderPartial('/emailtemplates/forgotpassword', [
							'username' => $usernameCheck['username'],
							'token' => $tokenEncrypt,
							'year' => $year,
						]);
						$fromAddress = Yii::$app->params['mailsendfrom'] ;
						$toAddress  = $usernameCheck['username'];
						$send = $mailGun->sendmail($subject, $textContent, $fromAddress, $toAddress);
						$send = json_decode($send);
						
						if($send[0] == "success" && $setToken){
							$message = "<div style=\"color:green !important;\"><p><center>A password reset link is sent to your email.<center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
						}else{
							$message = "<div style=\"color:red !important;\"><p><center>Error contacting the server, Try again later. Else contact <a href=\"mailto:knsridharan@winrich.in\">knsridharan@winrich.in</a><center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
						}
						
					}else{
						
						$message = "<div style=\"color:red !important;\"><p><center>Error contacting the server, Try again later, else contact <a href=\"mailto:knsridharan@winrich.in\">knsridharan@winrich.in</a><center></p></div>";
						return $this->render('forgotpassword',[
							'message' => $message,
						]);
					}
				}else{
					
					$message = "<div style=\"color:red !important;\"><p><center>Unable to contact the server, Try again later.<center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
				}
				
				
			}else{
				return $this->render('forgotpassword');
			}
		}else{
			return $this->goHome();
		}
	}
	
	public function actionResetpassword($token)
    {
		date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$year = $date->format('Y');
		$time = $date->format('Y-m-d H:i:s');
		$this->layout = false;
        if (Yii::$app->user->isGuest) {        
		
			if (Yii::$app->request->post()) {
				
				$newPassword = $_POST['newpassword'];
				$newPasswordRepeat = $_POST['newpasswordrepeat'];
				
				$salt = Yii::$app->params['salt'];
				$newPasswordSalt = $newPassword;
				$newPasswordSalt .= $salt;
				$newPassEncrypt = openssl_digest($newPasswordSalt, 'sha512');
				
				if($newPassword != $newPasswordRepeat){
						$deactivateTokens = Yii::$app->db->createCommand()->update('passwordreset', ['status' => 0], 'passwordreset.clientid ='.$userId)->execute();
						$message = "<div style=\"color:red !important;\"><p><center>Your passwords do not match. Please request another reset password link.<center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
				}
				
				$tokenCheck = (new \yii\db\Query())->select('*')->from('passwordreset')->where(['passwordreset.token'=>$token, 'passwordreset.status' =>'1'])->andWhere(['>=','expirydate', $time])->one();
				
				if(!empty($tokenCheck)){
					
					if($tokenCheck['token'] == $token){
						
						$userId = $tokenCheck['clientid'];
						
						$deactivateTokens = Yii::$app->db->createCommand()->update('passwordreset', ['status' => 0], 'passwordreset.clientid ='.$userId)->execute();
						
						try {
							$resetPass = Yii::$app->db->createCommand()->update('user', ['password' => $newPassEncrypt], 'user.id ='.$userId)->execute();
						}catch (\Exception $e){
							$message = "<div style=\"color:red !important;\"><p><center>Your password coudn't be reset. Please request a new reset password link.<center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
						}
						//echo $resetPass;die;
						
							$message = "<div style=\"color:green !important;\"><p><center>Your password has been reset.<center></p></div>";
							return $this->render('login',[
								'message' => $message,
							]);
						
					}else{
						
						$message = "<div style=\"color:red !important;\"><p><center>Looks like your reset link is tampered with. Please request a new reset password link.<center></p></div>";
						return $this->render('forgotpassword',[
							'message' => $message,
						]);
					}
				}else{
					
					$message = "<div style=\"color:red !important;\"><p><center>Your password reset request not found. Please request a new reset password link.<center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
				}
				
				
			}else{
				$tokenCheck = (new \yii\db\Query())->select('*')->from('passwordreset')->where(['passwordreset.token'=>$token, 'passwordreset.status' =>'1'])->andWhere(['>=','expirydate', $time])->one();
				//print_r($token);die;
				if(!empty($tokenCheck)){
					
					if($tokenCheck['token'] == $token){
						
						$userId = $tokenCheck['clientid'];
						
						return $this->render('resetpassword',[
							'token' => $token,
							]);
						
					}else{
						
						$message = "<div style=\"color:red !important;\"><p><center>Your password reset request not found. Please request a new reset password link.<center></p></div>";
						return $this->render('forgotpassword',[
							'message' => $message,
						]);
					}
				}else{
					
					$message = "<div style=\"color:red !important;\"><p><center>Your reset password link looks to be unavailable or expired. Please request another one.<center></p></div>";
							return $this->render('forgotpassword',[
								'message' => $message,
							]);
				}
			}
		}else{
			return $this->goHome();
		}
	}
	
	public function actionTerms()
    {	
		$this->layout = false;
		return $this->render('terms');
		
	}

	public function actionFaq()
    {	
		return $this->render('faq');
		
	}
	
	public function actionClientsync()
    {	
		date_default_timezone_set("Asia/Kolkata"); 
		$date = new \DateTime('now');
		$currentTime = $date->format('Y-m-d H:i:s');
		
		$this->layout = false;
		$db = Yii::$app->params['reportsdb'];;
		$username = Yii::$app->params['reportsdbuser'];;
		$password = Yii::$app->params['reportsdbpass'];;
		
		$connection = new \yii\db\Connection([
			'dsn' => $db,
			'username' => $username,
			'password' => $password,
		]);
		$connection->open();
		
		$command = $connection->createCommand('SELECT * FROM clients');
		$users = $command->queryAll();
		//print_r($posts);die;
		
		foreach($users as $u){
			$usernameCheck = (new \yii\db\Query())->select('*')->from('user')->where(['user.clientid'=>$u['id']])->one();
			if($usernameCheck === false){
				$insert = Yii::$app->db->createCommand()->insert('user', [
					'clientclass' => $u['clientclass'],
					'clientname' => $u['clientname'],
					'clientstatus' => $u['clientstatus'],
					'username' => $u['email'],
					'clientid' => $u['id'],
					'usertype' => '2',
					'address' => $u['address'],
					'street' => $u['street'],
					'area' => $u['area'],
					'city' => $u['city'],
					'pincode' => $u['pincode'],
					'stateid' => $u['stateid'],
					'phone1' => $u['phone1'],
					'phone2' => $u['phone2'],
					'mobile' => $u['mobile'],
					'pan' => $u['pan'],
					'email' => $u['email'],
					'birthdate' => $u['birthdate'],
					'anniversarydate' => $u['anniversarydate'],
					'bankname' => $u['bankname'],
					'rnname' => $u['rnname'],
					'isactive' => '0',
					'termsaccept' => '0',
					'hcode' => $u['hcode'],
					'createdate' => $currentTime,
				])->execute();
				
			}
		}
		
		return "User table updated";
	
	}
	
	public function actionSubscriberdetail()
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$createDate = $date->format('Y-m-d');
		
		$userId = Yii::$app->user->getIdentity()->username;
		
		if ($userId == "knsridharan@winrich.in" || $userId == "ashok.bangalore@gmail.com" || $userId == "rishiomshah@gmail.com") {
			
			return $this->render('subscriberdetail', [
                'model' => ''
            ]);
        
		} else {
		
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionSubscriberenquiry()
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$createDate = $date->format('Y-m-d');
		
		$userId = Yii::$app->user->getIdentity()->username;
		
		if (Yii::$app->request->isPost) {
			
			if ($userId == "knsridharan@winrich.in" || $userId == "ashok.bangalore@gmail.com" || $userId == "rishiomshah@gmail.com") {
			
			$usernames = $_POST['usernames'];
			$usernames = explode(',',$usernames);
			$usernameCheck = [];
			$count = 1;
			$statusText = "";
			foreach($usernames as $user){
				
				$u = trim($user);
				$username = (new \yii\db\Query())->select('*')->from(['user'])->where(['user.username' => $u])->one();
				if(!empty($username)){
					//$usernameCheck['']
					if($username['isactive'] == 0){
						$statusText = "Inactive";
					}elseif($username['isactive'] == 1){
						$statusText = "Active";
					}else{
						$statusText = "Deactive";
					}
					$usernameCheck[] = array(
						'serialnum' => $count++,
						'available' => 'Available',
						'id' => $username['id'],
						'username' => $username['username'],
						'isactive' => $statusText,
					);
				}else{
					$usernameCheck[] = array(
						'serialnum' => $count++,
						'available' => 'Not available',
						'id' => '',
						'username' => $user,
						'isactive' => '',
					);
				}

			}
			
			$response= json_encode(
					array(
						'status' => 'success',
						'data' => $usernameCheck
					)
				);
				return $response;
			
			        
			}else{
			
				$response= json_encode(
					array(
						'status' => 'fail',
						'message' => 'action not allowed for user'
					)
				);
				return $response;
			}
			
		}else {
				$response= json_encode(
					array(
						'status' => 'fail',
						'message' => 'bad request'
					)
				);
				return $response;
		}
	}
	
	public function actionSubscriberstatuschange()
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$createDate = $date->format('Y-m-d');
		
		$userId = Yii::$app->user->getIdentity()->username;

		if (Yii::$app->request->isPost) {
			
			if ($userId == "knsridharan@winrich.in" || $userId == "ashok.bangalore@gmail.com" || $userId == "rishiomshah@gmail.com") {
			
				$user = $_POST['id'];
				$isactive = $_POST['isactive'];
				
				try{
					$userUpdate = Yii::$app->db->createCommand()->update('user', ['isactive' => $isactive], 'user.id ='.$user)->execute();
				}catch(Exception $e){
					$response= json_encode(
						array(
							'status' => 'fail',
							'message' => $e
						)
					);
					return $response;
				}
				if($userUpdate >= 0){
					if($isactive == 0){
						$statusText = "Inactive";
					}elseif($isactive == 1){
						$statusText = "Active";
					}else{
						$statusText = "Deactive";
					}
					
					$response= json_encode(
						array(
							'status' => 'success',
							'isactive' => $statusText,
							'message' => 'number of users updated - '.$userUpdate
						)
					);
					return $response;
				}else{
					$response= json_encode(
						array(
							'status' => 'fail',
							'message' => 'user status couldn\'t be updated'
						)
					);
				}

				$response= json_encode(
					array(
						'status' => 'success',
						'data' => $usernameCheck
					)
				);
				return $response;
			
			        
			}else{
			
				$response= json_encode(
					array(
						'status' => 'fail',
						'message' => 'action not allowed for user'
					)
				);
				return $response;
			}
			
		}else {
				$response= json_encode(
					array(
						'status' => 'fail',
						'message' => 'bad request'
					)
				);
				return $response;
		}
	}
		
}
	
	/*public function actionTest()
    {	
	
		$connection = new \yii\db\Connection([
			'dsn' => 'mysql:host=127.0.0.1;dbname=font',
			'username' => 'root',
			'password' => '',
		]);
		
		$connection->open();
		$html = "";
		$command = $connection->createCommand('SELECT * FROM three');
		$fonts = $command->queryAll();
		foreach($fonts as $f){
			$html .= "<div class=\"col-md-4 col-sm-6 col-lg-3 col-print-4\">
					  <i class=\"fa ".$f['icon']."\" aria-hidden=\"true\" title=\"".$f['icon']."\"></i>
						".$f['icon']."  
					  <span class=\"text-muted\"></span>
					  </div>";
		}
		
		//print_r($fonts);die;
        return $this->render('test',['html' => $html]);
    }*/


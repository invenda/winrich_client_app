<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Assets;
use app\models\Instrument;
use app\models\Arn;
use app\models\Scheme;
use app\models\Transactiontype;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * AssetsController implements the CRUD actions for Assets model.
 */
class AssetsController extends Controller
{
	var $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],*/
			
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','create', 'update','delete'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					],
				],
			],
        ];
    }

    /**
     * Lists all Assets models.
     * @return mixed
     */
    public function actionIndex($instrumentId)
    {
		$userId = Yii::$app->user->id;
		
		$dataProvider = Assets::find()->where(['instrumentid'=>$instrumentId, 'clientid'=>$userId])->asArray()->all();

		$instrument = Instrument::find()->where(['id'=>$instrumentId])->asArray()->all();
		
        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'instrument' => $instrument,
        ]);
    }

    /**
     * Displays a single Assets model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        //print_r($this->findModelArray($id)); return;
		$clientid = (new \yii\db\Query())->select('assets.clientid')->from('assets')->where(['assets.id'=>$id])->one();
		if($clientid['clientid'] == Yii::$app->user->id){
			$model = $this->findModelArray($id);
			return $this->render('view', [
				'model' => $model,
				'assetid' => $id,
			]);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
		
    }

    /**
     * Creates a new Assets model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($instrumentId)
    {
		//print_r($_POST);die;
		

        $model = new Assets();

        if ($model->load(Yii::$app->request->post()) ) {
			
			$clientid = $_POST['Assets']['clientid'];
			if($clientid == Yii::$app->user->id){
				$model->save();
				//print_r($model->id);die;
				return $this->redirect(Url::to(['assets/view', 'id' => $model->id]));
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        
		} else {
		
		//print_r($model->errors); die;
			$instrument = Instrument::find()->where(['id'=>$instrumentId])->asArray()->all();
			$arn = Arn::find()->where(['instrumentid'=>$instrumentId])->asArray()->all();
			$transactionType = Transactiontype::find()->where(['instrumentid'=>$instrumentId])->asArray()->all();
			$scheme = Scheme::find()->where(['instrumentid'=>$instrumentId])->asArray()->all();
            return $this->render('create', [
                'model' => $model,
				'instrument' => $instrument,
				'arn' => $arn,
				'transactionType' => $transactionType,
				'scheme' => $scheme,
            ]);
        }
    }

    /**
     * Updates an existing Assets model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$clientid = (new \yii\db\Query())->select('assets.clientid')->from('assets')->where(['assets.id'=>$id])->one();
		if($clientid['clientid'] == Yii::$app->user->id){
			$model = $this->findModel($id);

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(Url::to(['assets/view', 'id' => $model->id]));
			} else {
				return $this->render('update', [
					'model' => $this->findModelArray($id),
					'assetid' => $id,
				]);
			}
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Assets model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
		
		$clientid = (new \yii\db\Query())->select('assets.clientid')->from('assets')->where(['assets.id'=>$id])->one();
		if($clientid['clientid'] == Yii::$app->user->id){
			
			$instrumentId =(new \yii\db\Query())->select('assets.instrumentid')->from('assets')->where(['assets.id'=>$id])->one();
			$this->findModel($id)->delete();

			return $this->redirect(Url::to(['assets/index', 'instrumentId' => $instrumentId['instrumentid']]));
			
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Assets model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Assets the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Assets::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelArray($id)
    {
		$instrumentid = new \yii\db\Expression('instrument.id');
		$arnid = new \yii\db\Expression('arn.id');
		$schemeid = new \yii\db\Expression('scheme.id');
		$transactiontypeid = new \yii\db\Expression('transactiontype.id');
		
		$model = (new \yii\db\Query())->select(['assets.id','assets.instrumentid','assets.folio','assets.description','assets.clientid','assets.amount','assets.multipledays','assets.frequencyid','assets.startdate','assets.enddate','instrument.description as instrumentdes','arn.description as arndes', 'scheme.description as schemedes','scheme.code as schemecode','transactiontype.description as transdes'])
		->from(['assets','instrument','arn','scheme','transactiontype'])
		->where(['assets.id' => $id, 'assets.instrumentid' => $instrumentid, 'assets.arnid' => $arnid, 'assets.schemeid' => $schemeid, 'assets.transactiontypeid' => $transactiontypeid])->one();
		
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

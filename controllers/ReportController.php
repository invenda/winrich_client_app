<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use app\models\Bond;
use app\models\Commodity;
use app\models\Commoditytype;
use app\models\Equity;
use app\models\Fixeddeposit;
use app\models\Insurance;
use app\models\Insurancetype;
use app\models\Mutualfund;
use app\models\Realestate;
use app\models\Transactiontype;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;


/**
 * FixeddepositController implements the CRUD actions for Fixeddeposit model.
 */
class ReportController extends Controller
{
	var $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
				'class' => AccessControl::className(),
				'only' => ['consolidated','index','view','create', 'update'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					],
				],
			],
			/*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],*/
        ];
    }

    /**
     * Lists all Fixeddeposit models.
     * @return mixed
     */
    public function actionConsolidated()
    {
        $userId = Yii::$app->user->id;
		
		$bondBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['bond'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
		$bondSellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['bond'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
		//print_r($bondBuySum);die;		
		$commodityBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['commodity'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
		$commoditySellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['commodity'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
		
		$equityBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['equity'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
		$equitySellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['equity'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
		
		$fdSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['fixeddeposit'])->where(['clientid' => $userId])->one();
		
		$insuranceSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['insurance'])->where(['clientid' => $userId])->one();
		
		$mfBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['mutualfund'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
		$mfSellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['mutualfund'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
		
		$realestateBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['realestate'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
		$realestateSellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['realestate'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
		
		/*$bond = Bond::find()->where(['clientid'=>$userId])->asArray()->all();
		$commodity = Commodity::find()->where(['clientid'=>$userId])->asArray()->all();
		$equity = Equity::find()->where(['clientid'=>$userId])->asArray()->all();
		$fd = Fixeddeposit::find()->where(['clientid'=>$userId])->asArray()->all();
		$insurance = Insurance::find()->where(['clientid'=>$userId])->asArray()->all();
		$mf = Mutualfund::find()->where(['clientid'=>$userId])->asArray()->all();
		$realestate = Realestate::find()->where(['clientid'=>$userId])->asArray()->all();
		$transType = (new \yii\db\Query())->select('*')->from(['transactiontype'])->all();
		$commodityType = (new \yii\db\Query())->select('*')->from(['commoditytype'])->all();
		$insuranceType = (new \yii\db\Query())->select('*')->from(['insurancetype'])->all();*/

        return $this->render('consolidated', [
            
            'bondBuySum' => $bondBuySum,
            'bondSellSum' => $bondSellSum,
			'commodityBuySum' => $commodityBuySum,
            'commoditySellSum' => $commoditySellSum,
            'equityBuySum' => $equityBuySum,
            'equitySellSum' => $equitySellSum,
            'fdSum' => $fdSum,
            'insuranceSum' => $insuranceSum,
            'mfBuySum' => $mfBuySum,
            'mfSellSum' => $mfSellSum,
            'realestateBuySum' => $realestateBuySum,
            'realestateSellSum' => $realestateSellSum,
            /*'bond' => $bond,
            'commodity' => $commodity,
            'equity' => $equity,
            'fd' => $fd,
            'insurance' => $insurance,
            'mf' => $mf,
            'realestate' => $realestate,
            'transType' => $transType,
            'commodityType' => $commodityType,
            'insuranceType' => $insuranceType,*/
			
        ]);
    }

     public function actionDownloadreport(){   

        $userId = Yii::$app->user->id;
        
        $bondBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['bond'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
        $bondSellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['bond'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
        //print_r($bondBuySum);die;     
        $commodityBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['commodity'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
        $commoditySellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['commodity'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
        
        $equityBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['equity'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
        $equitySellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['equity'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
        
        $fdSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['fixeddeposit'])->where(['clientid' => $userId])->one();
        
        $insuranceSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['insurance'])->where(['clientid' => $userId])->one();
        
        $mfBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['mutualfund'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
        $mfSellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['mutualfund'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();
        
        $realestateBuySum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['realestate'])->where(['transactiontypeid' => 1, 'clientid' => $userId])->one();
        $realestateSellSum = (new \yii\db\Query())->select('count(*),sum(amount)')->from(['realestate'])->where(['transactiontypeid' => 2, 'clientid' => $userId])->one();

        $content = $this->renderPartial('consolidated',[

            'bondBuySum' => $bondBuySum,
            'bondSellSum' => $bondSellSum,
            'commodityBuySum' => $commodityBuySum,
            'commoditySellSum' => $commoditySellSum,
            'equityBuySum' => $equityBuySum,
            'equitySellSum' => $equitySellSum,
            'fdSum' => $fdSum,
            'insuranceSum' => $insuranceSum,
            'mfBuySum' => $mfBuySum,
            'mfSellSum' => $mfSellSum,
            'realestateBuySum' => $realestateBuySum,
            'realestateSellSum' => $realestateSellSum,

            ]);  

        echo $content;

       // $content="this is test"; 

         // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => [
                'title' => 'Assets Report - client.winrich.in',
                'subject' => 'Report'
            ],
            'methods' => [
                'SetHeader' => ['Generated By: winrich.in ||Generated On: ' . date("r")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        //header('Content-Disposition: attachment;filename="filename.pdf"');
        // return the pdf output as per the destination setting

        //header('Content-Type: application/vnd.ms-excel');
        //header('Content-Disposition: attachment;filename="client_report.xls"');        

       
       // header("Pragma: no-cache");
       // header("Expires: 0");

        return $pdf->render();

    } 

	 /*public function actionDownloadreport()
	{
		$userId = Yii::$app->user->id;

        $transType = (new \yii\db\Query())->select('*')->from(['transactiontype'])->all();
        $commodityType = (new \yii\db\Query())->select('*')->from(['commoditytype'])->all();
        $insuranceType = (new \yii\db\Query())->select('*')->from(['insurancetype'])->all();
        $unitType = (new \yii\db\Query())->select('*')->from(['unittype'])->all();

        $bond = (new \yii\db\Query())->select(['issuername','scrip','units','amount','beneficiaryname','transactiondate','applicationnum','transactiontypeid'])->from(['bond'])->where(['clientid'=>$userId])->all();
        $commodity = (new \yii\db\Query())->select(['commoditytypeid','unittypeid','quantity','amount','transactiontypeid','transactiondate','marketvalue','marketvaluedate'])->from(['commodity'])->where(['clientid'=>$userId])->all();
        $equity = (new \yii\db\Query())->select(['scripname','quantity','rate','amount','transactiondate','demataccountnum','marketvalue','marketvaluedate','transactiontypeid'])->from(['equity'])->where(['clientid'=>$userId])->all();
        $fixeddeposit = (new \yii\db\Query())->select(['accountname','scheme','interestrate','amount','period','fdrnum','transactiondate','maturitydate','maturityamt','beneficiaryname'])->from(['fixeddeposit'])->where(['clientid'=>$userId])->all();
        $insurance = (new \yii\db\Query())->select(['insurancetypeid','arn','folio','scheme','buyername','amount','annualpremium','description','startdate','policyterm','beneficiaryname'])->from(['insurance'])->where(['clientid'=>$userId])->all();
        $mutualfund = (new \yii\db\Query())->select(['arn','folio','scheme','transactiontypeid','transactiondate','amount','marketvalue','marketvaluedate'])->from(['mutualfund'])->where(['clientid'=>$userId])->all();
        $realestate = (new \yii\db\Query())->select(['buyername','address','city','unitprice','area','amount','transactiondate','transactiontypeid','marketvalue','marketvaluedate'])->from(['realestate'])->where(['clientid'=>$userId])->all();
        $bankaccount = (new \yii\db\Query())->select(['primaryholder','jointholder','bankname','bankaddress','accountnum','ifsc','micr','isactive'])->from(['bankaccount'])->where(['clientid'=>$userId])->all();

		$counter = 1;
		$headerRows = array();
		
        $data[] = array(
            'report' => 'Asset Report'
        );
		$headerRows[0] = $counter;
        $data[] = array(
            'report' => ''
        );
		$counter++;
		
        $data[] = array(
            'report' => 'Bonds'
        );
		$counter++;
		$headerRows[1] = $counter;
        //Bond Asset
        $data[] = array(
            'issuername' => 'Issuer Name',
            'scrip' => 'Scrip',
            'units' => 'No. of Units',
            'amount' => 'Amount (Rs.)',
            'beneficiaryname' => 'Beneficiary Name',
            'applicationnum' => 'Application No.',
            'transactiondate' => 'Transaction Date',
            'transactiontypeid' => 'Transaction Type'
        );
		$counter++;
		
        foreach ($bond as $b)
        {
            $data[] = array(
                'issuername' => $b['issuername'],
                'scrip' => $b['scrip'],
                'units' => $b['units'],
                'amount' => $b['amount'],
                'beneficiaryname' => $b['beneficiaryname'],
                'applicationnum' => $b['applicationnum'],
                'transactiondate' => $b['transactiondate'],
                'transactiontype' => $transType[$b['transactiontypeid']-1]['description']
            );
			
			$counter++;
        }

        $data[] = array(
            'report' => ''
        );
		$counter++;
        $data[] = array(
            'report' => 'Commodities'
        );
		$counter++;
		$headerRows[2] = $counter;

        //Commodity Asset
        $data[] = array(
            'commoditytypeid' => 'Commodity Type',
            'unittypeid' => 'Unit Type',
            'quantity' => 'Quantity',
            'amount' => 'Amount (Rs.)',
            'transactiontypeid' => 'Transaction Type',
            'transactiondate' => 'Transaction Date',
            'marketvalue' => 'Market Value (Rs.)',
            'marketvaluedate' => 'Market Value Date'
        );
		$counter++;

        foreach ($commodity as $c)
        {
            $data[] = array(
                'commoditytypeid' => $commodityType[$c['commoditytypeid']-1]['description'],
                'unittypeid' => $unitType[$c['unittypeid']-1]['description'],
                'quantity' => $c['quantity'],
                'amount' => $c['amount'],
                'transactiontypeid' => $transType[$c['transactiontypeid']-1]['description'],
                'transactiondate' => $c['transactiondate'],
                'marketvalue' => $c['marketvalue'],
                'marketvaluedate' => $c['marketvaluedate']
            );
			$counter++;
        }

        $data[] = array(
            'report' => ''
        );
		$counter++;

        $data[] = array(
            'report' => 'Equities'
        );
		$counter++;
		$headerRows[3] = $counter;

        //Equity Asset
        $data[] = array(
            'scripname' => 'Scrip Name',
            'quantity' => 'Quantity',
            'rate' => 'Rate (Rs.)',
            'amount' => 'Amount (Rs.)',
            'transactiondate' => 'Transaction Date',
            'demataccountnum' => 'Demat Account Number',
            'marketvalue' => 'Market Value (Rs.)',
            'marketvaluedate' => 'Market Value Date',
            'transactiontypeid' => 'Transaction Type'
        );
		$counter++;

        foreach ($equity as $e)
        {
            $data[] = array(
                'scripname' => $e['scripname'],
                'quantity' => $e['quantity'],
                'rate' => $e['rate'],
                'amount' => $e['amount'],
                'transactiondate' => $e['transactiondate'],
                'demataccountnum' => $e['demataccountnum'],
                'marketvalue' => $e['marketvalue'],
                'marketvaluedate' => $e['marketvaluedate'],
                'transactiontypeid' => $transType[$e['transactiontypeid']-1]['description']
            );
			$counter++;
        }

        $data[] = array(
            'report' => ''
        );
		$counter++;

        $data[] = array(
            'report' => 'Fixed Deposits'
        );
		$counter++;
		$headerRows[4] = $counter;

        //Fix Deposits Asset
        $data[] = array(
            'accountname' => 'Account Name',
            'scheme' => 'Scheme Name',
            'interestrate' => 'Interest Rate (%)',
            'amount' => 'Amount (Rs.)',
            'period' => 'Period',
            'fdrnum' => 'FDR Number',
            'transactiondate' => 'Transaction Date',
            'maturitydate' => 'Maturity Date',
            'maturityamt' => 'Maturity Amount (Rs.)',
            'beneficiaryname' => 'Beneficiary Name'
        );
		$counter++;

        foreach ($fixeddeposit as $fd)
        {
            $data[] = array(
                'accountname' => $fd['accountname'],
                'scheme' => $fd['scheme'],
                'interestrate' => $fd['interestrate'],
                'amount' => $fd['amount'],
                'period' => $fd['period'],
                'fdrnum' => $fd['fdrnum'],
                'transactiondate' => $fd['transactiondate'],
                'maturitydate' => $fd['maturitydate'],
                'maturityamt' => $fd['maturityamt'],
                'beneficiaryname' => $fd['beneficiaryname']
            );
			$counter++;
        }

        $data[] = array(
            'report' => ''
        );
		$counter++;

        $data[] = array(
            'report' => 'Insurance Policies'
        );
		$counter++;
		$headerRows[5] = $counter;

        //Insurance Asset
        $data[] = array(
            'insurancetypeid' => 'Insurance Type',
            'arn' => 'Insurance Issuer',
            'folio' => 'Policy Name',
            'scheme' => 'Insurance Scheme',
            'buyername' => 'Buyer Name',
            'amount' => 'Policy Amount(Rs.)',
            'annualpremium' => 'Annual Premium (Rs.)',
            'description' => 'Description',
            'startdate' => 'Start Date',
            'policyterm' => 'Policy Term',
            'beneficiaryname'  => 'Beneficiary Name'
        );
		$counter++;

        foreach ($insurance as $ins)
        {
            $data[] = array(
                'insurancetypeid' => $insuranceType[$ins['insurancetypeid']-1]['description'],
                'arn' => $ins['arn'],
                'folio' => $ins['folio'],
                'scheme' => $ins['scheme'],
                'buyername' => $ins['buyername'],
                'amount' => $ins['amount'],
                'annualpremium' => $ins['annualpremium'],
                'description' => $ins['description'],
                'startdate' => $ins['startdate'],
                'policyterm' => $ins['policyterm'],
                'beneficiaryname'  => $ins['beneficiaryname']
            );
			$counter++;
        }

        $data[] = array(
            'report' => ''
        );
		$counter++;

        $data[] = array(
            'report' => 'Mutual Funds'
        );
		$counter++;
		$headerRows[6] = $counter;

        //Mutual Funds Asset
        $data[] = array(
            'arn' => 'Mutual Fund Issuer',
            'folio' => 'Mutual Fund Folio',
            'scheme' => 'Scheme Name',
            'transactiontypeid' => 'Transaction Type',
            'transactiondate' => 'Transaction Date',
            'amount' => 'Amount (Rs.)',
            'marketvalue' => 'Market Value (Rs.)',
            'marketvaluedate' => 'Market Value Date'
        );
		$counter++;

        foreach ($mutualfund as $mf)
        {
            $data[] = array(
                'arn' => $mf['arn'],
                'transactiondate' => $mf['transactiondate'],
                'folio' => $mf['folio'],
                'scheme' => $mf['scheme'],
                'amount' => $mf['amount'],
                'marketvalue' => $mf['marketvalue'],
                'marketvaluedate' => $mf['marketvaluedate'],
                'transactiontypeid' => $transType[$mf['transactiontypeid']-1]['description']
            );
			$counter++;
        }

        $data[] = array(
            'report' => ''
        );
		$counter++;

        $data[] = array(
            'report' => 'Real Estate'
        );
		$counter++;
		$headerRows[7] = $counter;

        //Real Estate Asset
        $data[] = array(
            'buyername' => 'Property Owner',
            'address' => 'Property Address',
            'city' => 'City',
            'unitprice' => 'Unit Price(Rs.)',
            'area' => 'Area(Sq. Ft.)',
            'amount' => 'Amount(Rs.)',
            'transactiondate' => 'Transaction Date',
            'transactiontypeid' => 'Transaction Type',
            'marketvalue' => 'Market Value',
            'marketvaluedate' => 'Market Value Date'
        );
		$counter++;

        foreach ($realestate as $rs)
        {
            $data[] = array(
                'buyername' => $rs['buyername'],
                'address' => $rs['address'],
                'city' => $rs['city'],
                'unitprice' => $rs['unitprice'],
                'area' => $rs['area'],
                'amount' => $rs['amount'],
                'transactiondate' => $rs['transactiondate'],
                'transactiontypeid' => $transType[$rs['transactiontypeid']-1]['description'],
                'marketvalue' => $rs['marketvalue'],
                'marketvaluedate' => $rs['marketvaluedate']
            );
			$counter++;
        }
		
		$data[] = array(
            'report' => ''
        );
		$counter++;

        $data[] = array(
            'report' => 'Bank Accounts'
        );
		$counter++;
		$headerRows[8] = $counter;
		
		//Bank Accounts
        $data[] = array(
            'primaryholder' => 'Primary Account Holder',
            'jointholder' => 'Joint Account Holder',
            'bankname' => 'Bank Name',
            'bankaddress' => 'Bank Address',
            'accountnum' => 'Account Number',
            'ifsc' => 'Bank IFSC Code',
            'micr' => 'Account MICR Code',
            'isactive' => 'Account Active',
        );
		$counter++;

        foreach ($bankaccount as $ba)
        {
            $data[] = array(
                'primaryholder' => $ba['primaryholder'],
				'jointholder' => $ba['jointholder'],
				'bankname' => $ba['bankname'],
				'bankaddress' => $ba['bankaddress'],
				'accountnum' => $ba['accountnum'],
				'ifsc' => $ba['ifsc'],
				'micr' => $ba['micr'],
				'isactive' => $ba['isactive'],
            );
			$counter++;
        }

        $file = fopen("../tmp/report.csv", "w") or die("Unable to open file!");

        foreach ($data as $d)
        {
            fputcsv($file,$d,',','"');
        }

        fclose($file);

        $separator = ',';
        $enclosure = '"';
        $objReader = \PHPExcel_IOFactory::createReader('CSV');
        $objReader->setDelimiter($separator);
        $objReader->setEnclosure($enclosure);
        //$objReader->setLineEnding($endrow);
        $objPHPExcel = $objReader->load('../tmp/report.csv');
		
        //C://wampRaj64/www/Winrich_client_app/tmp/report.csv

        $highestRow = $objPHPExcel->getActiveSheet()->getHighestRow();
        //$objPHPExcel->getActiveSheet()->removeRow(1, 1);

        $objPHPExcel->getProperties()
            ->setCreator('Winrich Client')
            ->setTitle('Client Report')
            ->setLastModifiedBy('Winrich Client')
            ->setDescription('All assets report')
            ->setSubject('Auto-Generated Assets Report');

        $activeSheet = $objPHPExcel->getSheet(0);
        $activeSheet->setTitle('Report');
        $header1 = 'a1:d1';
        //$activeSheet->getStyle($header1)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('00ffff00');
        $headerStyle1 = array(
            'font' => array(
				'bold' => true,
				'size' => 20,
				'underline' => true,
			),
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,),
        );
		
		$headerStyle2 = array(
            'font' => array(
				'bold' => true,
				'size' => 16,
				'underline' => true,
			),
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,),
        );
		
		$bodyStyle = array(
            'alignment' => array('horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,),
        );

        for ($col = ord('a'); $col <= ord('l'); $col++) {
            $activeSheet->getColumnDimension(chr($col))->setAutoSize(true);
        }
		
		for ($rows = 1; $rows <= $highestRow; $rows++) {
            
			$columns = "a".$rows.":"."l".$rows;
			$activeSheet->getStyle($columns)->applyFromArray($bodyStyle);
        }
		
		$activeSheet->getStyle("a".$headerRows[0])->applyFromArray($headerStyle1);
        $activeSheet->getStyle("a".$headerRows[1])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[2])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[3])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[4])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[5])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[6])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[7])->applyFromArray($headerStyle2);
        $activeSheet->getStyle("a".$headerRows[8])->applyFromArray($headerStyle2);


       // $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="client_report.xls"');        

       
		header("Pragma: no-cache");
		header("Expires: 0");
		$objWriter->save('php://output');
		unlink('../tmp/report.csv');
	} */
	
}

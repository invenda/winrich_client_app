<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Reference;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReferenceController implements the CRUD actions for Reference model.
 */
class ReferenceController extends Controller
{
	var $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','view','create', 'update'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@']
					],
				],
			],
			/*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],*/
        ];
    }

    /**
     * Lists all Reference models.
     * @return mixed
     */
    public function actionIndex()
    {
        $userId = Yii::$app->user->id;
		
		$dataProvider = Reference::find()->where(['clientid'=>$userId])->asArray()->all();
		
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);

    }

    /**
     * Displays a single Reference model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $clientid = (new \yii\db\Query())->select('reference.clientid')->from('reference')->where(['reference.id'=>$id])->one();
		
		//print_r($this->findModelArray($id));die;
		if($clientid['clientid'] == Yii::$app->user->id){
			return $this->render('view', [
				'model' => $this->findModelArray($id),
			]);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Reference model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$createDate = $date->format('Y-m-d');
		
		$userId = Yii::$app->user->id;
		$model = new Reference();
		
		if ($model->load(Yii::$app->request->post()) ) {
			
			if($_POST['Reference']['clientid'] == $userId){
				$model->createdate = $createDate;
				$model->save();
				return $this->redirect(['view', 'id' => $model->id]);
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        
		} else {
		
		//print_r($model->errors); die;
			return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Reference model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        date_default_timezone_set("Asia/Kolkata");
		$date = new \DateTime('now');
		$modifiedDate = $date->format('Y-m-d');
		
		$model = $this->findModel($id);
		$userId = Yii::$app->user->id;
		
        if ($model->load(Yii::$app->request->post())) {
            if($_POST['Reference']['clientid'] == $userId){
				$model->modifieddate = $modifiedDate;
				$model->save();
				return $this->redirect(['view', 'id' => $model->id]);
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        } else {
			
            $record = $this->findModelArray($id);
			if($record['clientid'] == $userId){
				return $this->render('update', [
					'model' => $record,
				]);
			}else {
				throw new NotFoundHttpException('The requested page does not exist.');
			}
        }
    }

    /**
     * Deletes an existing Reference model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $clientid = (new \yii\db\Query())->select('reference.clientid')->from('reference')->where(['reference.id'=>$id])->one();
		if($clientid['clientid'] == Yii::$app->user->id){
			$this->findModel($id)->delete();

			return $this->redirect(['index']);
		}else {
			throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Reference model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reference the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reference::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	protected function findModelArray($id)
    {
		$model = (new \yii\db\Query())->select(['reference.id','reference.clientid','reference.primaryholder','reference.jointholder','reference.nominee','reference.bankname','reference.bankaddress','reference.accountnum','reference.ifsc','reference.micr','reference.isactive','reference.specimen','reference.createdate','reference.modifieddate'])
		->from(['reference'])
		->where(['reference.id' => $id])->one();
		
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Arn */

$this->title = 'Create Arn';
$this->params['breadcrumbs'][] = ['label' => 'Arns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="arn-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

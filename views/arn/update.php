<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Arn */

$this->title = 'Update Arn: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Arns', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="arn-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

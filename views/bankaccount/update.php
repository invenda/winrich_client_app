	<a class="btn btn-primary" href="/bankaccount/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Account Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeCommodity" class="form-horizontal" action="/bankaccount/update/<?=$model['id']?>">
				
				<div class="form-group field-bankaccount-clientid">
					<label class="col-md-2 control-label" for="bankaccount-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="bankaccount-clientid" name="Bankaccount[clientid]">
				</div>
				
				<div class="form-group field-bankaccount-primaryholder">
					<label class="col-md-2 control-label" for="bankaccount-primaryholder">Primary Account Holder*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Abhi Rao" id="bankaccount-primaryholder" name="Bankaccount[primaryholder]" value="<?=$model['primaryholder']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-jointholder">
					<label class="col-md-2 control-label" for="bankaccount-jointholder">Joint Account Holder</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Priya Rao" id="bankaccount-jointholder" name="Bankaccount[jointholder]" value="<?=$model['jointholder']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-nominee">
					<label class="col-md-2 control-label" for="bankaccount-nominee">Nominee Name</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Priya Rao" id="bankaccount-nominee" name="Bankaccount[nominee]" value="<?=$model['nominee']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-bankname">
					<label class="col-md-2 control-label" for="bankaccount-bankname">Bank Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="SBI Bank" id="bankaccount-bankname" name="Bankaccount[bankname]" value="<?=$model['bankname']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-bankaddress">
					<label class="col-md-2 control-label" for="bankaccount-bankaddress">Bank Address*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Jayanagar, Bengaluru" id="bankaccount-bankaddress" name="Bankaccount[bankaddress]" value="<?=$model['bankaddress']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-accountnum">
					<label class="col-md-2 control-label" for="bankaccount-accountnum">Bank Account Number*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="BL12356..." id="bankaccount-accountnum" name="Bankaccount[accountnum]" value="<?=$model['accountnum']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-ifsc">
					<label class="col-md-2 control-label" for="bankaccount-ifsc">Bank IFSC Code</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="SBIB000011.." id="bankaccount-ifsc" name="Bankaccount[ifsc]" value="<?=$model['ifsc']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-micr">
					<label class="col-md-2 control-label" for="bankaccount-micr">Account MICR Code</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="011003511..." id="bankaccount-micr" name="Bankaccount[micr]" value="<?=$model['micr']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-bankaccount-isactive">
					<label class="col-md-2 control-label" for="bankaccount-isactive">Account Active*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="bankaccount-isactive" name="Bankaccount[isactive]" required>
								<option value="">Select One..</option>
								<?php if($model['isactive'] == "Yes"):?>
									<option value="Yes" selected>Yes</option>
									<option value="No">No</option>
								<?php endif; ?>
								<?php if($model['isactive'] == "No"):?>
									<option value="Yes">Yes</option>
									<option value="No" selected>No</option>
								<?php endif; ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<!--<button type="submit" class="btn btn-primary">Edit</button>-->
							<button type="submit" class="btn btn-primary" >Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<a class="btn btn-primary" href="/bankaccount/create" style="margin-bottom:15px">Add More Accounts</a>
<a class="btn btn-primary" href="/bankaccount/index" style="margin-bottom:15px; float:right;">Back</a>

<div class="panel-info widget-shadow">
					<h3 class="title1">Bank Accounts</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Account Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Primary Account Holder</th>
									<td><?=$model['primaryholder']?></td>
								</tr>
								<tr scope="row">
									<th >Joint Account Holder</th>
									<td><?=$model['jointholder']?><?php if($model['jointholder']==""){echo "- Nil -";} ?></td>
								</tr>
								<tr scope="row">
									<th >Nominee</th>
									<td><?=$model['nominee']?></td>
								</tr>
								<tr scope="row">
									<th >Bank Name</th>
									<td><?=$model['bankname']?></td>
								</tr>
								<tr scope="row">
									<th >Bank Address</th>
									<td><?=$model['bankaddress']?></td>
								</tr>
								<tr scope="row">
									<th >Account Number</th>
									<td><?=$model['accountnum']?></td>
								</tr>
								<tr scope="row">
									<th >Bank IFSC Code</th>
									<td><?=$model['ifsc']?></td>
								</tr>
								<tr scope="row">
									<th >Account MICR Code</th>
									<td><?=$model['micr']?></td>
								</tr>
								<tr scope="row">
									<th >Account Active</th>
									<td><?=$model['isactive']?></td>
								</tr>
								<!--<tr scope="row">
									<th >Specimen Signature</th>
									<td><img src="/images/specimen/<?=$model['specimen']?>" height="50px"></td>
								</tr>-->
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div>
							<a type="submit" class="btn btn-primary" href="/bankaccount/update/<?=$model['id']?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/bankaccount/delete/<?=$model['id']?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
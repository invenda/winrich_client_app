<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bankaccount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bankaccount-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'primaryholder')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jointholder')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bankname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bankaddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'accountnum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ifsc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'micr')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isactive')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'modifieddate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

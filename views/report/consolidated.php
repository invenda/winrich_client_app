<a class="btn btn-primary" href="/report/downloadreport">Download Report</a>
<div class="panel-group tool-tips" id="accordion" role="tablist" aria-multiselectable="true">
		<!--<h4 class="title2"> REPORT</h4>-->
		<br><br>
		<center><h2>What My Family Should Know</h2></center>
		<h4 style="float:left">READY REFERENCE:</h4>
		<h4 style="float:right">Mobile / Phone (Self) :_____________.<h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px "></th>
						<th style="text-align: center;"></th>
						<th style="text-align: center;">Name</th>
						<th style="text-align: center;">Office Address</th>
						<th style="text-align: center;">Residence address</th>
						<th style="text-align: center;">Mobile / Contact number</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">A</th>
						<th style="text-align: center;">Family Doctor</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">B</th>
						<th style="text-align: center;">Specialist Doctor (if any)</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">C</th>
						<th style="text-align: center;">Tax Consultant</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">D</th>
						<th scope="row" style="text-align: center;">Insurance Agent</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">E</th>
						<th style="text-align: center;">Stock Brokerr</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
		
		
		
		<h4 style="float:left">DOCUMENTS DETAILS:</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px "></th>
						<th style="text-align: center;"></th>
						<th style="text-align: center;">Number</th>
						<th style="text-align: center;">Expiry Date</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">A</th>
						<th style="text-align: center;">Passport</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">B</th>
						<th style="text-align: center;">Driving license</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">C</th>
						<th style="text-align: center;">Credit Cards / ATM Cards</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">D</th>
						<th scope="row" style="text-align: center;">Club Membership Professional / Others</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">E</th>
						<th style="text-align: center;">Vehicle Details</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">F</th>
						<th style="text-align: center;">Income Tax PAN No.</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">G</th>
						<th style="text-align: center;">Aadhar Card</th>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
		
		
		<h4 style="float:left">LOCATION OF IMPORTANT DOCUMENTS</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px "></th>
						<th style="text-align: center;">Type</th>
						<th style="text-align: center;">Location</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">A</th>
						<th style="text-align: center;">Personal Will</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">B</th>
						<th style="text-align: center;">Spouse’s Will</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">C</th>
						<th style="text-align: center;">Insurance Policies</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">D</th>
						<th scope="row" style="text-align: center;">Invest. Papers</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">E</th>
						<th style="text-align: center;">Property Records</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">F</th>
						<th style="text-align: center;">Birth Certificate</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">G</th>
						<th style="text-align: center;">Marriage Certificate</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">H</th>
						<th style="text-align: center;">Domicile Certificate</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">I</th>
						<th style="text-align: center;">Important Agreements</th>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">J</th>
						<th style="text-align: center;">Other Important Papers</th>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
		
		<h4 style="float:left">Insurance - LIC Policy Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="text-align: center;">Name / Nominee</th>
						<th style="text-align: center;">Policy No./Issuing Office</th>
						<th style="padding-left: 20px ">Amount Insured</th>
						<th style="text-align: center;">Issue Date/Maturity Date</th>
						<th style="text-align: center;">Table & Term</th>
						<th style="padding-left: 20px ">Premium</th>
						<th style="text-align: center;">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<th style="">Nominee - </th>
						<td>Through Mr.</td>
						<td style="text-align: center;"></td>
						<td>Date of last payment <br><br> Date of Maturity</td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<th style="">Nominee - </th>
						<td>Through Mr.</td>
						<td style="text-align: center;"></td>
						<td>Date of last payment <br><br> Date of Maturity</td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">3</th>
						<th style="">Nominee - </th>
						<td>Through Mr.</td>
						<td style="text-align: center;"></td>
						<td>Date of last payment <br><br> Date of Maturity</td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
			
		
		
		
		
		<h4 style="float:left">Medi Claim Policy Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="text-align: center;">Name & Type of Policy</th>
						<th style="text-align: center;">Policy No./Previous Policy No.</th>
						<th style="padding-left: 20px ">Amount Insured</th>
						<th style="text-align: center;">Issue Date/Maturity Date</th>
						<th style="padding-left: 20px ">Premium</th>
						<th style="text-align: center;">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<th style="">Floater Policy</th>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<th style="">Floater Policy</th>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
			
		
		
		
		
		<h4 style="float:left">Vehicle Insurance Policy Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="text-align: center;">Name / Vehicle</th>
						<th style="text-align: center;">Policy No./Issuing Office</th>
						<th style="padding-left: 20px ">Amount Insured</th>
						<th style="text-align: center;">Issue Date/Maturity Date</th>
						<th style="padding-left: 20px ">Premium</th>
						<th style="text-align: center;">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style="">Reg. No. <br><br> Model Name & No.<br><br>Engine No.<br><br>Chassis No.<br><br>Mfg Yr.<br><br>CCNominee-<br><br>Agent Name & Mobile No.</td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
				
		
		
		<h4 style="float:left">FIRE / BURGLARY INSURANCE DETAIL:</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="text-align: center;">Name of the Property / Nominee</th>
						<th style="text-align: center;">Policy No./Issuing Office</th>
						<th style="padding-left: 20px ">Amount Insured</th>
						<th style="text-align: center;">Issue Date/Maturity Date</th>
						<th style="text-align: center;">Risk Covered</th>
						<th style="padding-left: 20px ">Premium</th>
						<th style="text-align: center;">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
				
		
		
		<h4 style="float:left">Bank Accounts</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Bank Name</th>
						<th style="text-align: center;">Branch</th>
						<th style="text-align: center;">Type of Account</th>
						<th style="padding-left: 20px ">Operating Instructions</th>
						<th style="text-align: center;">Nominee/s</th>
						<th style="text-align: center;">Specimen Signature</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
						
		
		
		<h4 style="float:left">FIXED DEPOSIT / RECURRING DEPOSIT / COMPANY DEPOSIT:</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Bank / Company Name & Branch</th>
						<th style="padding-left: 20px ">Type of Dep. (SDR / FDR / RD)</th>
						<th style="text-align: center;">FDR No.</th>
						<th style="text-align: center;">Date of Dep.</th>
						<th style="padding-left: 20px ">FVG</th>
						<th style="text-align: center;">Amt. (Rs.)</th>
						<th style="text-align: center;">Due Date</th>
						<th style="text-align: center;">Op. Inst.</th>
						<th style="text-align: center;">Nominee/s</th>
						<th style="text-align: center;">Specimen Signature</th>
						<th style="padding-left: 20px ">Loan / OD availed</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
							
		
		
		<h4 style="float:left">SHARES/UNITS/DEBENTURES/BONDS: Standing in own name or Jointly with</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Company</th>
						<th style="padding-left: 20px ">No. of shares</th>
						<th style="text-align: center;">Demat A/c. No.</th>
						<th style="text-align: center;">Demat Bank Details</th>
						<th style="padding-left: 20px ">Demat Statement location</th>
						<th style="text-align: center;">Held Singly / Jointly</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
							
		
		
		<h4 style="float:left">Lockers</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Bank Name & Branch</th>
						<th style="padding-left: 20px ">Locker No.</th>
						<th style="text-align: center;">In the name of</th>
						<th style="text-align: center;">Code</th>
						<th style="padding-left: 20px ">Rent (Rs)</th>
						<th style="text-align: center;">Rent Renewal Date</th>
						<th style="padding-left: 20px ">Nominee</th>
						<th style="text-align: center;">Contents</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
							
		
		
		<h4 style="float:left">PUBLIC PROVIDENT FUND (PPF)</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Bank Name & Branch</th>
						<th style="padding-left: 20px ">FVG</th>
						<th style="text-align: center;">PPF Account No.</th>
						<th style="text-align: center;">Maturity Date</th>
						<th style="padding-left: 20px ">Nominee/s</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
									
		
		
		<h4 style="float:left">Pension A/c.</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Bank Name & Branch</th>
						<th style="padding-left: 20px ">Type of Account & Pension A/c. No.</th>
						<th style="text-align: center;">Operating Instructions</th>
						<th style="text-align: center;">Pension Payment Order No.</th>
						<th style="padding-left: 20px ">Nominee/s</th>
						<th style="padding-left: 20px ">Due Date for Live Certificate</th>
						<th style="padding-left: 20px ">Signature</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
									
		
		
		<h4 style="float:left">ATM / Debit Card Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">SB A/c. No. / Bank & Branch</th>
						<th style="text-align: center;">ATM / Debit Card No.</th>
						<th style="padding-left: 20px ">Issue Date</th>
						<th style="padding-left: 20px ">Valid Thru</th>
						<th style="padding-left: 20px ">CVV No.</th>
						<th style="padding-left: 20px ">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
										
		
		
		<h4 style="float:left">Credit Card Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Bank's Name</th>
						<th style="text-align: center;">ATM / Debit Card No.</th>
						<th style="padding-left: 20px ">Valid From</th>
						<th style="padding-left: 20px ">Valid Thru</th>
						<th style="padding-left: 20px ">CVV No.</th>
						<th style="padding-left: 20px ">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
		<h4 style="float:left">PANCARD Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Bank's Name</th>
						<th style="text-align: center;">ATM / Debit Card No.</th>
						<th style="padding-left: 20px ">Valid From</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
			
		
		<h4 style="float:left">Passport Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Passport No.</th>
						<th style="text-align: center;">Issue Date</th>
						<th style="padding-left: 20px ">Expiry Date</th>
						<th style="padding-left: 20px ">Issuing Authority</th>
						<th style="padding-left: 20px ">Previous Passport Details</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
				
		
		<h4 style="float:left">Electricity Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">House Details</th>
						<th style="text-align: center;">Meter No.</th>
						<th style="padding-left: 20px ">Customer no.</th>
						<th style="padding-left: 20px; color:red; ">Deposit Rs.</th>
						<th style="padding-left: 20px ">Previous Passport Details</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
					
		
		<h4 style="float:left">Gas Pipe Line Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">House Details</th>
						<th style="text-align: center;">Meter No.</th>
						<th style="padding-left: 20px ">Customer no.</th>
						<th style="padding-left: 20px; color:red; ">Deposit Rs.</th>
						<th style="padding-left: 20px ">Previous Passport Details</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
				
		<h4 style="float:left">Gas Cylinder Agency Service Details -</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">House Details</th>
						<th style="text-align: center;">Meter No.</th>
						<th style="padding-left: 20px ">IOC Serial No.</th>
						<th style="padding-left: 20px; color:red; ">Deposit Rs.</th>
						<th style="padding-left: 20px ">Previous Passport Details</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
			
				
		<h4 style="float:left">BSNL Land Line Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">House Details</th>
						<th style="text-align: center;">Phone No.</th>
						<th style="padding-left: 20px ">Customer No.</th>
						<th style="padding-left: 20px; color:red; ">Deposit LL/ Broad Band WiFI Rs.</th>
						<th style="padding-left: 20px ">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
			
		
			
				
		<h4 style="float:left">Driving License Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Driving License No. / Licencing Authority</th>
						<th style="text-align: center;">Issue Date/CDOI</th>
						<th style="padding-left: 20px ">Valid Details / Valid From</th>
						<th style="padding-left: 20px;">Deposit LL/ Broad Band WiFI Rs.</th>
						<th style="padding-left: 20px ">Valid Till</th>
						<th style="padding-left: 20px ">Remarks / Blood Group</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
				
		
			
				
		<h4 style="float:left">Ration Card Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Ration Card No./Issuing Authority</th>
						<th style="text-align: center;">Issue Date</th>
						<th style="padding-left: 20px ">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
					
		
			
				
		<h4 style="float:left">Aadhar Card - UID Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Aadhar Card No./ Enrollment No.</th>
						<th style="text-align: center;">Issue Date</th>
						<th style="padding-left: 20px ">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
			
				
		<h4 style="float:left">Election Identity Card - Details</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">Father’s/Husband’s Name</th>
						<th style="padding-left: 20px ">Identity Card No.</th>
						<th style="text-align: center;">Issue Date</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
			
		
			
				
		<h4 style="float:left">HOUSE PROPERTY:</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Property Detail & standing in the name of:</th>
						<th style="padding-left: 20px ">How acquired (Inherited / Loan) Bank Loan Detail: Loan Amt. Inst. Amt. O/s. Amt.</th>
						<th style="text-align: center;">Registration No. / Share Certificate No.</th>
						<th style="padding-left: 20px ">Nominee if any</th>
						<th style="text-align: center;">Property Card No. and valid upto</th>
						<th style="padding-left: 20px ">House Tax (Rs.)</th>
						<th style="padding-left: 20px ">Next Due Date of House Tax</th>
						<th style="text-align: center;">Ins. Policy No., Amt. & Due Date</th>
						<th style="padding-left: 20px ">Risk/s covered</th>
						<th style="text-align: center;">Mortgage with Bank Name & Branch / Place of Docs.</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
			
			
		
			
				
		<h4 style="float:left">House Tax Details:</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">S.No</th>
						<th style="padding-left: 20px ">Name</th>
						<th style="text-align: center;">House Details</th>
						<th style="padding-left: 20px ">Census No.</th>
						<th style="text-align: center;">Property Identification No. (PIN)</th>
						<th style="padding-left: 20px ">Construction Sq.Mtrs.</th>
						<th style="padding-left: 20px; color:red ">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">1</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
						<td style=""></td>
					</tr>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px ">2</th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
						<td></td>
						<td></td>
						<td style=""></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br><br>
		
		
				
				
		<h4 style="float:left">INCOME TAX</h4>
		<br><br>
		<div  class="widget-shadow">
			<table class="table stats-table ">
				<thead>
					<tr>
						<th style="padding-left: 20px ">Permanent Account No.</th>
						<th style="padding-left: 20px ">Ward No. and Office Address</th>
						<th style="text-align: center;">Last Return Filed</th>
						<th style="padding-left: 20px ">File No.</th>
					</tr>
				</thead>
				<tbody>
					<tr class='clickable-row' data-href='#bondBuy'>
						<th scope="row" style="padding-left: 20px "></th>
						<td style=""></td>
						<td></td>
						<td style="text-align: center;"></td>
					</tr>

				</tbody>
			</table>
		</div>
		<br><br>
		
		
		
		
		<br><br><br><br>
		<h4 style="float:left">WILL:</h4>
		<br><br>
		<h5 style="font-size: 15px;">
		My will is executed on :_________________ <br><br>
		Copy of the will is kept at:_______________<br>
		</h5>
		
		<br><br>
		<h4>POWER OF ATTORNEY :</h4>
		<br><br>
		<h5 style="font-size: 15px;">
			<ul style="margin-left:35px">
				<li style="line-height: 30px;">Power of Attorney executed for Wife/Son/Others</li>
				<li style="line-height: 30px;">My Power of Attorney is</li>
				<li style="line-height: 30px;">Deed Executed on:</li>
				<li style="line-height: 30px;">Details kept in File No.</li>
			</ul>
		</h5>
		
		<br><br>
		<h4>MY DEBT / LIABILITIES :</h4>
		<br><br>
		<h5 style="font-size: 15px;">
			<ul style="margin-left:35px">
				<li style="line-height: 30px;">I am guarantor of Mr.<br>Give complete details:
					<ul style="margin-left:35px">
						<li style="line-height: 30px;">1.</li>
						<li style="line-height: 30px;">2.</li>
					</ul>
				</li>
				<li style="line-height: 30px;">I have borrowed from : (Give compete details)<br><br><br></li>
				<li style="line-height: 30px;">Other Liabilities<br><br><br></li>
			</ul>
		</h5>
		
		
		<br><br><br>
		
		<h4>Compiled by : Mr. R K Chopra, Bank Of Baroda -Apex Training Centre,Gandhinagar. Certified Management Trainer of AIMA & National Open College Network (NOCN) a leading UK awarding & assessment organization and Accredited Management Teacher, All India Management Association (AIMA), New Delhi.</h4>
		<br>
		<h4>& Forwarded to : LIC Customerzone, Gandhinagar by Shri R.K.Chopra.</h4>
	<!--	
		<div class="widget-shadow">
						<table class="table stats-table ">
							<thead>
								<tr>
									<th style="padding-left: 20px ">ASSET</th>
									<th style="text-align: center;">QUANTITY</th>
									<th style="text-align: center;">TRANSACTION TYPE</th>
									<th style="text-align: center;">TOTAL VALUE</th>
								</tr>
							</thead>
							<tbody>
								<tr class='clickable-row' data-href='#bondBuy'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#bondBuy">BONDS</a></th>
									<td style="text-align: center;"><?=$bondBuySum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-success">BUY</span></td>
									<td style="text-align: center;"><?=(!empty($bondBuySum['sum(amount)']))? "₹".$bondBuySum['sum(amount)']: "₹0"?></td>
								</tr>
								<tr class='clickable-row' data-href='#bondSell'>
									<th scope="row" style="padding-left: 20px " ><a style="color:black;" class="page-scroll" href="#bondSell">BONDS</a></th>
									<td style="text-align: center;"><?=$bondSellSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-warning">SELL</span></td>
									<td style="text-align: center;"><?=(!empty($bondSellSum['sum(amount)']))? "₹".$bondSellSum['sum(amount)']: "₹0"?></td>
								</tr>
								
								<tr class='clickable-row' data-href='#equityBuy'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#equityBuy">EQUITY</a></th>
									<td style="text-align: center;"><?=$equityBuySum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-success">BUY</span></td>
									<td style="text-align: center;"><?=(!empty($equityBuySum['sum(amount)']))? "₹".$equityBuySum['sum(amount)']: "₹0"?></td>
								</tr>
								<tr class='clickable-row' data-href='#equitySell'>
									<th scope="row" style="padding-left: 20px " ><a style="color:black;" class="page-scroll" href="#equitySell">EQUITY</a></th>
									<td style="text-align: center;"><?=$equitySellSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-warning">SELL</span></td>
									<td style="text-align: center;"><?=(!empty($equitySellSum['sum(amount)']))? "₹".$equitySellSum['sum(amount)']: "₹0"?></td>
								</tr>
								
								<tr class='clickable-row' data-href='#fdSum'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#fdSum">FIXED DEPOSITS</a></th>
									<td style="text-align: center;"><?=$fdSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-primary">ALL</span></td>
									<td style="text-align: center;"><?=(!empty($fdSum['sum(amount)']))? "₹".$fdSum['sum(amount)']: "₹0"?></td>
								</tr>
								
								<tr class='clickable-row' data-href='#insuranceSum'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#insuranceSum">INSURANCE</a></th>
									<td style="text-align: center;"><?=$insuranceSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-primary">ALL</span></td>
									<td style="text-align: center;"><?=(!empty($insuranceSum['sum(amount)']))? "₹".$insuranceSum['sum(amount)']: "₹0"?></td>
								</tr>
															
								<tr class='clickable-row' data-href='#mfBuy'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#mfBuy">MUTUAL FUNDS</a></th>
									<td style="text-align: center;"><?=$mfBuySum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-success">BUY</span></td>
									<td style="text-align: center;"><?=(!empty($mfBuySum['sum(amount)']))? "₹".$mfBuySum['sum(amount)']: "₹0"?></td>
								</tr>
								<tr class='clickable-row' data-href='#mfSell'>
									<th scope="row" style="padding-left: 20px " ><a style="color:black;" class="page-scroll" href="#mfSell">MUTUAL FUNDS</a></th>
									<td style="text-align: center;"><?=$mfSellSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-warning">SELL</span></td>
									<td style="text-align: center;"><?=(!empty($mfSellSum['sum(amount)']))? "₹".$mfSellSum['sum(amount)']: "₹0"?></td>
								</tr>
								
								<tr class='clickable-row' data-href='#realestateBuy'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#realestateBuy">REAL ESTATE</a></th>
									<td style="text-align: center;"><?=$realestateBuySum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-success">BUY</span></td>
									<td style="text-align: center;"><?=(!empty($realestateBuySum['sum(amount)']))? "₹".$realestateBuySum['sum(amount)']: "₹0"?></td>
								</tr>
								<tr class='clickable-row' data-href='#realestateSell'>
									<th scope="row" style="padding-left: 20px " ><a style="color:black;" class="page-scroll" href="#realestateSell">REAL ESTATE</a></th>
									<td style="text-align: center;"><?=$realestateSellSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-warning">SELL</span></td>
									<td style="text-align: center;"><?=(!empty($realestateSellSum['sum(amount)']))? "₹".$realestateSellSum['sum(amount)']: "₹0"?></td>
								</tr>
								
								<tr class='clickable-row' data-href='#commodityBuy'>
									<th scope="row" style="padding-left: 20px "><a style="color:black;" class="page-scroll" href="#commodityBuy">COMMODITY</a></th>
									<td style="text-align: center;"><?=$commodityBuySum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-success">BUY</span></td>
									<td style="text-align: center;"><?=(!empty($commodityBuySum['sum(amount)']))? "₹".$commodityBuySum['sum(amount)']: "₹0"?></td>
								</tr>
								<tr class='clickable-row' data-href='#commoditySell'>
									<th scope="row" style="padding-left: 20px " ><a style="color:black;" class="page-scroll" href="#commoditySell">COMMODITY</a></th>
									<td style="text-align: center;"><?=$commoditySellSum['count(*)']?></td>
									<td style="text-align: center;"><span class="label label-warning">SELL</span></td>
									<td style="text-align: center;"><?=(!empty($commoditySellSum['sum(amount)']))? "₹".$commoditySellSum['sum(amount)']: "₹0"?></td>
								</tr>
							</tbody>
						</table>
		</div>

		
	-->	
		
		
</div>

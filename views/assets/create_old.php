<div class="row">
	<h3 class="title1">Variable Forms :</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form id=assettype<?=$instrumentId?> class="form-horizontal" action="/index.php?r=assets/create&instrumentId=<?=$instrumentId?>">
				<div class="form-group">
					<label class="col-md-2 control-label">Email Address</label>
					<div class="col-md-8">
						<div class="input-group">							
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input type="text" class="form-control1" placeholder="Email Address">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Password</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							<input type="password" class="form-control1" id="exampleInputPassword1" placeholder="Password">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Email Address</label>
					<div class="col-md-8">
						<div class="input-group input-icon right">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input id="email" class="form-control1" type="text" placeholder="Email Address">
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">With tooltip</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Password</label>
					<div class="col-md-8">
						<div class="input-group input-icon right">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							<input type="password" class="form-control1" placeholder="Password">
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">With tooltip</p>
					</div>
				</div>
				<div class="form-group has-success">
					<label class="col-md-2 control-label">Input Addon Success</label>
					<div class="col-md-8">
						<div class="input-group input-icon right">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input id="email" class="form-control1" type="text" placeholder="Email Address">
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">Email is valid!</p>
					</div>
				</div>
				<div class="form-group has-error">
					<label class="col-md-2 control-label">Input Addon Error</label>
					<div class="col-md-8">
						<div class="input-group input-icon right">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							<input type="password" class="form-control1" placeholder="Password">
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">Error!</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Checkbox Addon</label>
					<div class="col-md-8">
						<div class="input-group">
							<div class="input-group-addon"><input type="checkbox"></div>
							<input type="text" class="form-control1">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Checkbox Addon</label>
					<div class="col-md-8">
						<div class="input-group">
							<input type="text" class="form-control1">
							<div class="input-group-addon"><input type="checkbox"></div>
							
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">Checkbox on right</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Radio Addon</label>
					<div class="col-md-8">
						<div class="input-group">
							<div class="input-group-addon"><input type="radio"></div>
							<input type="text" class="form-control1">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Radio Addon</label>
					<div class="col-md-8">
						<div class="input-group">
							<input type="text" class="form-control1">
							<div class="input-group-addon"><input type="radio"></div>
							
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">Radio on right</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Input Processing</label>
					<div class="col-md-8">
						<div class="input-icon right spinner">
							<i class="fa fa-fw fa-spin fa-spinner"></i>
							<input id="email" class="form-control1" type="text" placeholder="Processing...">
						</div>
					</div>
					<div class="col-sm-2">
						<p class="help-block">Processing right</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Static Paragraph</label>
					<div class="col-md-8">
						<p class="form-control1 m-n">email@example.com</p>
					</div>
				</div>
				<div class="form-group mb-n">
					<label class="col-md-2 control-label">Readonly</label>
					<div class="col-md-8">
						<input type="text" class="form-control1" placeholder="Readonly" readonly="">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>	
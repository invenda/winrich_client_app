
				<div class="tables">
					<h3 class="title1"><?php echo $instrument[0]['description']." Assets"?></h3>
					<a class="btn btn-primary" href="/assets/create/<?=$instrument[0]['id']?>">Add New Asset</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your Assets</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>ID</th>
									<th>ARN ID</th>
									<th>Folio</th>
									<th>Tranx</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<th scope="row"><?=$assets['instrumentid']?></th>
										<td><?=$assets['arnid']?></td>
										<td><?=$assets['folio']?></td>
										<td><?=$assets['transactiontypeid']?></td>
									</tr>
							
								<tr>
								<td colspan="4" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table table-hover"> 
							<tbody>
								
								<tr scope="row">
									<th >Folio</th>
									<td><?=$assets['folio']?></td>
								</tr>
								<tr scope="row">
									<th >Description</th>
									<td><?=$assets['description']?></td>
								</tr>
								<tr scope="row">
									<th >Amount</th>
									<td><?=$assets['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Start Date</th>
									<td><?=$assets['startdate']?></td>
								</tr>
								<tr scope="row">
									<th >End Date</th>
									<td><?=$assets['enddate']?></td>
								</tr>
								
								
								
							</tbody></table>
									<a type="submit" class="btn btn-primary" href="/assets/view/<?=$assets['id']?>">View</a>
									<a type="submit" class="btn btn-primary" href="/assets/update/<?=$assets['id']?>">Edit</a>
									<a type="submit" class="btn btn-danger" href="/assets/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php endforeach?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
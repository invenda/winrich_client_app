<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<div class="panel-info widget-shadow">
					<h3 class="title1"><?=$model['instrumentdes']?> Details</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Asset Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Asset Type</th>
									<td><?=$model['instrumentdes']?></td>
								</tr>
								<tr scope="row">
									<th >ARN</th>
									<td><?=$model['arndes']?></td>
								</tr>
								<tr scope="row">
									<th >Folio</th>
									<td><?=$model['folio']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Type</th>
									<td><?=$model['transdes']?></td>
								</tr>
								<tr scope="row">
									<th >Scheme</th>
									<td><?=$model['schemedes']?> (<?=$model['schemecode']?>)</td>
								</tr>
								<tr scope="row">
									<th >Description</th>
									<td><?=$model['description']?></td>
								</tr>
								<tr scope="row">
									<th >Amount</th>
									<td><?=$model['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Multiple Days</th>
									<td><?=$model['multipledays']?></td>
								</tr>
								<tr scope="row">
									<th >Frequency</th>
									<td><?=$model['frequencyid']?></td>
								</tr>
								<tr scope="row">
									<th >Start Date</th>
									<td><?=$model['startdate']?></td>
								</tr>
								<tr scope="row">
									<th >End Date</th>
									<td><?=$model['enddate']?></td>
								</tr>
								
								
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<a type="submit" class="btn btn-primary" href="/assets/update/<?=$assetid?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/assets/delete/<?=$assetid?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
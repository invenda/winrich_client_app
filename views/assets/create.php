	<h3 class="title1">Add New <?=$instrument[0]['description']?> - Asset Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettype<?=$instrument[0]['id']?>" class="form-horizontal" action="/assets/create/<?=$instrument[0]['id']?>">
				<div class="form-group field-assets-instrumentid">
					<label class="col-md-2 control-label" for="assets-instrumentid"></label>
					<input type="hidden" id="assets-instrumentid" class="form-control" name="Assets[instrumentid]" value="<?=$instrument[0]['id']?>">
				</div>
				
				<div class="form-group field-assets-arnid">
					<label class="col-md-2 control-label" for="assets-arnid">ARN*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							<select class="form-control1"  id="assets-arnid" name="Assets[arnid]" required>
								<option value="0">Select ARN..</option>
								<?php foreach($arn as $a):?>
								<option value="<?=$a['id']?>"><?=$a['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-folio">
					<label class="col-md-2 control-label" for="assets-folio">Folio*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Folio Name" id="assets-folio" name="Assets[folio]" maxlength="500" required>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-transactiontypeid">
					<label class="col-md-2 control-label" for="assets-transactiontypeid">Transaction Type*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							<select class="form-control1"  id="assets-transactiontypeid" name="Assets[transactiontypeid]" required>
								<option value="0">Select a Transaction Type..</option>
								<?php foreach($transactionType as $trans):?>
								<option value="<?=$trans['id']?>"><?=$trans['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-schemeid">
					<label class="col-md-2 control-label" for="assets-schemeid">Scheme*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-key"></i>
							</span>
							<select class="form-control1"  id="assets-schemeid" name="Assets[schemeid]" required>
								<option value="0">Select a Scheme..</option>
								<?php foreach($scheme as $sch):?>
								<option value="<?=$sch['id']?>"><?=$sch['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-description">
					<label class="col-md-2 control-label" for="assets-description">Description</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Breif Description" id="assets-description" name="Assets[description]" maxlength="500">
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-amount">
					<label class="col-md-2 control-label" for="assets-amount">Amount*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Amount of your Asset" id="assets-amount" name="Assets[amount]" required>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-multipledays">
					<label class="col-md-2 control-label" for="assets-multipledays">Multiple Days*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Value" id="assets-multipledays" name="Assets[multipledays]" maxlength="45" required>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-frequencyid">
					<label class="col-md-2 control-label" for="assets-frequencyid">Frequency*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Frequency of your Purchase" id="assets-frequencyid" name="Assets[frequencyid]" required>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-startdate">
					<label class="col-md-2 control-label" for="assets-startdate">Start Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="YYYY-MM-DD" id="assets-startdate" name="Assets[startdate]" required>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-enddate">
					<label class="col-md-2 control-label" for="assets-enddate">End Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="YYYY-MM-DD" id="assets-enddate" name="Assets[enddate]" required>
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-clientid">
					<label class="col-md-2 control-label" for="assets-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="assets-clientid" name="Assets[clientid]">
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
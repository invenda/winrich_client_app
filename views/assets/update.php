	<h3 class="title1">Edit <?=$model['instrumentdes']?> - Asset Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettype<?=$model['instrumentid']?>" class="form-horizontal" action="/assets/update/<?=$assetid?>">
				
				<div class="form-group field-assets-description">
					<label class="col-md-2 control-label" for="assets-description">Description</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Breif Description" value="<?=$model['description']?>" id="assets-description" name="Assets[description]" maxlength="500">
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-amount">
					<label class="col-md-2 control-label" for="assets-amount">Amount</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Amount of your Asset" value="<?=$model['amount']?>" id="assets-amount" name="Assets[amount]">
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-multipledays">
					<label class="col-md-2 control-label" for="assets-multipledays">Multiple Days</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Value" value="<?=$model['multipledays']?>" id="assets-multipledays" name="Assets[multipledays]" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-frequencyid">
					<label class="col-md-2 control-label" for="assets-frequencyid">Frequency</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Frequency of your Purchase" value="<?=$model['frequencyid']?>" id="assets-frequencyid" name="Assets[frequencyid]">
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-startdate">
					<label class="col-md-2 control-label" for="assets-startdate">Start Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="YYYY-MM-DD" value="<?=$model['startdate']?>" id="assets-startdate" name="Assets[startdate]">
						</div>
					</div>
				</div>
				
				<div class="form-group field-assets-enddate">
					<label class="col-md-2 control-label" for="assets-enddate">End Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="YYYY-MM-DD" value="<?=$model['enddate']?>" id="assets-enddate" name="Assets[enddate]">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<!--<button type="submit" class="btn btn-primary">Edit</button>-->
							<button type="submit" class="btn btn-primary" >Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
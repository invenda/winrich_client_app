<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assets-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Assets', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'instrumentid',
            'arnid',
            'folio',
            'transactiontypeid',
            // 'schemeid',
            // 'description',
            // 'clientid',
            // 'amount',
            // 'multipledays',
            // 'frequencyid',
            // 'startdate',
            // 'enddate',
            // 'createdate',
            // 'modifieddate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

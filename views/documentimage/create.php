<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Documentimage */

$this->title = 'Create Documentimage';
$this->params['breadcrumbs'][] = ['label' => 'Documentimages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documentimage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

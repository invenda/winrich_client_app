<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fixed Deposit';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<a class="btn btn-primary" href="/fixeddeposit/create">Add New FD</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your Assets</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>Sr No.</th>
									<th>Scheme</th>
									<th>Interest Rate</th>
									<th>Amount</th>
									<th>Manage</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<td scope="row"><?=$count?></td>
										<td><?=$assets['scheme']?></td>
										<td><?=$assets['interestrate']?></td>
										<td><?=$assets['amount']?></td>
										<td>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/fixeddeposit/view/<?=$assets['id']?>">View</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/fixeddeposit/update/<?=$assets['id']?>">Edit</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-danger" href="/fixeddeposit/delete/<?=$assets['id']?>">Delete</a>
										</td>
									</tr>
							
								<tr>
								<td colspan="5" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table"> 
							<tbody>
								
								<tr scope="row">
									<th>Account Name</th>
									<td><?=$assets['accountname']?></td>
								</tr>
								<tr scope="row">
									<th>Bank Name</th>
									<td><?=$assets['bankname']?></td>
								</tr>
								<tr scope="row">
									<th>Bank Address</th>
									<td><?=$assets['bankaddress']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$assets['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Period of Deposit</th>
									<td><?=$assets['period']?></td>
								</tr>
								<tr scope="row">
									<th >Maturity Date</th>
									<td><?=$assets['maturitydate']?></td>
								</tr>
								
								<tr scope="row">
									<th >Maturity Amount</th>
									<td><?=$assets['maturityamt']?></td>
								</tr>
								<tr scope="row">
									<th >Beneficiary Name</th>
									<td><?=$assets['beneficiaryname']?></td>
								</tr>
								<tr scope="row">
									<th >FDR Number</th>
									<td><?=$assets['fdrnum']?></td>
								</tr>
								
							</tbody></table>
										<a type="submit" class="btn btn-primary" href="/fixeddeposit/view/<?=$assets['id']?>">View</a>
										<a type="submit" class="btn btn-primary" href="/fixeddeposit/update/<?=$assets['id']?>">Edit</a>
										<a type="submit" class="btn btn-danger" href="/fixeddeposit/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php $count++;?>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
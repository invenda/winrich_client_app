<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fixeddeposit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fixeddeposit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clientid')->textInput() ?>

    <?= $form->field($model, 'accountname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'scheme')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'interestrate')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'period')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fdrnum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transactiondate')->textInput() ?>

    <?= $form->field($model, 'maturitydate')->textInput() ?>

    <?= $form->field($model, 'maturityamt')->textInput() ?>

    <?= $form->field($model, 'beneficiaryname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'documentimageid')->textInput() ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'modifieddate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

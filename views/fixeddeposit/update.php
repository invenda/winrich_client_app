	<a class="btn btn-primary" href="/fixeddeposit/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update FD Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypefixeddeposit" class="form-horizontal" action="/fixeddeposit/update/<?=$model['id']?>">
			
				<div class="form-group field-fixeddeposit-clientid">
					<label class="col-md-2 control-label" for="fixeddeposit-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="fixeddeposit-clientid" name="Fixeddeposit[clientid]">
				</div>
				<div class="form-group field-fixeddeposit-accountname">
					<label class="col-md-2 control-label" for="fixeddeposit-accountname">Account Holder Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Frank" id="fixeddeposit-accountname" name="Fixeddeposit[accountname]" value="<?=$model['accountname']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-bankname">
					<label class="col-md-2 control-label" for="fixeddeposit-bankname">Bank Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="HDFC Bank" id="fixeddeposit-bankname" name="Fixeddeposit[bankname]" value="<?=$model['bankname']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-bankaddress">
					<label class="col-md-2 control-label" for="fixeddeposit-bankaddress">Bank Address*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Bengaluru" id="fixeddeposit-bankaddress" name="Fixeddeposit[bankaddress]" value="<?=$model['bankaddress']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-interestrate">
					<label class="col-md-2 control-label" for="fixeddeposit-interestrate">Interest Rate(%)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-superscript"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="12.5" id="fixeddeposit-interestrate" name="Fixeddeposit[interestrate]" value="<?=$model['interestrate']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-amount">
					<label class="col-md-2 control-label" for="fixeddeposit-scrip">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="50000" id="fixeddeposit-amount" name="Fixeddeposit[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-period">
					<label class="col-md-2 control-label" for="fixeddeposit-period">FD Period(Months)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-clock-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="24" id="fixeddeposit-period" name="Fixeddeposit[period]" value="<?=$model['period']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-transactiondate">
					<label class="col-md-2 control-label" for="fixeddeposit-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['transactiondate'], 'id' => 'fixeddeposit-transactiondate','name' => 'Fixeddeposit[transactiondate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => '']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-maturitydate">
					<label class="col-md-2 control-label" for="fixeddeposit-maturitydate">Maturity Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['maturitydate'], 'id' => 'fixeddeposit-maturitydate','name' => 'Fixeddeposit[maturitydate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-maturityamt">
					<label class="col-md-2 control-label" for="fixeddeposit-maturityamt">Maturity Amount(₹)</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="60000" id="fixeddeposit-maturityamt" name="Fixeddeposit[maturityamt]" value="<?=$model['maturityamt']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-fixeddeposit-beneficiaryname">
					<label class="col-md-2 control-label" for="fixeddeposit-beneficiaryname">Beneficiary Name</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Spouse name, etc.." id="fixeddeposit-beneficiaryname" name="Fixeddeposit[beneficiaryname]" value="<?=$model['beneficiaryname']?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<a class="btn btn-primary" href="/fixeddeposit/create" style="margin-bottom:15px">Add More Fixed Deposits</a>
<a class="btn btn-primary" href="/fixeddeposit/index" style="margin-bottom:15px; float:right;">Back</a>

<div class="panel-info widget-shadow">
					<h3 class="title1">Fixed Deposit</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Asset Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Account Name</th>
									<td><?=$model['accountname']?></td>
								</tr>
								<tr scope="row">
									<th >Bank Name</th>
									<td><?=$model['bankname']?></td>
								</tr>
								<tr scope="row">
									<th >Bank Address</th>
									<td><?=$model['bankaddress']?></td>
								</tr>
								<tr scope="row">
									<th >Scheme Name</th>
									<td><?=$model['scheme']?></td>
								</tr>
								<tr scope="row">
									<th >Interest Rate(%)</th>
									<td><?=$model['interestrate']?></td>
								</tr>
								<tr scope="row">
									<th >Amount(₹)</th>
									<td><?=$model['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Deposit Period(Months)</th>
									<td><?=$model['period']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$model['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >FDR Number</th>
									<td><?=$model['fdrnum']?></td>
								</tr>
								<tr scope="row">
									<th >Maturity Date</th>
									<td><?=$model['maturitydate']?></td>
								</tr>
								<tr scope="row">
									<th >Maturity Amount(₹)</th>
									<td><?=$model['maturityamt']?></td>
								</tr>
								<tr scope="row">
									<th >Beneficiary Name</th>
									<td><?=$model['beneficiaryname']?></td>
								</tr>
								
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div>
							<a type="submit" class="btn btn-primary" href="/fixeddeposit/update/<?=$model['id']?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/fixeddeposit/delete/<?=$model['id']?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
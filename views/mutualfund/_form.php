<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mutualfund */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mutualfund-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clientid')->textInput() ?>

    <?= $form->field($model, 'arn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transactiondate')->textInput() ?>

    <?= $form->field($model, 'folio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'scheme')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'marketvalue')->textInput() ?>

    <?= $form->field($model, 'marketvaluedate')->textInput() ?>

    <?= $form->field($model, 'transactiontypeid')->textInput() ?>

    <?= $form->field($model, 'documentimageid')->textInput() ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'modifieddate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

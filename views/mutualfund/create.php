	<a class="btn btn-primary" href="/mutualfund/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Add New - Mutual Fund Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeMutualfund" class="form-horizontal" action="/mutualfund/create">
			
				<div class="form-group field-mutualfund-clientid">
					<label class="col-md-2 control-label" for="mutualfund-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="mutualfund-clientid" name="Mutualfund[clientid]">
				</div>
				<div class="form-group field-mutualfund-arn">
					<label class="col-md-2 control-label" for="mutualfund-arn">Mutual Fund Issuer*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="HDFC Bank" id="mutualfund-arn" name="Mutualfund[arn]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-folio">
					<label class="col-md-2 control-label" for="mutualfund-folio">Mutual Fund Folio*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-file-text-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Folio" id="mutualfund-folio" name="Mutualfund[folio]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-scheme">
					<label class="col-md-2 control-label" for="mutualfund-scheme">Scheme Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Scheme" id="mutualfund-scheme" name="Mutualfund[scheme]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-transactiontypeid">
					<label class="col-md-2 control-label" for="mutualfund-transactiontypeid">Transaction Type*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="mutualfund-transactiontypeid" name="Mutualfund[transactiontypeid]" required>
								<option value="">Select Transaction Type..</option>
								<?php foreach($transtype as $t):?>
								<option value="<?=$t['id']?>"><?=$t['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-transactiondate">
					<label class="col-md-2 control-label" for="mutualfund-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'attribute' => 'transactiondate','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => ""]]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-amount">
					<label class="col-md-2 control-label" for="mutualfund-amount">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="50000" id="mutualfund-amount" name="Mutualfund[amount]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-marketvalue">
					<label class="col-md-2 control-label" for="mutualfund-marketvalue">Market Value</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="65000" id="mutualfund-marketvalue" name="Mutualfund[marketvalue]">
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-marketvaluedate">
					<label class="col-md-2 control-label" for="mutualfund-marketvaluedate">Market Value Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'attribute' => 'marketvaluedate','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
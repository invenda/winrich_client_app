	<a class="btn btn-primary" href="/mutualfund/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Mutual Fund Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeMutualfund" class="form-horizontal" action="/mutualfund/update/<?=$model['id']?>">
			
				<div class="form-group field-mutualfund-clientid">
					<label class="col-md-2 control-label" for="mutualfund-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="mutualfund-clientid" name="Mutualfund[clientid]">
				</div>
				<div class="form-group field-mutualfund-transactiondate">
					<label class="col-md-2 control-label" for="mutualfund-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['transactiondate'], 'id' => 'mutualfund-transactiondate','name' => 'Mutualfund[transactiondate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => '']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-amount">
					<label class="col-md-2 control-label" for="mutualfund-amount">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="50000" id="mutualfund-amount" name="Mutualfund[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-marketvalue">
					<label class="col-md-2 control-label" for="mutualfund-marketvalue">Market Value</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="65000" id="mutualfund-marketvalue" name="Mutualfund[marketvalue]" value="<?=$model['marketvalue']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-mutualfund-marketvaluedate">
					<label class="col-md-2 control-label" for="mutualfund-marketvaluedate">Market Value Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['marketvaluedate'], 'id' => 'mutualfund-marketvaluedate','name' => 'Mutualfund[marketvaluedate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
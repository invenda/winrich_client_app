	<a class="btn btn-primary" href="/bond/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Bond Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeCommodity" class="form-horizontal" action="/bond/update/<?=$model['id']?>">
				
				<div class="form-group field-bond-clientid">
					<label class="col-md-2 control-label" for="bond-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="bond-clientid" name="Bond[clientid]">
				</div>
				
				<div class="form-group field-bond-units">
					<label class="col-md-2 control-label" for="bond-units">Number of Units*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-list-ol"></i>
							</span>
							<input class="form-control form-control1" type="number" placeholder="100" id="bond-units" name="Bond[units]" value="<?=$model['units']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-amount">
					<label class="col-md-2 control-label" for="bond-scrip">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="20000" id="bond-amount" name="Bond[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-beneficiaryname">
					<label class="col-md-2 control-label" for="bond-beneficiaryname">Beneficiary Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Spouse name, etc" id="bond-beneficiaryname" name="Bond[beneficiaryname]" value="<?=$model['beneficiaryname']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-transactiondate">
					<label class="col-md-2 control-label" for="bond-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['transactiondate'], 'id' => 'bond-transactiondate','name' => 'Bond[transactiondate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => '']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-applicationnum">
					<label class="col-md-2 control-label" for="bond-applicationnum">Application Number*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="1234567" id="bond-applicationnum" name="Bond[applicationnum]" value="<?=$model['applicationnum']?>" required>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<!--<button type="submit" class="btn btn-primary">Edit</button>-->
							<button type="submit" class="btn btn-primary" >Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
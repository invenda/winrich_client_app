<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bonds';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<a class="btn btn-primary" href="/bond/create">Add New Bond</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your Assets</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>Sr No.</th>
									<th>Bond Name</th>
									<th>Units</th>
									<th>Amount(₹)</th>
									<th>Transaction Type</th>
									<th>Manage</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<td scope="row"><?=$count?></td>
										<td><?=$assets['scrip']?></td>
										<td><?=$assets['units']?></td>
										<td><?=$assets['amount']?></td>
										<td><?=$transtype[$assets['transactiontypeid']-1]['description']?></td>
										<td>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/bond/view/<?=$assets['id']?>">View</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/bond/update/<?=$assets['id']?>">Edit</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-danger" href="/bond/delete/<?=$assets['id']?>">Delete</a>
										</td>
									</tr>
							
								<tr>
								<td colspan="6" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table"> 
							<tbody>
								
								<tr scope="row">
									<th >Beneficiary Name</th>
									<td><?=$assets['beneficiaryname']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$assets['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Application Number</th>
									<td><?=$assets['applicationnum']?></td>
								</tr>
								
							</tbody></table>
										<a type="submit" class="btn btn-primary" href="/bond/view/<?=$assets['id']?>">View</a>
										<a type="submit" class="btn btn-primary" href="/bond/update/<?=$assets['id']?>">Edit</a>
										<a type="submit" class="btn btn-danger" href="/bond/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php $count++;?>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Bond */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bond-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clientid')->textInput() ?>

    <?= $form->field($model, 'issuername')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'scrip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'units')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'beneficiaryname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transactiondate')->textInput() ?>

    <?= $form->field($model, 'applicationnum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transactiontypeid')->textInput() ?>

    <?= $form->field($model, 'documentimageid')->textInput() ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'modifieddate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<a class="btn btn-primary" href="/bond/create" style="margin-bottom:15px">Add More Bonds</a>
<a class="btn btn-primary" href="/bond/index" style="margin-bottom:15px; float:right;">Back</a>

<div class="panel-info widget-shadow">
					<h3 class="title1">Bonds</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Asset Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Bond Issuer</th>
									<td><?=$model['issuername']?></td>
								</tr>
								<tr scope="row">
									<th >Scrip Name</th>
									<td><?=$model['scrip']?></td>
								</tr>
								<tr scope="row">
									<th >Number of Units</th>
									<td><?=$model['units']?></td>
								</tr>
								<tr scope="row">
									<th >Amount(₹)</th>
									<td><?=$model['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Beneficiary Name</th>
									<td><?=$model['beneficiaryname']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Type</th>
									<td><?=$model['transdes']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$model['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Application Number</th>
									<td><?=$model['applicationnum']?></td>
								</tr>
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div>
							<a type="submit" class="btn btn-primary" href="/bond/update/<?=$model['id']?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/bond/delete/<?=$model['id']?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
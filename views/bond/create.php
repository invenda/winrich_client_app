	<a class="btn btn-primary" href="/bond/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Add New - Bond Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeBond" class="form-horizontal" action="/bond/create">
			
				<div class="form-group field-bond-clientid">
					<label class="col-md-2 control-label" for="bond-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="bond-clientid" name="Bond[clientid]">
				</div>
				<div class="form-group field-bond-issuername">
					<label class="col-md-2 control-label" for="bond-issuername">Bond Issuer*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="HDFC Bank" id="bond-issuername" name="Bond[issuername]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-scrip">
					<label class="col-md-2 control-label" for="bond-scrip">Scrip Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Franklin India Bluechip" id="bond-scrip" name="Bond[scrip]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-units">
					<label class="col-md-2 control-label" for="bond-units">Number of Units*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-list-ol"></i>
							</span>
							<input class="form-control form-control1" type="number" placeholder="100" id="bond-units" name="Bond[units]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-amount">
					<label class="col-md-2 control-label" for="bond-amount">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="20000" id="bond-amount" name="Bond[amount]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-beneficiaryname">
					<label class="col-md-2 control-label" for="bond-beneficiaryname">Beneficiary Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Spouse name, etc" id="bond-beneficiaryname" name="Bond[beneficiaryname]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-transactiontypeid">
					<label class="col-md-2 control-label" for="bond-transactiontypeid">Transaction Type*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="bond-transactiontypeid" name="Bond[transactiontypeid]" required>
								<option value="">Select Transaction Type..</option>
								<?php foreach($transtype as $t):?>
								<option value="<?=$t['id']?>"><?=$t['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-transactiondate">
					<label class="col-md-2 control-label" for="bond-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'attribute' => 'transactiondate','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => ""]]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-bond-applicationnum">
					<label class="col-md-2 control-label" for="bond-applicationnum">Application Number*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="1234567" id="bond-applicationnum" name="Bond[applicationnum]" required>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reference Data';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<a class="btn btn-primary" href="/reference/create">Add New Reference</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your References</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>Sr No.</th>
									<th>Reference</th>
									<th>Name</th>
									<th>Office Address</th>
									<th>Manage</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<td scope="row"><?=$count?></td>
										<td><?=$assets['bankname']?></td>
										<td><?=$assets['primaryholder']?></td>
										<td><?=$assets['accountnum']?></td>
										<td>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/reference/view/<?=$assets['id']?>">View</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/reference/update/<?=$assets['id']?>">Edit</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-danger" href="/reference/delete/<?=$assets['id']?>">Delete</a>
										</td>
									</tr>
							
								<tr>
								<td colspan="5" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table"> 
							<tbody>
								
								<tr scope="row">
									<th >Joint Account Holder</th>
									<td><?=$assets['jointholder']?></td>
								</tr>
								<tr scope="row">
									<th >Bank Address</th>
									<td><?=$assets['bankaddress']?></td>
								</tr>
								<tr scope="row">
									<th >Bank IFSC Code</th>
									<td><?=$assets['ifsc']?></td>
								</tr>
								<tr scope="row">
									<th >Account MICR Code</th>
									<td><?=$assets['micr']?></td>
								</tr>
								<tr scope="row">
									<th >Account Active</th>
									<td><?=$assets['isactive']?></td>
								</tr>
								
							</tbody></table>
										<a type="submit" class="btn btn-primary" href="/reference/view/<?=$assets['id']?>">View</a>
										<a type="submit" class="btn btn-primary" href="/reference/update/<?=$assets['id']?>">Edit</a>
										<a type="submit" class="btn btn-danger" href="/reference/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php $count++;?>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
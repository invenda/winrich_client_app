	<a class="btn btn-primary" href="/reference/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Reference Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeCommodity" class="form-horizontal" action="/reference/update/<?=$model['id']?>">
				
				<div class="form-group field-reference-clientid">
					<label class="col-md-2 control-label" for="reference-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="reference-clientid" name="Reference[clientid]">
				</div>
				
				<div class="form-group field-reference-primaryholder">
					<label class="col-md-2 control-label" for="reference-primaryholder">Primary Account Holder*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Abhi Rao" id="reference-primaryholder" name="Reference[primaryholder]" value="<?=$model['primaryholder']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-reference-jointholder">
					<label class="col-md-2 control-label" for="reference-jointholder">Joint Account Holder</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Priya Rao" id="reference-jointholder" name="Reference[jointholder]" value="<?=$model['jointholder']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-reference-nominee">
					<label class="col-md-2 control-label" for="reference-nominee">Nominee Name</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-male"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Priya Rao" id="reference-nominee" name="Reference[nominee]" value="<?=$model['nominee']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-reference-bankname">
					<label class="col-md-2 control-label" for="reference-bankname">Bank Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="SBI Bank" id="reference-bankname" name="Reference[bankname]" value="<?=$model['bankname']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-reference-bankaddress">
					<label class="col-md-2 control-label" for="reference-bankaddress">Bank Address*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Jayanagar, Bengaluru" id="reference-bankaddress" name="Reference[bankaddress]" value="<?=$model['bankaddress']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-reference-accountnum">
					<label class="col-md-2 control-label" for="reference-accountnum">Bank Account Number*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="BL12356..." id="reference-accountnum" name="Reference[accountnum]" value="<?=$model['accountnum']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-reference-ifsc">
					<label class="col-md-2 control-label" for="reference-ifsc">Bank IFSC Code</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="SBIB000011.." id="reference-ifsc" name="Reference[ifsc]" value="<?=$model['ifsc']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-reference-micr">
					<label class="col-md-2 control-label" for="reference-micr">Account MICR Code</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="011003511..." id="reference-micr" name="Reference[micr]" value="<?=$model['micr']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-reference-isactive">
					<label class="col-md-2 control-label" for="reference-isactive">Account Active*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="reference-isactive" name="Reference[isactive]" required>
								<option value="">Select One..</option>
								<?php if($model['isactive'] == "Yes"):?>
									<option value="Yes" selected>Yes</option>
									<option value="No">No</option>
								<?php endif; ?>
								<?php if($model['isactive'] == "No"):?>
									<option value="Yes">Yes</option>
									<option value="No" selected>No</option>
								<?php endif; ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<!--<button type="submit" class="btn btn-primary">Edit</button>-->
							<button type="submit" class="btn btn-primary" >Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php

use app\components\HeaderWidget;
use yii\helpers\Html;

$userId = Yii::$app->user->id;
$username = Yii::$app->user->getIdentity()->username;
?>
<?php $this->beginPage() ?>
<!DOCTYPE HTML>
<html>
<?= $this->render('@app/views/layouts/header', $_params_) ?>
<body class="cbp-spmenu-push">
	<div class="main-content">
	<?= $this->render('@app/views/layouts/sidebar', $_params_) ?>
    <?= HeaderWidget::widget(['username' => $username, 'userId' => $userId]) ?>
	<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<?= $content ?>
			</div>
		</div>
		<?= $this->render('@app/views/layouts/footer', $_params_) ?>
		</div>
		<script>
			$(document).ready(function() {
			if (window.location.href != "http://client.winrich.in/"){
				$("#menu0").removeClass("active");
			}
			});
		</script>
	<!-- Classie -->
		<script src="/js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			

			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!--scrolling js-->
	<script src="/js/jquery.nicescroll.js"></script>
	<script src="/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="/js/bootstrap.js"> </script>
   <?php $this->endBody() ?>
		</body>
</html>
<?php $this->endPage() ?>
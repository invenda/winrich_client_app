<?php $instruments = [
							['id'=>1, 'description'=>'Reference','icon'=>'fa-users', 'link' =>'/reference'],
							['id'=>2, 'description'=>'Bond','icon'=>'fa-briefcase', 'link' =>'/bond'],
							['id'=>3, 'description'=>'Equity','icon'=>'fa-bar-chart', 'link' =>'/equity'],
							['id'=>4, 'description'=>'Fixed Deposits','icon'=>'fa-rupee', 'link' =>'/fixeddeposit'],
							['id'=>5, 'description'=>'Insurance','icon'=>'fa-pencil-square', 'link' =>'/insurance'],
							['id'=>6, 'description'=>'Mutual Fund','icon'=>'fa-trello', 'link' =>'/mutualfund'],
							['id'=>7, 'description'=>'Real Estate','icon'=>'fa-check-square-o', 'link' =>'/realestate'],
							['id'=>8, 'description'=>'Commodity','icon'=>'fa-file-text-o', 'link' =>'/commodity'],
							['id'=>9, 'description'=>'Bank Accounts','icon'=>'fa-money', 'link' =>'/bankaccount'],
							['id'=>10, 'description'=>'Documents','icon'=>'fa-briefcase', 'link' =>'/documents'],
							['id'=>11, 'description'=>'Lockers','icon'=>'fa-lock', 'link' =>'/lockers'],
						];
?>
<!--left-fixed -navigation-->
		<div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
					<ul class="nav" id="side-menu">
						<li>
							<a href="/" id="menu0"><i class="fa fa-home nav_icon"></i>Home</a>
						</li>
						<?php
						foreach($instruments as $ins):?>
							<li><a href="<?=$ins['link']?>" id="menu<?=$ins['id']?>"><i class="fa <?=$ins['icon']?> nav_icon"></i><?=$ins['description']?></a></li>
						
						<?php endforeach ?>
						<li>
							<a href="/report/consolidated" id="menuReport"><i class="fa fa-file-text-o nav_icon"></i>Report</a>
						</li>
					</ul>
					<!-- //sidebar-collapse -->
				</nav>
			</div>
		</div>
		<!--left-fixed -navigation-->
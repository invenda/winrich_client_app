<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Frequently Asked Questions';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<table class="table">
						
							<tr><td>
							<h5>1. What is Winrich Client Portal?</h5>
							<p style="padding-left:15px;">It is an application made exclusively for Winrich clients where they can securely store their financial information. This application is like a record keeping book which can be accessed anytime. The records can be added, edited or deleted.<p>
							</tr></td>
							
							<tr><td>
							<h5>2. Am I eligible to use Winrich Client Portal?</h5>
							<p style="padding-left:15px;">Winrich Client Portal can only be used by Winrich clients. The application can be used by you only after the Winrich team grants access to your email address.<p>
							</tr></td>
							
							<tr><td>
							<h5>3. How do I sign up at Winrich Client Portal?</h5>
							<p><ul style="padding-left:22px;">
							<li>Open the <a href="https://client.winrich.in/site/signup">Sign Up</a> page in your web browser.</li>
							<li>Enter your email address with which your Winrich Client Portal account has been set up.</li>
							<li>Check the box, ‘By registering, I accept to the Terms and Conditions’.</li>
							<li>On clicking ‘Sign Up’, an email is sent to your email address with a link with your default password. For security reasons, you are advised to change the password by clicking on the ‘Change password’ link after logging in for the first time.</li>
							</ul><p>
							</tr></td>
							
							<tr><td>
							<h5>4. How do I log into Winrich Client Portal?</h5>
							<p style="padding-left:15px;">Once you have set up your account, head to the <a href="https://client.winrich.in/site/login">Sign in</a> page. Here enter your email and password and click on ‘Sign in’ button to sign into your Winrich account.<p>
							</tr></td>
							
							<tr><td>
							<h5>5. I have forgotten my password? How do I recover it?</h5>
							<p><ul style="padding-left:22px;">
							<li>Go to <a href="https://client.winrich.in/site/forgotpassword">reset password page</a>.</li>
							<li>Enter your email associated with your Winrich Client Portal account.</li>
							<li>On clicking ‘Reset Password’, you will receive an email with a link to reset your password. The reset password link is valid for only 15 minutes.</li>
							<li>On clicking the link, you will be routed to ‘Reset Password’ page where you are required to enter your new password.</li>
							<li>You can now access your Winrich account with the new password.</li>
							</ul><p>
							</tr></td>
							
							<tr><td>
							<h5>6. How do I change my existing password?</h5>
							<p><ul style="padding-left:22px;">
							<li>Click on the User profile icon in the upper right corner of the application.</li>
							<li>Click on the reset password link.</li>
							<li>Enter the Old password, new password and confirm the new password.</li>
							<li>Click on the ‘Reset’ button.</li>
							</ul>
							After the password is successfully changed, you will receive an email informing you that your password was recently changed. If you think you didn’t change your password and there is some unknown activity in your account, contact the Winrich team as soon as possible.
							<p>
							</tr></td>
							
							<tr><td>
							<h5>7. What type of asset details can I add in the Winrich portal?</h5>
							<p style="padding-left:15px;">Winrich Client Portal supports adding details of various types of assets. You can add the following:</p>
							<p>
							<ul style="padding-left:22px;">
							<li>Bonds</li>
							<li>Equity</li>
							<li>Fixed Deposits</li>
							<li>Insurance</li>
							<li>Mutual Funds</li>
							<li>Commodity</li>
							<li>Bank Accounts</li>
							</ul>
							<p>
							</tr></td>
							
							<tr><td>
							<h5>8. How do I add my asset details in Winrich Client Portal?</h5>
							<p><ul style="padding-left:22px;">
							<li>Click on the type of asset from the side menu bar for which the details are to be added.</li>
							<li>This will take you to the list of all the asset objects under that category.</li>
							<li>Click on the add button and fill up all the mandatory details of the asset object. Click on the add button again to successfully add the object.</li>
							</ul><p>
							</tr></td>
							
							<tr><td>
							<h5>9. What is user profile?</h5>
							<p style="padding-left:15px;">User profile is the section where you can add your personal details like Name, Address, Mobile number, PAN Card, etc. This helps you bring all your details in one place and thus provides easy access to your personal data.<p>
							</tr></td>
							
							<tr><td>
							<h5>10. What are reports?</h5>
							<p style="padding-left:15px;">Reports show a consolidated value of all the assets you have fed into the portal. It shows you the sum of all buys and sells across all the asset types to give you a rounded view of all your possessions. You can also download the report by clicking on ‘Download report’ button. The report is download in excel format for your local record.<p>
							</tr></td>
							
							<tr><td>
							<h5>11. How secure is the data that I feed into Winrich Client Portal?</h5>
							<p style="padding-left:15px;">Winrich Client Portal uses industry standard methods to protect your data including the HTTPS protocol to keep a secure connection between the Application and the client machine. For more details, you can look at the ‘<a href="https://client.winrich.in/site/terms">Disclaimer</a>’ section.
<p>
							</tr></td>
							
							<tr><td>
							<h5>12. How did I logout from my account?</h5>
							<p><ul style="padding-left:22px;">
							<li>Click on the User profile icon in the upper right corner of the application.</li>
							<li>Now click on the ‘Logout’ link and you will be logged out from your Winrich account.</li>
							</ul><p>
							</tr></td>
						
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
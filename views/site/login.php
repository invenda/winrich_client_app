<?php

use yii\helpers\Html;

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Winrich</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
 <!-- js-->
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts--> 
<!--animate-->
<link href="/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="/js/metisMenu.min.js"></script>
<script src="/js/custom.js"></script>
<link href="/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<?= Html::csrfMetaTags() ?>
</head> 
<body >
	<div class="main-content">

		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page login-page ">
				<h3 class="title1">WinRich Client Login</h3>
				<div class="widget-shadow">
					<div class="login-top">
						<h4>Welcome back to WinRich Login ! <br> Not a Member? <a href="/site/signup">  Sign Up »</a> </h4>
					</div>
					<div class="login-body">
						<form id="login-form" class="form-horizontal" action="/site/login" method="post" role="form">
						<input type="hidden" name="_csrf" value="UUlOejBwMTFlBCsbejple2AGYzVyGwhANnwoI3cTaFk3Dn8cekVmYw==">
						
							<input type="text" id="loginform-username" class="form-control user" name="LoginForm[username]" placeholder="Enter your email" value="<?php if(!empty($username)):?><?=$username?><?php endif; ?>" required="" autofocus>
							
							<input type="password" id="loginform-password" class="form-control lock" name="LoginForm[password]" placeholder="Password">
							
							<input type="submit" id="login-button" name="login-button" value="Sign In">
							
							<div class="forgot-grid">
																
								<input type="checkbox" id="loginform-rememberme" name="LoginForm[rememberMe]" value="1" checked> <label for="loginform-rememberme">Remember Me</label>
								
								<div class="forgot">
									<a href="/site/forgotpassword">forgot password?</a>
								</div>
								<div class="clearfix"> </div>
							</div>
						</form>
					</div>
				</div>
				<div id="result" >
					<?php if(!empty($message)):?>
						<?=$message?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2017 WinRich. All Rights Reserved</p>
		</div>
        <!--//footer-->
	</div>
	<!-- Classie -->
		<script src="/js/classie.js"></script>
	<!--scrolling js-->
	<script src="/js/jquery.nicescroll.js"></script>
	<script src="/js/scripts.js"></script>
	<script>
		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		function validate() {
			$("#result").html("");
			var email = $("#loginform-username").val();
			if (validateEmail(email)) {
				$("#login-form").submit();
			} else {
				$("#result").html('<div style=\"color:red !important;\"><p><center>Please enter a valid email to proceed<center></p></div>');
			}
			return false;
		}

		$("#login-button").bind("click", validate);
	</script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="/js/bootstrap.js"> </script>
</body>
</html>
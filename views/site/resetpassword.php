<?php

use yii\helpers\Html;

?>
<!DOCTYPE HTML>
<html>
<head>
<title>Winrich</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
 <!-- js-->
<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts--> 
<!--animate-->
<link href="/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="/js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="/js/metisMenu.min.js"></script>
<script src="/js/custom.js"></script>
<link href="/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<?= Html::csrfMetaTags() ?>
</head> 
<body >
	<div class="main-content">

		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page login-page ">
				<h3 class="title1">WinRich Client</h3>
				<div class="widget-shadow">
					<div class="login-top">
						<h4>Password Reset</h4>
					</div>
					<div class="login-body">
						<form id="resetpassword-form" class="form-horizontal" action="/site/resetpassword/<?=$token?>" method="post" role="form">
						<input type="hidden" name="_csrf" value="UUlOejBwMTFlBCsbejple2AGYzVyGwhANnwoI3cTaFk3Dn8cekVmYw==">
						
							<input type="password" id="newpassword" class="form-control user" name="newpassword" placeholder="Enter new Password" required="" autofocus>
							
							<input type="password" id="newpasswordrepeat" class="form-control lock" name="newpasswordrepeat" placeholder="Re-type Password">
							
							<input type="submit" name="login-button" value="Reset Password">
							
							<div class="forgot-grid">
								<div class="forgot" style="float:left !important; "><a href="/site/login">Sign In</a><br></div>
								<div class="forgot">
									<a href="#">forgot password?</a>
								</div>
								<div class="clearfix"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div id="errordiv">
				<?php if(!empty($message)):?><?=$message?><?php endif; ?>
			</div>
		</div>
		<script>
	$('#resetpassword-form').submit(function(e){
			$("#errordiv").html('');
			return validate();
    	});
	function validate() {
		
		// The two password inputs
		var newPassword = $("#newpassword").val();
		var confirmPassword = $("#newpasswordrepeat").val();

		if (newPassword != "" && confirmPassword != "") {	
				if (newPassword != confirmPassword ) {
					$("#errordiv").html('<div style=\"color:red !important;\"><p><center>Your passwords do not match<center></p></div>');
				} else {
					return true;
				}
			
		} else {
			$("#errordiv").html('<div style=\"color:red !important;\"><p><center>None of the password reset fields cannot be empty<center></p></div>');
		}
		return false;

	}
	</script>
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2017 WinRich. All Rights Reserved</p>
		</div>
        <!--//footer-->
	</div>
	<!-- Classie -->
		<script src="/js/classie.js"></script>
	<!--scrolling js-->
	<script src="/js/jquery.nicescroll.js"></script>
	<script src="/js/scripts.js"></script>
	<!--//scrolling js-->
	<!-- Bootstrap Core JavaScript -->
   <script src="/js/bootstrap.js"> </script>
</body>
</html>
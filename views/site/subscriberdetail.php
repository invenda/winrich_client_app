	<h3 class="title1">Subscriber Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="userCheckForm" class="form-horizontal" action="">
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="usercheck">Usernames to Check</label>
					<div class="col-md-8">
						<div class="input-group">
							<input class="form-control form-control1" type="text" placeholder="Comma separated Usernames" id="usercheck" name="usercheck">
						</div>
					</div>
				</div>
				
				
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<input id="reset-button" type="submit" onclick="getUser()" class="btn btn-primary" value="Check">
						</div>
					</div>
				</div>
			</form>
		</div>
		<div id="errordiv">
		<?php if(!empty($message)):?><?=$message?><?php endif; ?>
		</div>
		<br>
		<h4>Your Assets</h4>
			<table class="table"> 
			
				<thead> 
					<tr> 
						<th>Sr No.</th>
						<th>Username</th>
						<th>User ID</th>
						<th>User Available</th>
						<th>Status</th>
						<th>Manage</th>
					</tr> 
				</thead>
				<tbody id="user-details">
				</tbody>
							
						</table>
	</div>

	<script>
		function getUser()
		{
			$('#userCheckForm').on('submit', function(e){
			  e.preventDefault();
			});
			//$('#user-details').html('');
			var usernames = $('#usercheck').val();
			var requestUser = $.ajax({
                type: "POST",
                url: "/site/subscriberenquiry",
                data: {
                    'usernames': usernames
                }
            });
			var userHtml = "";
            requestUser.done(function (msg) {
				
                obj = JSON.parse(msg);
                if (obj.status == "success") {
                    var userData = obj.data;
					for(var i=0; i < userData.length; i++){
						if(userData[i].available == "Available"){
							userHtml += '<tr scope="row" role="button" ><td scope="row">'+userData[i].serialnum+'</td><td>'+userData[i].username+'</td><td>'+userData[i].id+'</td><td>'+userData[i].available+'</td><td id="isactive'+userData[i].id+'">'+userData[i].isactive+'</td><td><a type="submit" onclick="updateUserStatus(\''+userData[i].id+'\',1)" class="btn btn-primary" >Activate</a><a type="submit" onclick="updateUserStatus(\''+userData[i].id+'\',0)" class="btn btn-primary" >Inactivate</a><a type="submit" onclick="updateUserStatus(\''+userData[i].id+'\',2)" class="btn btn-danger" >Deavtivate</a></td></tr>';
						}else{
							userHtml += '<tr scope="row" role="button" ><td scope="row">'+userData[i].serialnum+'</td><td>'+userData[i].username+'</td><td>'+userData[i].id+'</td><td>'+userData[i].available+'</td><td id="isactive'+userData[i].id+'">'+userData[i].isactive+'</td><td></td></tr>';
						}
					}
					
					$('#user-details').html(userHtml);
                    
					//alert('inside response');
                } else {
                    alert(obj.message);
                }
            });
			
		}
		
		function updateUserStatus(userId, isactive)
		{
			//$('#user-details').html('');
			
			var requestUpdate = $.ajax({
                type: "POST",
                url: "/site/subscriberstatuschange",
                data: {
                    'id': userId,
                    'isactive': isactive
                }
            });
			var activeHtml = "";
			requestUpdate.done(function (msg) {
				
                obj = JSON.parse(msg);
                if (obj.status == "success") {
                    activeHtml = obj.isactive;
					var divref = '#isactive'+userId;
					$(divref).html(activeHtml);
                    alert(obj.message);
                } else {
                    alert(obj.message);
                }
            });
			
		}
		
	</script>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */

$this->title = 'Home';
?>
<h3 class="title1"><?= Html::encode($this->title) ?></h3>
<!--<div class="row-one">
					<div class="col-md-4 widget">
						<div class="stats-left " style="width:100% !important;">
							<h5>SENSEX</h5>
							<h4><?php if(!empty($bse)):?>₹<?=$bse?><?php else:?>Not Available<?php endif;?></h4>
						</div>
						<div class="clearfix"> </div>	
					</div>
					<div class="col-md-4 widget states-mdl">
						<div class="stats-left " style="width:100% !important;">
							<h5>USD</h5>
							<h4><?php if(!empty($usd)):?>₹<?=$usd?><?php else:?>Not Available<?php endif;?></h4>
						</div>
						<div class="clearfix"> </div>	
					</div>
					<div class="col-md-4 widget states-last">
						<div class="stats-left " style="width:100% !important;">
							<h5>GOLD</h5>
							<h4><?php if(!empty($gold)):?>₹<?=$gold?><?php else:?>Not Available<?php endif;?></h4>
						</div>
						<div class="clearfix"> </div>	
					</div>
					<div class="clearfix"> </div>	
				</div>-->
				<!--<div class="charts">
					<div id="aumDiv" class="col-md-4 charts-grids widget">
						<h4 class="title">AUM</h4>
						<canvas id="aumGraph" height="300" width="400"> </canvas>
					</div>
					<div id="sipDiv" class="col-md-4 charts-grids widget states-mdl">
						<h4 class="title">SIP</h4>
						<canvas id="sipGraph" height="300" width="400"> </canvas>
					</div>
					<div id="investmentDiv" class="col-md-4 charts-grids widget">
						<h4 class="title">Investment</h4>
						<canvas id="investmentGraph" height="300" width="400"> </canvas>
					</div>
					<?php ?>
					<div class="clearfix"> </div>
							 <script>
								<?php if(!empty($aum)): ?>
									var aumData = {
										labels : [<?php foreach($aum as $a): ?>'<?=$a->createdate?>',<?php endforeach?>],
										datasets : [
											{
												fillColor : "rgba(233, 78, 2, 0.9)",
												strokeColor : "rgba(233, 78, 2, 0.9)",
												highlightFill: "#e94e02",
												highlightStroke: "#e94e02",
												data : [<?php foreach($aum as $a): ?><?=$a->clients[0]->total?>,<?php endforeach?>]
											}
										]
										
									};
									new Chart(document.getElementById("aumGraph").getContext("2d")).Line(aumData);
								<?php else: ?>	
									document.getElementById("aumDiv").innerHTML = '<h4 class="title">AUM</h4><canvas id="aumGraph" height="291" width="400"></canvas><p>No Data</p>';
								<?php endif; ?>	
								
								<?php if(!empty($sip)): ?>
									var sipData = {
										labels : [<?php foreach($sip as $s): ?>'<?=$s->createdate?>',<?php endforeach?>],
										datasets : [
											{
												fillColor : "rgba(242, 179, 63, 1)",
												strokeColor : "#F2B33F",
												pointColor : "rgba(242, 179, 63, 1)",
												pointStrokeColor : "#fff",
												data : [<?php foreach($sip as $s): ?><?=$s->total?>,<?php endforeach?>]

											}
										]
										
									};
									new Chart(document.getElementById("sipGraph").getContext("2d")).Bar(sipData);
								<?php else: ?>	
									document.getElementById("sipDiv").innerHTML = '<h4 class="title">SIP</h4><canvas id="sipGraph" height="291" width="400"></canvas><p>No Data</p>';
								<?php endif; ?>
									
								<?php if(!empty($investment)): ?>
									var investmentData = {
										labels : [<?php foreach($investment as $in): ?>'<?=$in->createdate?>',<?php endforeach?>],
										datasets : [
											{
												fillColor : "rgba(242, 179, 63, 1)",
												strokeColor : "#F2B33F",
												pointColor : "rgba(242, 179, 63, 1)",
												pointStrokeColor : "#fff",
												data : [<?php foreach($investment as $in): ?><?=$in->total?>,<?php endforeach?>]

											}
										]
										
									};
									new Chart(document.getElementById("investmentGraph").getContext("2d")).Bar(investmentData);
								<?php else: ?>	
									document.getElementById("investmentDiv").innerHTML = '<h4 class="title">Investment</h4><canvas id="investmentGraph" height="291" width="400"></canvas><p>No Data</p>';
								<?php endif; ?>
							
							</script>
							
				</div>-->
				<div >
					<div style="padding-left:15px;" class="widget-shadow">
					<a class="twitter-timeline" data-lang="en" data-width="950" data-height="400" href="https://twitter.com/sridharawinrich">Tweets by sridharawinrich</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
						
					</div>
					
					<div class="clearfix"> </div>
				</div>
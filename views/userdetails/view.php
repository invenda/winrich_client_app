<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<div class="panel-info widget-shadow">
					<h3 class="title1">Your Profile</h3>
					
					<div class="form-group">
						<div class="col-md-8">
							<div class="input-group">
								<a type="submit" class="btn btn-primary" href="/userdetails/update/<?php echo $userid ?>">Edit</a>
							</div>
						</div>
					</div><br><br>
					
					<div class="col-md-12 panel-grids">
					
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Profile Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >First Name</th>
									<td> 
                        <?php if (isset($model['firstname'])) 
                                echo  $model['firstname'];
                                      ?>
                                    </td>
								</tr>
								<tr scope="row">
									<th >Last Name</th>
									<td><?php if (isset($model['lastname']))
                                     echo $model['lastname'];
                                      ?>
                                    </td>
								</tr>
								<tr scope="row">
									<th >Gender</th>
									<td><?php if (isset($model['gender'])) echo $model['gender'];?></td>
								</tr>
								<tr scope="row">
									<th >Birthday</th>
									<td><?php if (isset($model['birthdate'])) echo   date("d/m/Y", strtotime($model['birthdate'])); ?>
                                      </td>
								</tr>
								<tr scope="row">
									<th >Address 1</th>
									<td><?php if (isset($model['address1'])) echo $model['address1'];?></td>
								</tr>
								<tr scope="row">
									<th >Address 2</th>
									<td><?php if (isset($model['address2'])) echo $model['address2'];?></td>
								</tr>
								<tr scope="row">
									<th >Country</th>
									<td><?php if (isset($model['country']['description'])) echo $model['country']['description'];?></td>
								</tr>
								<tr scope="row">
									<th >State</th>
									<td><?php if (isset($model['state']['description'])) echo $model['state']['description'];?></td>
								</tr>
								<tr scope="row">
									<th >City</th>
									<td><?php if (isset($model['city'])) echo $model['city'];?></td>
								</tr>
								<tr scope="row">
									<th >Postal Code</th>
									<td><?php if (isset($model['postalcode'])) echo $model['postalcode'];?></td>
								</tr>
								<tr scope="row">
									<th >Mobile</th>
									<td><?php if (isset($model['mobile'])) echo $model['mobile'];?></td>
								</tr>
								<tr scope="row">
									<th >Phone</th>
									<td><?php if (isset($model['phone'])) echo $model['phone'];?></td>
								</tr>
								
								<tr scope="row">
									<th >Email</th>
									<td><?php if (isset($model['email'])) echo $model['email'];?></td>
								</tr>
								<tr scope="row">
									<th >Website</th>
									<td><?php if (isset($model['website'])) echo $model['website'];?></td>
								</tr>
								<tr scope="row">
									<th >Facebook Profile</th>
									<td><?php if (isset($model['facebookid'])) echo $model['facebookid'];?></td>
								</tr>
								<tr scope="row">
									<th >Twitter Handle</th>
									<td><?php if (isset($model['twitterhandle'])) echo $model['twitterhandle'];?></td>
								</tr>
								<tr scope="row">
									<th >Passport Number</th>
									<td><?php if (isset($model['passport'])) echo $model['passport'];?></td>
								</tr>
								<tr scope="row">
									<th >Aadhar Number</th>
									<td><?php if (isset($model['aadhar'])) echo $model['aadhar'];?></td>
								</tr>
								<tr scope="row">
									<th >Voter ID Number</th>
									<td><?php if (isset($model['voterid'])) echo $model['voterid'];?></td>
								</tr>
								<tr scope="row">
									<th >Provident Fund Number</th>
									<td><?php if (isset($model['pf'])) echo $model['pf'];?></td>
								</tr>
								
								<tr scope="row">
									<th >PPF Number</th>
									<td><?php if (isset($model['ppf'])) echo $model['ppf'];?></td>
								</tr>
								<tr scope="row">
									<th >Super Annuation Number</th>
									<td><?php if (isset($model['superannuation'])) echo $model['superannuation'];?></td>
								</tr>
								<tr scope="row">
									<th >PAN Number</th>
									<td><?php if (isset($model['pan'])) echo $model['pan'];?></td>
								</tr>
								<tr scope="row">
									<th >Driving License Number</th>
									<td><?php if (isset($model['drivinglicense'])) echo $model['drivinglicense'];?></td>
								</tr>
								<tr scope="row">
									<th >PRAN Number</th>
									<td><?php if (isset($model['pran'])) echo $model['pran'];?></td>
								</tr>
                                <tr>
                                  <th>Documents:</th>
                                  <th>
                                    <?php foreach($userImage as $key=>$value): ?>
                                    <div class="imgWrap">
                                      <?php echo strpos($value['url'], '.pdf') == false ?  '<img src="'. $s3route.'/'. $value['url']. '"
                                      style="width:50px;height:50px;">'
                                      :'<img src="/images/pdf-icon.png" style="width:50px;height:50px;" onclick="downloadPdf('.'\''. $s3route.'/'.$value['url'].'\''.')">' ?>
                                      <label><?php echo $value['filelabel'] ?></label>
                                    </div>
                                    <?php endforeach; ?>
                                  </th>
                                </tr>
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-8">
							<div class="input-group">
								<a type="submit" class="btn btn-primary" href="/userdetails/update/<?php echo $userid ?>">Edit</a>
							</div>
						</div>
					</div>
				<div class="clearfix"> </div>
				
				</div>
                <script>
                  function downloadPdf(url){
                    window.open(url);
                  }
                    
                </script>
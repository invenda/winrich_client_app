	<h3 class="title1">Reset Password</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="resetPasswordForm" class="form-horizontal" action="/userdetails/changepassword/<?=$userId?>">
				<div class="form-group field-assets-instrumentid">
					<label class="col-md-2 control-label" for="assets-instrumentid"></label>
					<input type="hidden" id="clientid" class="form-control" name="clientid" value="<?=$userId?>">
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="oldpassword">Old Password</label>
					<div class="col-md-8">
						<div class="input-group">
							<input class="form-control form-control1" type="password" placeholder="Old Password" id="oldpassword" name="oldpassword" >
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="newpassword">New Password</label>
					<div class="col-md-8">
						<div class="input-group">
							<input class="form-control form-control1" type="password" placeholder="New Password" id="newpassword" name="newpassword" >
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-md-2 control-label" for="newpasswordrepeat">Repeat New Password</label>
					<div class="col-md-8">
						<div class="input-group">
							<input class="form-control form-control1" type="password" placeholder="Re-type New Password" id="newpasswordrepeat" name="newpasswordrepeat" >
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<input id="reset-button" type="submit" class="btn btn-primary" value="Reset">
						</div>
					</div>
				</div>
			</form>
		</div>
		<div id="errordiv">
		<?php if(!empty($message)):?><?=$message?><?php endif; ?>
		</div>
	</div>
	<script>
	$('#resetPasswordForm').submit(function(e){
			$("#errordiv").html('');
			return validate();
    	});
	function validate() {
		
		// The three password inputs
		var oldPassword = $("#oldpassword").val();
		var newPassword = $("#newpassword").val();
		var confirmPassword = $("#newpasswordrepeat").val();

		if (oldPassword != "" && newPassword != "" && confirmPassword != "") {
			if(oldPassword != newPassword) {	
				if (newPassword != confirmPassword ) {
					$("#errordiv").html('<div style=\"color:red !important;\"><p><center>Your passwords do not match<center></p></div>');
				} else {
					return true;
				}
			} else {
				$("#errordiv").html('<div style=\"color:red !important;\"><p><center>Old and new passwords cannot be same<center></p></div>');
			}
		} else {
			$("#errordiv").html('<div style=\"color:red !important;\"><p><center>None of the password reset fields cannot be empty<center></p></div>');
		}
		return false;

	}
	</script>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Userdetails */

$this->title = 'Create Userdetails';
$this->params['breadcrumbs'][] = ['label' => 'Userdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userdetails-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

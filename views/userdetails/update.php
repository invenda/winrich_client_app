	<h3 class="title1">Update Profile Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeProfile" class="form-horizontal" action="/userdetails/update/<?php echo $userid?>" enctype="multipart/form-data">
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" id="ud-updateButton" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
				<div class="form-group field-userdetails-clientid">
					<label class="col-md-2 control-label" for="userdetails-clientid"></label>
					<!--<input type="hidden" id="userdetails-id" class="form-control" name="Userdetails[id]" value="<?php/*$model['id']*/?>">
					<input type="hidden" id="userdetails-clientid" class="form-control" name="Userdetails[clientid]" value="<?php/*$model['clientid']*/?>">-->
				</div>
								
				<div class="form-group field-userdetails-firstname">
					<label class="col-md-2 control-label" for="userdetails-firstname">First Name</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="John" id="userdetails-firstname" name="Userdetails[firstname]" value="<?php if (isset($model['firstname'])) 
                                echo  $model['firstname'];
                                      ?>" maxlength="100">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-lastname">
					<label class="col-md-2 control-label" for="userdetails-lastname">Last Name</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Doe" id="userdetails-lastname" name="Userdetails[lastname]" value="<?php if (isset($model['lastname']))
                                     echo $model['lastname'];
                                      ?>" maxlength="100">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-gender">
					<label class="col-md-2 control-label" for="userdetails-gender">Gender</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<select class="form-control1"  id="userdetails-gender" name="Userdetails[gender]">
								<option value="">Select Gender..</option>
								<?php foreach($gender  as $key=>$val): ?>
                                  <option value="<?php echo $val ?>" <?php if( isset($model['gender']) && $val == $model['gender']) echo 'selected="selected"';?>> 
                                    <?php echo $val; ?>
                                  </option>
                                <?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-birthdate">
					<label class="col-md-2 control-label" for="userdetails-birthdate">Birth Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<?php echo yii\jui\DatePicker::widget(['model' => $model,'value'=> (isset($model['birthdate']) == true ? $model['birthdate'] : ''), 'id' => 'userdetails-birthdate','name' => 'Userdetails[birthdate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => '1980-12-25']])  ?>
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-address1">
					<label class="col-md-2 control-label" for="userdetails-address1">Address 1</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Address 1" id="House No..." name="Userdetails[address1]" value="<?php if (isset($model['address1'])) echo $model['address1'];?>" maxlength="255">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-address2">
					<label class="col-md-2 control-label" for="userdetails-address2">Address 2</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Street, Locality..." id="userdetails-address2" name="Userdetails[address2]" value="<?php if (isset($model['address2'])) echo $model['address2'];?>" maxlength="255">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-city">
					<label class="col-md-2 control-label" for="userdetails-city">City</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Mumbai" id="userdetails-city" name="Userdetails[city]" value="<?php if (isset($model['city'])) echo $model['city'];?>" maxlength="100">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-countryid">
					<label class="col-md-2 control-label" for="userdetails-countryid">Country</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<select class="form-control1" onChange=addStates()  id="userdetails-countryid" name="Userdetails[countryid]">
                                <option value="">Select a Country..</option>
								<?php foreach($country  as $key=>$val): ?>
                                  <option value="<?php echo $val['id'] ?>" <?php if( isset($model['country']) && $val['id'] == $model['country']['id']) echo 'selected="selected"';?>> 
                                    <?php echo $val['description'] ?>
                                  </option>
                                <?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-stateid">
					<label class="col-md-2 control-label" for="userdetails-stateid">State</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<select class="form-control1"  id="userdetails-stateid" name="Userdetails[stateid]">
								<option value="">Select a State..</option>
								<?php foreach($state  as $key=>$val): ?>
                                  <option value="<?php echo $val['id'] ?>" <?php if($val['id'] == $model['state']['id']) echo 'selected="selected"';?>> 
                                    <?php echo $val['description'] ?>
                                  </option>
                                <?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-postalcode">
					<label class="col-md-2 control-label" for="userdetails-postalcode">Pincode</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="400001" id="userdetails-postalcode" name="Userdetails[postalcode]" value="<?php if (isset($model['postalcode'])) echo $model['postalcode'];?>" maxlength="50">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-mobile">
					<label class="col-md-2 control-label" for="userdetails-mobile">Mobile</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="9800098000" id="userdetails-mobile" name="Userdetails[mobile]" value="<?php if (isset($model['mobile'])) echo $model['mobile'];?>" maxlength="20">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-phone">
					<label class="col-md-2 control-label" for="userdetails-phone">Phone</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="022-43214321" id="userdetails-phone" name="Userdetails[phone]" value="<?php if (isset($model['phone'])) echo $model['phone'];?>" maxlength="20">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-email">
					<label class="col-md-2 control-label" for="userdetails-email">Email</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" onkeyup="validate2()" type="text" placeholder="abc@example.com" id="userdetails-email" name="Userdetails[email]" value="<?php if (isset($model['email'])) echo $model['email'];?>" maxlength="255">
						</div>
					</div>
					<div id="emailError" ></div>
				</div>
				
				<div class="form-group field-userdetails-website">
					<label class="col-md-2 control-label" for="userdetails-website">Website</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="www.example.com" id="userdetails-website" name="Userdetails[website]" value="<?php if (isset($model['website'])) echo $model['website'];?>" maxlength="255">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-facebookid">
					<label class="col-md-2 control-label" for="userdetails-facebookid">Facebook Profile</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="www.facebook.com/profile" id="userdetails-facebookid" name="Userdetails[facebookid]" value="<?php if (isset($model['facebookid'])) echo $model['facebookid'];?>" maxlength="255">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-twitterhandle">
					<label class="col-md-2 control-label" for="userdetails-twitterhandle">Twitter Handle</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="www.twitter.com/profile" id="userdetails-twitterhandle" name="Userdetails[twitterhandle]" value="<?php if (isset($model['twitterhandle'])) echo $model['twitterhandle'];?>" maxlength="255">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-passport">
					<label class="col-md-2 control-label" for="userdetails-passport">Passport</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="J8361234" id="userdetails-passport" name="Userdetails[passport]" value="<?php if (isset($model['passport'])) echo $model['passport'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-aadhar">
					<label class="col-md-2 control-label" for="userdetails-aadhar">Aadhar Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="2336 1234 1234" id="userdetails-aadhar" name="Userdetails[aadhar]" value="<?php if (isset($model['aadhar'])) echo $model['aadhar'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-voterid">
					<label class="col-md-2 control-label" for="userdetails-voterid">Voter ID Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="K12567754" id="userdetails-voterid" name="Userdetails[voterid]" value="<?php if (isset($model['voterid'])) echo $model['voterid'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-pf">
					<label class="col-md-2 control-label" for="userdetails-pf">Provident Fund Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="KNKRP123459876543" id="userdetails-pf" name="Userdetails[pf]" value="<?php if (isset($model['pf'])) echo $model['pf'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-ppf">
					<label class="col-md-2 control-label" for="userdetails-ppf">PPF Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="776PPF00000000000011" id="userdetails-ppf" name="Userdetails[ppf]" value="<?php if (isset($model['ppf'])) echo $model['ppf'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-superannuation">
					<label class="col-md-2 control-label" for="userdetails-superannuation">Super Annuation Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="KA34349124334" id="userdetails-superannuation" name="Userdetails[superannuation]" value="<?php if (isset($model['superannuation'])) echo $model['superannuation'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-pan">
					<label class="col-md-2 control-label" for="userdetails-pan">PAN Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="AAAPL1234C" id="userdetails-pan" name="Userdetails[pan]" value="<?php if (isset($model['pan'])) echo $model['pan'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-drivinglicense">
					<label class="col-md-2 control-label" for="userdetails-drivinglicense">Driving License Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="TN75 2013 0008800" id="userdetails-drivinglicense" name="Userdetails[drivinglicense]" value="<?php if (isset($model['drivinglicense'])) echo $model['drivinglicense'];?>" maxlength="45">
						</div>
					</div>
				</div>
				
				<div class="form-group field-userdetails-pran">
					<label class="col-md-2 control-label" for="userdetails-pran">PRAN Number</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="K125645434567543" id="userdetails-pran" name="Userdetails[pran]" value="<?php if (isset($model['pran'])) echo $model['pran'];?>" maxlength="45">
						</div>
					</div>
				</div>
              
                <div class="form-group field-userdetails-images">
					<label class="col-md-2 control-label" for="userdetails-images">Upload Images: </label>
					<div class="col-md-8">
                        <div id="image_list">
                          <?php foreach($userImage as $key=>$value): ?>
                            <div class="imgWrap">
                              <?php echo strpos($value['url'], '.pdf') == false ?  '<img src="'. $s3route.'/'. $value['url']. '"
                              style="width:50px;height:50px;">'
                              :'<img src="/images/pdf-icon.png" style="width:50px;height:50px;" onclick="downloadPdf('.'\''. $s3route.'/'.$value['url'].'\''.')">' ?>
                              <label><?php echo $value['filelabel'] ?></label>
                            </div>
                          <?php endforeach; ?>
                        </div>
						<div class="input-group">							
							<input class="form-control form-control1" type="file" id="userdetails-images" name="images[]" onchange="showImages(this)">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" id="ud-updateButton" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script>
		function addStates()
		{
			var countryid = $('#userdetails-countryid').val();
			//alert(countryid);
			var requestStates = $.ajax({
                type: "POST",
                url: "/userdetails/getstates",
                data: {
                    'countryid': countryid
                }
            });

			var states = '<option value="">Select a State..</option>';
            requestStates.done(function (msg) {
                obj = JSON.parse(msg);
                if (obj.status == "success") {
                    //alert(JSON.stringify(obj.data));
					var stData = obj.data;
					for(var i=0; i < stData.length; i++){
						states += '<option value="'+stData[i]['id']+'">'+stData[i]['description']+'</option>';
					}
					
					$('#userdetails-stateid').html(states);
                    
					//document.getElementById("companybutton").innerHTML = obj.data.awsUserName;
					//$('#company').append('<li class="col-md-12 " ><a style="margin-left: 10px; color: black !important;" href="#" onclick="getObjects('+obj.data.ec2AccountID+'\''+obj.data.awsUserName+'\')">'+obj.data.awsUserName+'</a></li>');
					//getObjects(obj.data.ec2AccountID,obj.data.awsUserName);
					
                } else {
                    $('#userdetails-stateid').html('<option value="">Select a State..</option>');
                }
            });
			
		}
		
		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		function validate() {
			$("#emailError").html("");
			var email = $("#userdetails-email").val();
			if (validateEmail(email)) {
				$("#assettypeProfile").submit();
			} else {
				$("#emailError").html('<div style=\"color:red !important;\"><p><center>Please enter a valid email<center></p></div>');
				}
			return false;
		}
		
		function validate2() {
			$("#emailError").html("");
			var email = $("#userdetails-email").val();
			if (validateEmail(email)) {
				$("#emailError").html('<div style=\"color:green !important;\"><p><center>Okay!<center></p></div>');
			} else {
				$("#emailError").html('<div style=\"color:red !important;\"><p><center>Please enter a valid email<center></p></div>');
			}
			return false;
		}
		
		$("#ud-updateButton").bind("click", validate);
      
        function showImages(imgObj){
          var reader = new FileReader();
          var filetype;
          if(imgObj.files.length > 0){
            if(imgObj.files[0].type.indexOf('pdf') != -1){
              filetype = 'pdf';
            }else if(imgObj.files[0].type.indexOf('image') != -1){
              filetype = 'image';
            }else{
              alert('Unsupported file type chosen.Please choose Pdf/Image file only.');
            }
            
            if( imgObj.files[0].size > 5 * 1024 * 1024){
              alert("File size can not be greater than 5 MB.");
            }
          }
          
          
          reader.onload = function (event) {
              if($('#newImageUpload').length > 0){
                $('#newImageUpload').remove();
              }
              var img = event.target.result;
              var imgRef = '<div class="imgWrap" id="newImageUpload">';
              if(filetype == 'pdf'){
                imgRef += '<img src="/images/pdf-icon.png" style="width:50px;height:50px;" onclick="downloadPdf('+'\''+ img+'\''+')">';
              }else{
                imgRef += '<img src="'+img+'" style="width:50px;height:50px;"/>';  
              }        
              imgRef += '<input type="text" name="Userdetails[filelabel]" style="margin-left:3px;"></div>';
              var div = document.getElementById('image_list');
              div.innerHTML = div.innerHTML + imgRef;

          }
          reader.readAsDataURL(imgObj.files[0]);
        }
      
        function downloadPdf(url){
          window.open(url);
        }
	</script>
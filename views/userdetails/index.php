<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Userdetails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="userdetails-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Userdetails', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'clientid',
            'firstname',
            'lastname',
            'gender',
            // 'birthdate',
            // 'address1',
            // 'address2',
            // 'city',
            // 'stateid',
            // 'countryid',
            // 'postalcode',
            // 'phone',
            // 'mobile',
            // 'email:email',
            // 'website',
            // 'facebookid',
            // 'twitterhandle',
            // 'logoimageid',
            // 'profileimageid',
            // 'createdate',
            // 'modifieddate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Commoditytype */

$this->title = 'Create Commoditytype';
$this->params['breadcrumbs'][] = ['label' => 'Commoditytypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commoditytype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

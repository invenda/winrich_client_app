<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<a class="btn btn-primary" href="/insurance/create" style="margin-bottom:15px">Add More Policies</a>
<a class="btn btn-primary" href="/insurance/index" style="margin-bottom:15px; float:right;">Back</a>

<div class="panel-info widget-shadow">
					<h3 class="title1">Insurance</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Asset Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Insurance Type</th>
									<td><?=$model['insurancedes']?></td>
								</tr>
								<tr scope="row">
									<th >Insurance Issuer</th>
									<td><?=$model['arn']?></td>
								</tr>
								<tr scope="row">
									<th >Policy Name</th>
									<td><?=$model['folio']?></td>
								</tr>
								<tr scope="row">
									<th >Insurance Scheme</th>
									<td><?=$model['scheme']?></td>
								</tr>
								<tr scope="row">
									<th >Buyer Name</th>
									<td><?=$model['buyername']?></td>
								</tr>
								<tr scope="row">
									<th >Amount(₹)</th>
									<td><?=$model['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Annual Premium(₹)</th>
									<td><?=$model['annualpremium']?></td>
								</tr>
								<tr scope="row">
									<th >Description</th>
									<td><?=$model['description']?></td>
								</tr>
								<tr scope="row">
									<th >Start Date</th>
									<td><?=$model['startdate']?></td>
								</tr>
								<tr scope="row">
									<th >Policy Term(Years)</th>
									<td><?=$model['policyterm']?></td>
								</tr>
								<tr scope="row">
									<th >Beneficiary Name</th>
									<td><?=$model['beneficiaryname']?></td>
								</tr>
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div>
							<a type="submit" class="btn btn-primary" href="/insurance/update/<?=$model['id']?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/insurance/delete/<?=$model['id']?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
	<a class="btn btn-primary" href="/insurance/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update - Insurance Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeInsurance" class="form-horizontal" action="/insurance/update/<?=$model['id']?>">
			
				<div class="form-group field-insurance-clientid">
					<label class="col-md-2 control-label" for="insurance-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="insurance-clientid" name="Insurance[clientid]">
				</div>
				<div class="form-group field-insurance-scheme">
					<label class="col-md-2 control-label" for="insurance-scheme">Insurance Scheme*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-adn"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Scheme Details" id="insurance-scheme" name="Insurance[scheme]" value="<?=$model['scheme']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-insurance-amount">
					<label class="col-md-2 control-label" for="insurance-amount">Policy Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="500000" id="insurance-amount" name="Insurance[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-insurance-annualpremium">
					<label class="col-md-2 control-label" for="insurance-annualpremium">Annual Premium(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="25000" id="insurance-annualpremium" name="Insurance[annualpremium]" value="<?=$model['annualpremium']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-insurance-description">
					<label class="col-md-2 control-label" for="insurance-description">Description</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-book"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Optional Description" id="insurance-description" name="Insurance[description]" value="<?=$model['description']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-insurance-startdate">
					<label class="col-md-2 control-label" for="insurance-startdate">Start Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['startdate'], 'id' => 'insurance-startdate','name' => 'Insurance[startdate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => '']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-insurance-policyterm">
					<label class="col-md-2 control-label" for="insurance-policyterm">Policy Term(Years)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-clock-o"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="10" id="insurance-policyterm" name="Insurance[policyterm]" value="<?=$model['policyterm']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-insurance-beneficiaryname">
					<label class="col-md-2 control-label" for="insurance-beneficiaryname">Beneficiary Name</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Spouse Name, etc.." id="insurance-beneficiaryname" name="Insurance[beneficiaryname]" value="<?=$model['beneficiaryname']?>">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
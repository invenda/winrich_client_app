<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Insurance';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<a class="btn btn-primary" href="/insurance/create">Add New Insurance Policy</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your Assets</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>Sr No.</th>
									<th>Insurance Type</th>
									<th>Policy Name</th>
									<th>Amount(₹)</th>
									<th>Manage</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<td scope="row"><?=$count?></td>
										<td><?=$insurancetype[$assets['insurancetypeid']-1]['description']?></td>
										<td><?=$assets['folio']?></td>
										<td><?=$assets['amount']?></td>
										<td>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/insurance/view/<?=$assets['id']?>">View</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/insurance/update/<?=$assets['id']?>">Edit</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-danger" href="/insurance/delete/<?=$assets['id']?>">Delete</a>
										</td>
									</tr>
							
								<tr>
								<td colspan="5" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table"> 
							<tbody>
								
								<tr scope="row">
									<th >Insurance Scheme</th>
									<td><?=$assets['scheme']?></td>
								</tr>
								<tr scope="row">
									<th >Buyer Name</th>
									<td><?=$assets['buyername']?></td>
								</tr>
								<tr scope="row">
									<th >Annual Premium(₹)</th>
									<td><?=$assets['annualpremium']?></td>
								</tr>
								<tr scope="row">
									<th >Description</th>
									<td><?=$assets['description']?></td>
								</tr>
								<tr scope="row">
									<th >Start Date</th>
									<td><?=$assets['startdate']?></td>
								</tr>
								<tr scope="row">
									<th >Policy Term(Years)</th>
									<td><?=$assets['policyterm']?></td>
								</tr>
								<tr scope="row">
									<th >Beneficiary Name</th>
									<td><?=$assets['beneficiaryname']?></td>
								</tr>
								
							</tbody></table>
										<a type="submit" class="btn btn-primary" href="/insurance/view/<?=$assets['id']?>">View</a>
										<a type="submit" class="btn btn-primary" href="/insurance/update/<?=$assets['id']?>">Edit</a>
										<a type="submit" class="btn btn-danger" href="/insurance/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php $count++;?>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
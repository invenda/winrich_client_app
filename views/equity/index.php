<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Equity';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<a class="btn btn-primary" href="/equity/create">Add New Equity</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your Assets</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>Sr No.</th>
									<th>Scrip Name</th>
									<th>Units</th>
									<th>Amount</th>
									<th>Transaction Type</th>
									<th>Manage</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<td scope="row"><?=$count?></td>
										<td><?=$assets['scripname']?></td>
										<td><?=$assets['quantity']?></td>
										<td><?=$assets['amount']?></td>
										<td><?=$transtype[$assets['transactiontypeid']-1]['description']?></td>
										<td>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/equity/view/<?=$assets['id']?>">View</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/equity/update/<?=$assets['id']?>">Edit</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-danger" href="/equity/delete/<?=$assets['id']?>">Delete</a>
										</td>
									</tr>
							
								<tr>
								<td colspan="6" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table"> 
							<tbody>
								
								<tr scope="row">
									<th >Share Price(₹)</th>
									<td><?=$assets['rate']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$assets['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Demat Account Number</th>
									<td><?=$assets['demataccountnum']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value</th>
									<td><?=$assets['marketvalue']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value Date</th>
									<td><?=$assets['marketvaluedate']?></td>
								</tr>
								
							</tbody></table>
										<a type="submit" class="btn btn-primary" href="/equity/view/<?=$assets['id']?>">View</a>
										<a type="submit" class="btn btn-primary" href="/equity/update/<?=$assets['id']?>">Edit</a>
										<a type="submit" class="btn btn-danger" href="/equity/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php $count++;?>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
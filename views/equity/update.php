	<a class="btn btn-primary" href="/equity/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Equity Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeEquity" class="form-horizontal" action="/equity/update/<?=$model['id']?>">
			
				<div class="form-group field-equity-clientid">
					<label class="col-md-2 control-label" for="equity-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="equity-clientid" name="Equity[clientid]">
				</div>
				<div class="form-group field-equity-quantity">
					<label class="col-md-2 control-label" for="equity-quantity">Number of Shares*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-list-ol"></i>
							</span>
							<input class="form-control form-control1" type="number" onKeyUp="totalVal()" placeholder="500" id="equity-quantity" name="Equity[quantity]" value="<?=$model['quantity']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-equity-rate">
					<label class="col-md-2 control-label" for="equity-rate">Share Price(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" onKeyUp="totalVal()" step="0.01" placeholder="1000" id="equity-rate" name="Equity[rate]" value="<?=$model['rate']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-equity-amount">
					<label class="col-md-2 control-label" for="equity-scrip">Total Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="25000" id="equity-amount" name="Equity[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-equity-marketvalue">
					<label class="col-md-2 control-label" for="equity-marketvalue">Market Value(₹)</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="1234321" id="equity-marketvalue" name="Equity[marketvalue]" value="<?=$model['marketvalue']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-equity-marketvaluedate">
					<label class="col-md-2 control-label" for="equity-marketvaluedate">Market Value Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['marketvaluedate'], 'id' => 'equity-marketvaluedate','name' => 'Equity[marketvaluedate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script>
	function totalVal(){
		
		var quantity = $("#equity-quantity").val();
		var rate = $("#equity-rate").val();
		
		var total = quantity*rate;
		
		$("#equity-amount").val(total);
	}
	</script>
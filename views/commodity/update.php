	<a class="btn btn-primary" href="/commodity/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Commodity Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeCommodity" class="form-horizontal" action="/commodity/update/<?=$model['id']?>">
			
				<div class="form-group field-commodity-clientid">
					<label class="col-md-2 control-label" for="commodity-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="commodity-clientid" name="Commodity[clientid]">
				</div>
				<div class="form-group field-commodity-units">
					<label class="col-md-2 control-label" for="commodity-units">Number of Units*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-list-ol"></i>
							</span>
							<input class="form-control form-control1" type="number" placeholder="100" id="commodity-units" name="Commodity[units]" value="<?=$model['quantity']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-amount">
					<label class="col-md-2 control-label" for="commodity-amount">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="20000" id="commodity-amount" name="Commodity[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-transactiondate">
					<label class="col-md-2 control-label" for="commodity-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['transactiondate'], 'id' => 'commodity-transactiondate','name' => 'Commodity[transactiondate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => '']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-marketvalue">
					<label class="col-md-2 control-label" for="commodity-marketvalue">Market Value(₹)</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="25000" id="commodity-marketvalue" name="Commodity[marketvalue]" value="<?=$model['marketvalue']?>">
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-marketvaluedate">
					<label class="col-md-2 control-label" for="commodity-marketvaluedate">Market Value Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['marketvaluedate'], 'id' => 'commodity-marketvaluedate','name' => 'Commodity[marketvaluedate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<a class="btn btn-primary" href="/commodity/create" style="margin-bottom:15px">Add More Commodities</a>
<a class="btn btn-primary" href="/commodity/index" style="margin-bottom:15px; float:right;">Back</a>

<div class="panel-info widget-shadow">
					<h3 class="title1">Commodities</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Asset Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Commodity Type</th>
									<td><?=$model['commoditydes']?></td>
								</tr>
								<tr scope="row">
									<th >Unit Type</th>
									<td><?=$model['unitdes']?></td>
								</tr>
								<tr scope="row">
									<th >Number of Units</th>
									<td><?=$model['quantity']?></td>
								</tr>
								<tr scope="row">
									<th >Amount(₹)</th>
									<td><?=$model['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Type</th>
									<td><?=$model['transdes']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$model['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value</th>
									<td><?=$model['marketvalue']?></td>
								</tr>
								
								<tr scope="row">
									<th >Market Value Date</th>
									<td><?=$model['marketvaluedate']?></td>
								</tr>
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div>
							<a type="submit" class="btn btn-primary" href="/commodity/update/<?=$model['id']?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/commodity/delete/<?=$model['id']?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
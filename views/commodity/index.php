<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commodities';
$this->params['breadcrumbs'][] = $this->title;
$count = 1;
?>

				<div class="tables">
					<h3 class="title1"><?= Html::encode($this->title) ?></h3>
					<a class="btn btn-primary" href="/commodity/create">Add New Commodity</a>
					
					<div class="bs-example widget-shadow" data-example-id="hoverable-table"> 
						<h4>Your Assets</h4>
						<table class="table"> 
						
							<thead> 
								<tr> 
									<th>Sr No.</th>
									<th>Commodity Type</th>
									<th>Units</th>
									<th>Amount(₹)</th>
									<th>Transaction Type</th>
									<th>Manage</th>
								</tr> 
							</thead>
							<tbody>
								<?php foreach($dataProvider as $assets):?>
									<tr scope="row" role="button" data-toggle="collapse" class="accordion-toggle"  data-target="#collapse<?=$assets['id']?>" aria-expanded="true" aria-controls="collapse<?=$assets['id']?>" class="">
										<td scope="row"><?=$count?></td>
										<td><?=$commoditytype[$assets['commoditytypeid']-1]['description']?></td>
										<td><?=$assets['quantity']?></td>
										<td><?=$assets['amount']?></td>
										<td><?=$transtype[$assets['transactiontypeid']-1]['description']?></td>
										<td>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/commodity/view/<?=$assets['id']?>">View</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-primary" href="/commodity/update/<?=$assets['id']?>">Edit</a>
											<a type="submit" onclick="event.stopPropagation()" class="btn btn-danger" href="/commodity/delete/<?=$assets['id']?>">Delete</a>
										</td>
									</tr>
							
								<tr>
								<td colspan="6" style="border-top:none !important;">
								<div id="collapse<?=$assets['id']?>" class="accordian-body collapse" role="tabpanel" aria-labelledby="heading<?=$assets['id']?>" aria-expanded="true">
									<div class="mail-body">
										<table class="table"> 
							<tbody>
								
								<tr scope="row">
									<th >Unit</th>
									<td><?=$unittype[$assets['unittypeid']-1]['description']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$assets['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value</th>
									<td><?=$assets['marketvalue']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value Date</th>
									<td><?=$assets['marketvaluedate']?></td>
								</tr>
								
							</tbody></table>
										<a type="submit" class="btn btn-primary" href="/commodity/view/<?=$assets['id']?>">View</a>
										<a type="submit" class="btn btn-primary" href="/commodity/update/<?=$assets['id']?>">Edit</a>
										<a type="submit" class="btn btn-danger" href="/commodity/delete/<?=$assets['id']?>">Delete</a>
									
								</div>
								</div>
								</td>
								</tr>
								<?php $count++;?>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<div class="clearfix"> </div>
						
					</div>
				
				</div>
				
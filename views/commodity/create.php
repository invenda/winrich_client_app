	<a class="btn btn-primary" href="/commodity/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Add New - Commodity Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeCommodity" class="form-horizontal" action="/commodity/create">
			
				<div class="form-group field-commodity-clientid">
					<label class="col-md-2 control-label" for="commodity-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="commodity-clientid" name="Commodity[clientid]">
				</div>
				<div class="form-group field-commodity-commoditytypeid">
					<label class="col-md-2 control-label" for="commodity-commoditytypeid">Commodity Type*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="commodity-commoditytypeid" name="Commodity[commoditytypeid]" required>
								<option value="">Select Commodity Type..</option>
								<?php foreach($commoditytype as $c):?>
								<option value="<?=$c['id']?>"><?=$c['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-unittypeid">
					<label class="col-md-2 control-label" for="commodity-unittypeid">Unit Type*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="commodity-unittypeid" name="Commodity[unittypeid]" required>
								<option value="">Select Unit Type..</option>
								<?php foreach($unittype as $u):?>
								<option value="<?=$u['id']?>"><?=$u['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-quantity">
					<label class="col-md-2 control-label" for="commodity-quantity">Number of Units*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-list-ol"></i>
							</span>
							<input class="form-control form-control1" type="number" placeholder="100" id="commodity-quantity" name="Commodity[quantity]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-amount">
					<label class="col-md-2 control-label" for="commodity-amount">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="20000" id="commodity-amount" name="Commodity[amount]" required>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-transactiontypeid">
					<label class="col-md-2 control-label" for="commodity-transactiontypeid">Transaction Type*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-sort"></i>
							</span>
							<select class="form-control1"  id="commodity-transactiontypeid" name="Commodity[transactiontypeid]" required>
								<option value="">Select Transaction Type..</option>
								<?php foreach($transtype as $t):?>
								<option value="<?=$t['id']?>"><?=$t['description']?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-transactiondate">
					<label class="col-md-2 control-label" for="commodity-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'attribute' => 'transactiondate','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => ""]]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-marketvalue">
					<label class="col-md-2 control-label" for="commodity-marketvalue">Market Value(₹)</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="25000" id="commodity-marketvalue" name="Commodity[marketvalue]" >
						</div>
					</div>
				</div>
				<div class="form-group field-commodity-marketvaluedate">
					<label class="col-md-2 control-label" for="commodity-marketvaluedate">Market Value Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'attribute' => 'marketvaluedate','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
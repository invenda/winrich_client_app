<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Commodity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="commodity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clientid')->textInput() ?>

    <?= $form->field($model, 'commoditytypeid')->textInput() ?>

    <?= $form->field($model, 'unittypeid')->textInput() ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'transactiontypeid')->textInput() ?>

    <?= $form->field($model, 'transactiondate')->textInput() ?>

    <?= $form->field($model, 'marketvalue')->textInput() ?>

    <?= $form->field($model, 'marketvaluedate')->textInput() ?>

    <?= $form->field($model, 'documentimageid')->textInput() ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'modifieddate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

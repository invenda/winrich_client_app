<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Insurancetype */

$this->title = 'Create Insurancetype';
$this->params['breadcrumbs'][] = ['label' => 'Insurancetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancetype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Assets */
?>

<a class="btn btn-primary" href="/realesstate/create" style="margin-bottom:15px">Add More Properties</a>
<a class="btn btn-primary" href="/realestate/index" style="margin-bottom:15px; float:right;">Back</a>

<div class="panel-info widget-shadow">
					<h3 class="title1">Real Estate</h3>

					<div class="col-md-12 panel-grids">
						
						
						<div class="panel panel-primary"> 
							<div class="panel-heading" >
								<h3 class="panel-title">Asset Details</h3>
							</div>
							<div class="panel-body" style="padding-top:0px !important;">
							<table class="table table-hover"> 
							<tbody>
								<tr scope="row">
									<th >Property Owner</th>
									<td><?=$model['buyername']?></td>
								</tr>
								<tr scope="row">
									<th >Property Address</th>
									<td><?=$model['address']?></td>
								</tr>
								<tr scope="row">
									<th >City</th>
									<td><?=$model['city']?></td>
								</tr>
								<tr scope="row">
									<th >Unit Price(₹)</th>
									<td><?=$model['unitprice']?></td>
								</tr>
								<tr scope="row">
									<th >Area(Sq Ft.)</th>
									<td><?=$model['area']?></td>
								</tr>
								<tr scope="row">
									<th >Amount(₹)</th>
									<td><?=$model['amount']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Date</th>
									<td><?=$model['transactiondate']?></td>
								</tr>
								<tr scope="row">
									<th >Transaction Type</th>
									<td><?=$model['transdes']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value</th>
									<td><?=$model['marketvalue']?></td>
								</tr>
								<tr scope="row">
									<th >Market Value Date</th>
									<td><?=$model['marketvaluedate']?></td>
								</tr>
								
							</tbody>
							
						</table>
							</div>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-8">
						<div>
							<a type="submit" class="btn btn-primary" href="/realestate/update/<?=$model['id']?>">Edit</a>
							<a type="submit" class="btn btn-danger" href="/realestate/delete/<?=$model['id']?>">Delete</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				
				</div>
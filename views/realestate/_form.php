<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Realestate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realestate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'clientid')->textInput() ?>

    <?= $form->field($model, 'buyername')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unitprice')->textInput() ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'transactiondate')->textInput() ?>

    <?= $form->field($model, 'transactiontypeid')->textInput() ?>

    <?= $form->field($model, 'marketvalue')->textInput() ?>

    <?= $form->field($model, 'marketvaluedate')->textInput() ?>

    <?= $form->field($model, 'documentimageid')->textInput() ?>

    <?= $form->field($model, 'createdate')->textInput() ?>

    <?= $form->field($model, 'modifieddate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

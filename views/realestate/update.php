	<a class="btn btn-primary" href="/realestate/index" style="margin-bottom:15px; float:right;">Back</a>
	<h3 class="title1">Update Real Estate Details</h3>
	<div class="form-three widget-shadow">
		<div class=" panel-body-inputin">
			<form method="post" id="assettypeRealestate" class="form-horizontal" action="/realestate/update/<?=$model['id']?>">
			
				<div class="form-group field-realestate-clientid">
					<label class="col-md-2 control-label" for="realestate-clientid"></label>
						<input class="form-control form-control1" type="hidden" value="<?=Yii::$app->user->id;?>" placeholder="clientid" id="realestate-clientid" name="Realestate[clientid]">
				</div>
				<div class="form-group field-realestate-buyername">
					<label class="col-md-2 control-label" for="realestate-buyername">Buyer Name*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-user"></i>
							</span>
							<input class="form-control form-control1" type="text" placeholder="Robert" id="realestate-buyername" name="Realestate[buyername]" value="<?=$model['buyername']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-realestate-unitprice">
					<label class="col-md-2 control-label" for="realestate-unitprice">Unit Price(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" onKeyUp="totalVal()" step="0.01" placeholder="10500" id="realestate-unitprice" name="Realestate[unitprice]" value="<?=$model['unitprice']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-realestate-area">
					<label class="col-md-2 control-label" for="realestate-area">Area(Sq. Ft.)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-th"></i>
							</span>
							<input class="form-control form-control1" type="number" onKeyUp="totalVal()" step="0.01" placeholder="10500" id="realestate-area" name="Realestate[area]" value="<?=$model['area']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-realestate-amount">
					<label class="col-md-2 control-label" for="realestate-amount">Amount(₹)*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="2500000" id="realestate-amount" name="Realestate[amount]" value="<?=$model['amount']?>" required>
						</div>
					</div>
				</div>
				<div class="form-group field-realestate-transactiondate">
					<label class="col-md-2 control-label" for="realestate-transactiondate">Transaction Date*</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['transactiondate'], 'id' => 'realestate-transactiondate','name' => 'Realestate[transactiondate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd', 'required' => '']]) ?>
						</div>
					</div>
				</div>
				<div class="form-group field-realestate-marketvalue">
					<label class="col-md-2 control-label" for="realestate-marketvalue">Market Value</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rupee"></i>
							</span>
							<input class="form-control form-control1" type="number" step="0.01" placeholder="3250000" id="realestate-marketvalue" name="Realestate[marketvalue]" value="<?=$model['marketvalue']?>">
						</div>
					</div>
				</div>
				
				
				<div class="form-group field-realestate-marketvaluedate">
					<label class="col-md-2 control-label" for="realestate-marketvaluedate">Market Value Date</label>
					<div class="col-md-8">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<?=yii\jui\DatePicker::widget(['model' => $model,'value'=> $model['marketvaluedate'], 'id' => 'realestate-marketvaluedate','name' => 'Realestate[marketvaluedate]','language' => 'en','dateFormat' => 'yyyy-MM-dd','options' => ['class' => 'form-control form-control1', 'placeholder' => 'yyyy-mm-dd']]) ?>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8">
						<div class="input-group">
							<button type="submit" class="btn btn-primary">Update</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script>
	function totalVal(){
		
		var quantity = $("#realestate-unitprice").val();
		var rate = $("#realestate-area").val();
		
		var total = quantity*rate;
		
		$("#realestate-amount").val(total);
	}
	</script>
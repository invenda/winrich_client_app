<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "arn".
 *
 * @property integer $id
 * @property string $description
 * @property integer $instrumentid
 */
class Arn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'arn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instrumentid'], 'integer'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'instrumentid' => 'Instrumentid',
        ];
    }
}

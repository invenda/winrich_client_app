<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documentimage".
 *
 * @property integer $id
 * @property string $imagename
 */
class Documentimage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documentimage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imagename'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imagename' => 'Imagename',
        ];
    }
}

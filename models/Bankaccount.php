<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bankaccount".
 *
 * @property integer $id
 * @property integer $clientid
 * @property string $primaryholder
 * @property string $jointholder
 * @property string $bankname
 * @property string $bankaddress
 * @property string $accountnum
 * @property string $ifsc
 * @property string $micr
 * @property string $isactive
 * @property string $specimen
 * @property string $createdate
 * @property string $modifieddate
 */
class Bankaccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bankaccount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid'], 'integer'],
            [['createdate', 'modifieddate'], 'safe'],
            [['primaryholder', 'jointholder', 'bankname', 'accountnum'], 'string', 'max' => 100],
            [['bankaddress'], 'string', 'max' => 255],
            [['ifsc', 'micr'], 'string', 'max' => 45],
            [['isactive'], 'string', 'max' => 10],
            [['specimen'], 'string', 'max' => 250],
            [['nominee'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'primaryholder' => 'Primaryholder',
            'jointholder' => 'Jointholder',
            'bankname' => 'Bankname',
            'bankaddress' => 'Bankaddress',
            'accountnum' => 'Accountnum',
            'ifsc' => 'Ifsc',
            'micr' => 'Micr',
            'isactive' => 'Isactive',
            'specimen' => 'specimen',
            'nominee' => 'nominee',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

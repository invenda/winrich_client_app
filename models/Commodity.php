<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commodity".
 *
 * @property integer $id
 * @property integer $clientid
 * @property integer $commoditytypeid
 * @property integer $unittypeid
 * @property double $quantity
 * @property double $amount
 * @property integer $transactiontypeid
 * @property string $transactiondate
 * @property double $marketvalue
 * @property string $marketvaluedate
 * @property integer $documentimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class Commodity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'commodity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'commoditytypeid', 'unittypeid', 'transactiontypeid', 'documentimageid'], 'integer'],
            [['quantity', 'amount', 'marketvalue'], 'number'],
            [['transactiondate', 'marketvaluedate', 'createdate', 'modifieddate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'commoditytypeid' => 'Commoditytypeid',
            'unittypeid' => 'Unittypeid',
            'quantity' => 'Quantity',
            'amount' => 'Amount',
            'transactiontypeid' => 'Transactiontypeid',
            'transactiondate' => 'Transactiondate',
            'marketvalue' => 'Marketvalue',
            'marketvaluedate' => 'Marketvaluedate',
            'documentimageid' => 'Documentimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

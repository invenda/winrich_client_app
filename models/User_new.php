<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $usertype
 * @property string $firstname
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property string $gender
 * @property string $birthdate
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property integer $stateid
 * @property integer $countryid
 * @property string $postalcode
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property string $website
 * @property string $facebookid
 * @property string $twitterhandle
 * @property integer $logoimageid
 * @property integer $profileimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usertype', 'stateid', 'countryid', 'logoimageid', 'profileimageid'], 'integer'],
            [['birthdate', 'createdate', 'modifieddate'], 'safe'],
            [['firstname', 'lastname', 'username', 'city'], 'string', 'max' => 100],
            [['password', 'address1', 'address2', 'email', 'website', 'facebookid', 'twitterhandle'], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 45],
            [['postalcode'], 'string', 'max' => 50],
            [['phone', 'mobile'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usertype' => 'Usertype',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'username' => 'Username',
            'password' => 'Password',
            'gender' => 'Gender',
            'birthdate' => 'Birthdate',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'city' => 'City',
            'stateid' => 'Stateid',
            'countryid' => 'Countryid',
            'postalcode' => 'Postalcode',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'website' => 'Website',
            'facebookid' => 'Facebookid',
            'twitterhandle' => 'Twitterhandle',
            'logoimageid' => 'Logoimageid',
            'profileimageid' => 'Profileimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

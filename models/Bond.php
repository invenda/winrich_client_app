<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bond".
 *
 * @property integer $id
 * @property integer $clientid
 * @property string $issuername
 * @property string $scrip
 * @property integer $units
 * @property double $amount
 * @property string $beneficiaryname
 * @property string $transactiondate
 * @property string $applicationnum
 * @property integer $transactiontypeid
 * @property integer $documentimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class Bond extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bond';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'units', 'transactiontypeid', 'documentimageid'], 'integer'],
            [['amount'], 'number'],
            [['transactiondate', 'createdate', 'modifieddate'], 'safe'],
            [['issuername', 'scrip'], 'string', 'max' => 255],
            [['beneficiaryname', 'applicationnum'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'issuername' => 'Issuername',
            'scrip' => 'Scrip',
            'units' => 'Units',
            'amount' => 'Amount',
            'beneficiaryname' => 'Beneficiaryname',
            'transactiondate' => 'Transactiondate',
            'applicationnum' => 'Applicationnum',
            'transactiontypeid' => 'Transactiontypeid',
            'documentimageid' => 'Documentimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mutualfund".
 *
 * @property integer $id
 * @property integer $clientid
 * @property string $arn
 * @property string $transactiondate
 * @property string $folio
 * @property string $scheme
 * @property double $amount
 * @property double $marketvalue
 * @property string $marketvaluedate
 * @property integer $transactiontypeid
 * @property integer $documentimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class Mutualfund extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mutualfund';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'transactiontypeid', 'documentimageid'], 'integer'],
            [['transactiondate', 'marketvaluedate', 'createdate', 'modifieddate'], 'safe'],
            [['amount', 'marketvalue'], 'number'],
            [['arn', 'folio', 'scheme'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'arn' => 'Arn',
            'transactiondate' => 'Transactiondate',
            'folio' => 'Folio',
            'scheme' => 'Scheme',
            'amount' => 'Amount',
            'marketvalue' => 'Marketvalue',
            'marketvaluedate' => 'Marketvaluedate',
            'transactiontypeid' => 'Transactiontypeid',
            'documentimageid' => 'Documentimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

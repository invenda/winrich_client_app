<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactiontype".
 *
 * @property integer $id
 * @property string $description
 * @property integer $instrumentid
 */
class Transactiontype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactiontype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instrumentid'], 'integer'],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'instrumentid' => 'Instrumentid',
        ];
    }
}

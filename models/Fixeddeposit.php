<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fixeddeposit".
 *
 * @property integer $id
 * @property integer $clientid
 * @property string $accountname
 * @property string $bankname
 * @property string $bankaddress
 * @property string $scheme
 * @property double $interestrate
 * @property double $amount
 * @property string $period
 * @property string $fdrnum
 * @property string $transactiondate
 * @property string $maturitydate
 * @property double $maturityamt
 * @property string $beneficiaryname
 * @property integer $documentimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class Fixeddeposit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fixeddeposit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'documentimageid'], 'integer'],
            [['interestrate', 'amount', 'maturityamt'], 'number'],
            [['transactiondate', 'maturitydate', 'createdate', 'modifieddate'], 'safe'],
            [['accountname', 'bankname', 'bankaddress', 'scheme'], 'string', 'max' => 255],
            [['period', 'fdrnum', 'beneficiaryname'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'accountname' => 'Accountname',
            'bankname' => 'Bankname',
            'bankaddress' => 'Bankaddress',
            'scheme' => 'Scheme',
            'interestrate' => 'Interestrate',
            'amount' => 'Amount',
            'period' => 'Period',
            'fdrnum' => 'Fdrnum',
            'transactiondate' => 'Transactiondate',
            'maturitydate' => 'Maturitydate',
            'maturityamt' => 'Maturityamt',
            'beneficiaryname' => 'Beneficiaryname',
            'documentimageid' => 'Documentimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

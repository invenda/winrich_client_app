<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userimagedocument".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $doctype
 * @property string $filelabel
 * @property string $url
 * @property integer $createdby
 * @property string $createddate
 *
 * @property User $user
 */
class Userimagedocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userimagedocument';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid', 'doctype', 'url'], 'required'],
            [['userid', 'doctype', 'createdby'], 'integer'],
            [['createddate'], 'safe'],
            [['filelabel', 'url'], 'string', 'max' => 255],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'doctype' => 'Doctype',
            'filelabel' => 'Filelabel',
            'url' => 'Url',
            'createdby' => 'Createdby',
            'createddate' => 'Createddate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}

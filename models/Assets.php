<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "assets".
 *
 * @property integer $id
 * @property integer $instrumentid
 * @property integer $arnid
 * @property string $folio
 * @property integer $transactiontypeid
 * @property integer $schemeid
 * @property string $description
 * @property integer $clientid
 * @property double $amount
 * @property string $multipledays
 * @property integer $frequencyid
 * @property string $startdate
 * @property string $enddate
 * @property string $createdate
 * @property string $modifieddate
 */
class Assets extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'assets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instrumentid', 'arnid', 'transactiontypeid', 'schemeid', 'clientid', 'frequencyid'], 'integer'],
            [['amount'], 'number'],
            [['startdate', 'enddate', 'createdate', 'modifieddate'], 'safe'],
            [['folio', 'description'], 'string', 'max' => 500],
            [['multipledays'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instrumentid' => 'Instrumentid',
            'arnid' => 'Arnid',
            'folio' => 'Folio',
            'transactiontypeid' => 'Transactiontypeid',
            'schemeid' => 'Schemeid',
            'description' => 'Description',
            'clientid' => 'Clientid',
            'amount' => 'Amount',
            'multipledays' => 'Multipledays',
            'frequencyid' => 'Frequencyid',
            'startdate' => 'Startdate',
            'enddate' => 'Enddate',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

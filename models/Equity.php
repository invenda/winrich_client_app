<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equity".
 *
 * @property integer $id
 * @property integer $clientid
 * @property string $scripname
 * @property integer $quantity
 * @property double $rate
 * @property double $amount
 * @property string $transactiondate
 * @property string $demataccountnum
 * @property double $marketvalue
 * @property string $marketvaluedate
 * @property integer $transactiontypeid
 * @property string $createdate
 * @property string $modifieddate
 */
class Equity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'equity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'quantity', 'transactiontypeid'], 'integer'],
            [['rate', 'amount', 'marketvalue'], 'number'],
            [['transactiondate', 'marketvaluedate', 'createdate', 'modifieddate'], 'safe'],
            [['scripname'], 'string', 'max' => 255],
            [['demataccountnum'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'scripname' => 'Scripname',
            'quantity' => 'Quantity',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'transactiondate' => 'Transactiondate',
            'demataccountnum' => 'Demataccountnum',
            'marketvalue' => 'Marketvalue',
            'marketvaluedate' => 'Marketvaluedate',
            'transactiontypeid' => 'Transactiontypeid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

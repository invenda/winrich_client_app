<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrument".
 *
 * @property integer $id
 * @property string $description
 * @property string $instrumentcol
 */
class Instrument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instrument';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string', 'max' => 500],
            [['instrumentcol'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'instrumentcol' => 'Instrumentcol',
        ];
    }
}

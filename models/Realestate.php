<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "realestate".
 *
 * @property integer $id
 * @property integer $clientid
 * @property string $buyername
 * @property string $address
 * @property string $city
 * @property double $unitprice
 * @property string $area
 * @property double $amount
 * @property string $transactiondate
 * @property integer $transactiontypeid
 * @property double $marketvalue
 * @property string $marketvaluedate
 * @property integer $documentimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class Realestate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'realestate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'transactiontypeid', 'documentimageid'], 'integer'],
            [['unitprice', 'amount', 'marketvalue'], 'number'],
            [['transactiondate', 'marketvaluedate', 'createdate', 'modifieddate'], 'safe'],
            [['buyername', 'city'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 1000],
            [['area'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientid' => 'Clientid',
            'buyername' => 'Buyername',
            'address' => 'Address',
            'city' => 'City',
            'unitprice' => 'Unitprice',
            'area' => 'Area',
            'amount' => 'Amount',
            'transactiondate' => 'Transactiondate',
            'transactiontypeid' => 'Transactiontypeid',
            'marketvalue' => 'Marketvalue',
            'marketvaluedate' => 'Marketvaluedate',
            'documentimageid' => 'Documentimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

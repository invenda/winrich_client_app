<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurance".
 *
 * @property integer $id
 * @property integer $insurancetypeid
 * @property integer $clientid
 * @property string $arn
 * @property string $folio
 * @property string $scheme
 * @property string $buyername
 * @property double $amount
 * @property double $annualpremium
 * @property string $description
 * @property string $startdate
 * @property string $policyterm
 * @property string $beneficiaryname
 * @property integer $documentimageid
 * @property string $createdate
 * @property string $modifieddate
 */
class Insurance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insurance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insurancetypeid', 'clientid', 'documentimageid'], 'integer'],
            [['amount', 'annualpremium'], 'number'],
            [['startdate', 'createdate', 'modifieddate'], 'safe'],
            [['arn', 'folio', 'scheme', 'buyername', 'description'], 'string', 'max' => 255],
            [['policyterm', 'beneficiaryname'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'insurancetypeid' => 'Insurancetypeid',
            'clientid' => 'Clientid',
            'arn' => 'Arn',
            'folio' => 'Folio',
            'scheme' => 'Scheme',
            'buyername' => 'Buyername',
            'amount' => 'Amount',
            'annualpremium' => 'Annualpremium',
            'description' => 'Description',
            'startdate' => 'Startdate',
            'policyterm' => 'Policyterm',
            'beneficiaryname' => 'Beneficiaryname',
            'documentimageid' => 'Documentimageid',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
}

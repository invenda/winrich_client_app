<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "scheme".
 *
 * @property integer $id
 * @property string $description
 * @property string $code
 * @property integer $instrumentid
 */
class Scheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scheme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['instrumentid'], 'integer'],
            [['description', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'code' => 'Code',
            'instrumentid' => 'Instrumentid',
        ];
    }
}

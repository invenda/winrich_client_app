<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $clientclass
 * @property string $clientname
 * @property string $clientstatus
 * @property string $username
 * @property string $password
 * @property integer $clientid
 * @property integer $usertype
 * @property string $address
 * @property string $street
 * @property string $area
 * @property string $city
 * @property string $pincode
 * @property string $stateid
 * @property string $phone1
 * @property string $phone2
 * @property string $mobile
 * @property string $pan
 * @property string $email
 * @property string $birthdate
 * @property string $anniversarydate
 * @property string $bankname
 * @property string $rnname
 * @property string $isactive
 * @property integer $termsaccept
 * @property string $hcode
 * @property string $createdate
 * @property string $modifieddate
 *
 * @property Userdetails[] $userdetails
 * @property Userimagedocument[] $userimagedocuments
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $authKey;
    public $accessToken;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clientid', 'usertype', 'termsaccept'], 'integer'],
            [['createdate', 'modifieddate'], 'safe'],
            [['clientclass'], 'string', 'max' => 4],
            [['clientname', 'username', 'address', 'street', 'area', 'stateid', 'email', 'birthdate', 'anniversarydate', 'bankname', 'rnname'], 'string', 'max' => 255],
            [['clientstatus', 'phone1', 'phone2'], 'string', 'max' => 50],
            [['password'], 'string', 'max' => 500],
            [['city'], 'string', 'max' => 80],
            [['pincode'], 'string', 'max' => 35],
            [['mobile'], 'string', 'max' => 20],
            [['pan', 'isactive'], 'string', 'max' => 45],
            [['hcode'], 'string', 'max' => 100],
            [['clientname'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clientclass' => 'Clientclass',
            'clientname' => 'Clientname',
            'clientstatus' => 'Clientstatus',
            'username' => 'Username',
            'password' => 'Password',
            'clientid' => 'Clientid',
            'usertype' => 'Usertype',
            'address' => 'Address',
            'street' => 'Street',
            'area' => 'Area',
            'city' => 'City',
            'pincode' => 'Pincode',
            'stateid' => 'Stateid',
            'phone1' => 'Phone1',
            'phone2' => 'Phone2',
            'mobile' => 'Mobile',
            'pan' => 'Pan',
            'email' => 'Email',
            'birthdate' => 'Birthdate',
            'anniversarydate' => 'Anniversarydate',
            'bankname' => 'Bankname',
            'rnname' => 'Rnname',
            'isactive' => 'Isactive',
            'termsaccept' => 'Termsaccept',
            'hcode' => 'Hcode',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }
  
        /**
     * @inheritdoc
     */
    public static function findIdentity($id) {
        $user = self::find()
                ->where([
                    "id" => $id
                ])
                ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $userType = null) {

        $user = self::find()
                ->where(["accessToken" => $token])
                ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username) {
        $user = self::find()
                ->where([
                    "username" => $username
                ])
                ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === $password;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserdetails()
    {
        return $this->hasMany(Userdetails::className(), ['userid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserimagedocuments()
    {
        return $this->hasMany(Userimagedocument::className(), ['userid' => 'id']);
    }
}

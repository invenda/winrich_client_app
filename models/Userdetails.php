<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userdetails".
 *
 * @property integer $id
 * @property integer $userid
 * @property integer $clientid
 * @property string $firstname
 * @property string $lastname
 * @property string $gender
 * @property string $birthdate
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property integer $stateid
 * @property integer $countryid
 * @property string $postalcode
 * @property string $phone
 * @property string $mobile
 * @property string $email
 * @property string $website
 * @property string $facebookid
 * @property string $twitterhandle
 * @property integer $logoimageid
 * @property integer $profileimageid
 * @property string $passport
 * @property string $aadhar
 * @property string $voterid
 * @property string $pf
 * @property string $ppf
 * @property string $superannuation
 * @property string $pan
 * @property string $drivinglicense
 * @property string $pran
 * @property string $createdate
 * @property string $modifieddate
 *
 * @property Country $country
 * @property States $state
 * @property User $user
 */
class Userdetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userdetails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid'], 'required'],
            [['userid', 'clientid', 'stateid', 'countryid', 'logoimageid', 'profileimageid'], 'integer'],
            [['birthdate', 'createdate', 'modifieddate'], 'safe'],
            [['firstname', 'lastname', 'city'], 'string', 'max' => 100],
            [['gender', 'passport', 'aadhar', 'voterid', 'pf', 'ppf', 'superannuation', 'pan', 'drivinglicense', 'pran'], 'string', 'max' => 45],
            [['address1', 'address2', 'email', 'website', 'facebookid', 'twitterhandle'], 'string', 'max' => 255],
            [['postalcode'], 'string', 'max' => 50],
            [['phone', 'mobile'], 'string', 'max' => 20],
            [['countryid'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['countryid' => 'id']],
            [['stateid'], 'exist', 'skipOnError' => true, 'targetClass' => States::className(), 'targetAttribute' => ['stateid' => 'id']],
            [['userid'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'clientid' => 'Clientid',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'gender' => 'Gender',
            'birthdate' => 'Birthdate',
            'address1' => 'Address1',
            'address2' => 'Address2',
            'city' => 'City',
            'stateid' => 'Stateid',
            'countryid' => 'Countryid',
            'postalcode' => 'Postalcode',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'website' => 'Website',
            'facebookid' => 'Facebookid',
            'twitterhandle' => 'Twitterhandle',
            'logoimageid' => 'Logoimageid',
            'profileimageid' => 'Profileimageid',
            'passport' => 'Passport',
            'aadhar' => 'Aadhar',
            'voterid' => 'Voterid',
            'pf' => 'Pf',
            'ppf' => 'Ppf',
            'superannuation' => 'Superannuation',
            'pan' => 'Pan',
            'drivinglicense' => 'Drivinglicense',
            'pran' => 'Pran',
            'createdate' => 'Createdate',
            'modifieddate' => 'Modifieddate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'countryid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(States::className(), ['id' => 'stateid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }
}
